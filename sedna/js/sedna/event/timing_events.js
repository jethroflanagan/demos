define([],
function () {
	var type = new box.event.EventType('timing event');
	var e = box.event.createEvent(type);
	return {
		tick: e('tick')
	};
});