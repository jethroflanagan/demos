define([],
function () {
	var type = new box.event.EventType('app event');
	var e = box.event.createEvent(type);
	return {
		showScene: e('showScene'),
		loadScene: e('loadScene'),
		showPlayer: e('showPlayer')
	};
});