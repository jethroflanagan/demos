define([
	box.pkg('main'),
	
	//load up deps for the project
	box.lib('backbone'),
	box.lib('lodash'),
	box.lib('jquery-nc'),
	box.lib('modernizr')
],
function (Main, Backbone, _, $) {
	// easing fails to import even with compiler, so include only used anims here
	$.extend( $.easing, {
		def: 'easeOutQuad',
		easeOutQuad: function (x, t, b, c, d) {
			return -c *(t/=d)*(t-2) + b;
			},
		easeOutBack: function (x, t, b, c, d, s) {
			if (s == undefined) s = 1.70158;
			return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
		}
	});

	//console.log('v', $.fn.jquery, 'new', $.easing);
	var main = new Main();
});