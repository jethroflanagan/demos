define([
	box.lib('backbone'),
	box.lib('lodash'),
	box.lib('jquery-nc'),

	//project
	box.pkg('event/menu_events'),

	//partials
	'text!partial/menu_item.html'
],
function (Backbone, _, $, MenuEvents, Template) {
	var Menu = Backbone.View.extend({
		$element: null,
		menuId: null,
		title: null,
		number: null,
		cancelHighlight: null,

		initialize: function (params) {
			_.bindAll(this);
			this.menuId = params.menuId;
			this.title = params.title;
			this.number = params.number;

			this.render();
			this.$el.appendTo(params.container);
			this.$el.css({ left: (this.number - 1) * 107 });

			this.enable();
		},

		render: function () {
			this.$el = $(_.template(Template, {
				id: this.menuId,
				title: this.title,
				number: this.number
			}));
		},

		enable: function () {
			this.$el.click(this.onClick);
			this.$el.addClass('menu__item--enabled');
			this.stopHighlighting();
		},

		disable: function () {
			this.$el.unbind('click');
			this.$el.removeClass('menu__item--enabled');
			this.stopHighlighting();
		},

		onClick: function (e) {
			this.setActive();
		},

		setActive: function () {
			//console.log(this.menuId);
			box.event.dispatch(this, MenuEvents.activate, {id: this.menuId});
			this.$el.siblings().removeClass('menu__item--active');
			this.$el.addClass('menu__item--active');
		},

		highlight: function () {
			window.btn = this;
			this.cancelHighlight = false;
			this.fadeInHighlight();
		},

		fadeInHighlight: function () {
			if (this.cancelHighlight) {
				this.removeHighlight();
				return;
			}
			this.$el.animate({ opacity: 1 }, 800, this.fadeOutHighlight);
		},

		fadeOutHighlight: function () {
			if (this.cancelHighlight) {
				this.removeHighlight();
				return;
			}
			this.$el.animate({ opacity: 0.5 }, 300, this.fadeInHighlight);
		},

		removeHighlight: function () {
			this.cancelHighlight = false;
			this.$el.css({ opacity: '' });
		},

		stopHighlighting: function () {
			this.cancelHighlight = true;
		}

	});

	return Menu;
});