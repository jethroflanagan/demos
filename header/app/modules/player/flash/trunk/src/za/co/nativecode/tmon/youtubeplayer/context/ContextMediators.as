package za.co.nativecode.tmon.youtubeplayer.context 
{
	import org.robotlegs.core.IMediatorMap;
	import za.co.nativecode.tmon.youtubeplayer.Main;
	import za.co.nativecode.tmon.youtubeplayer.mediator.ApplicationMediator;
	import za.co.nativecode.tmon.youtubeplayer.mediator.YouTubePlayerMediator;
	import za.co.nativecode.tmon.youtubeplayer.view.YouTubePlayer;
	/**
	 * @author Bradley Botha
	 */
	public class ContextMediators 
	{
		
		static public function init(mediatorMap:IMediatorMap):void
		{
			mediatorMap.mapView(YouTubePlayer, YouTubePlayerMediator);
			
			//app mediator
			mediatorMap.mapView(Main, ApplicationMediator);
			
		}
		
	}

}