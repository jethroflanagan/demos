<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>YouTube Player</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />

	<script src="js/swfobject.js" type="text/javascript"></script>
	<script src="js/jquery-1.7.2.min.js" type="text/javascript"></script>
	<script src="js/yt.js" type="text/javascript"></script>

	<script type="text/javascript">
		var videoProperties = {
			videoID:"h99SBtgWZUs"
		};
		var params = {
			menu: "false",
			scale: "noScale",
			allowFullscreen: "true",
			allowScriptAccess: "always",
			bgcolor: "#000000"
		};
		var attributes = {
			id:"YouTubePlayer"
		};
		//swfobject.embedSWF("YouTube%20Player.swf", "player-internal", "100%", "100%", "9.0.0", "expressInstall.swf", videoProperties, params, attributes);
	</script>
	<style>
		body
		{
			margin:0;
		}
		.video-player
		{
			width: 640px;
			height: 360px;
			position: relative;
		}
		.video-player .over, .video-player .normal
		{
			position: absolute;
			left: 50%;
	        top: 50%;
	        margin-top: -47px;
	        margin-left: -44px;
	        cursor: pointer;
	    }
		.video-player .bumper
		{
			position: absolute;
			top: 0;
			left: 0;
			display: none;
		}
	</style>
</head>
<body>
	<!-- Note: the div with the ID of 'swfInjectTarget' is REPLACED by the flash object, it is not inserted into this div-->
	<div class="video-player">
		<!-- The swf will be injected here -->
		<div class="player-internal" id="player-internal">
			<script>
				//document.write('<iframe width="100%" height="100%" frameborder="0" allowfullscreen src="http://www.youtube.com/embed/' + videoProperties.videoID + '?theme=light&color=white&html5=1"></iframe>');
				var setupPlayer;
				$(document).ready(function()
					{
						var holder = $(".video-player");
						var player = $(".player-internal", holder);
						var ytPlayer; //youtube player
						var playButton = $(".normal", player);
						var playOverButton = $(".over", player);
						var bumper = $(".bumper", player);

						//init();

						function onPlayerReady(e)
						{
							console.log("ready");
							//e.target.playVideo();
						}

						function onPlayerStateChange(e)
						{

						}

						function setupYoutube()
						{
							ytPlayer = new YT.Player("yt-player",
								{
									height: "360",
									width: "640",
									videoId: videoProperties.videoID,
									playerVars: { "controls": 0},
									html5: 1,
									events:
										{
											"onReady": onPlayerReady,
											"onStateChange": onPlayerStateChange
										}
								});
						}

						function init()
						{
							console.log("init");
							playButton.mouseover(onOver);
							playButton.mouseout(onOut);
							holder.click(showVideo);
							setupYoutube();
						}

						function onOver(e)
						{
							playButton.animate({opacity: 0}, 200);
						}

						function onOut(e)
						{
							playButton.animate({opacity: 1}, 200);
						}

						function showVideo(e)
						{
							bumper.css({visibility: "hidden"});
							playOverButton.css({visibility: "hidden"});
							playButton.css({visibility: "hidden"});
							ytPlayer.playVideo();
						}

						setupPlayer = init;
					});

				function onYouTubeIframeAPIReady()
				{
					console.log("kickoff");
					setupPlayer();
				}
			</script>
			<div id="yt-player"></div>
			<img src="images/bumper.jpg" class="bumper" />
			<a href="#" ><img src="images/play_over.png" class="over" />
			<img src="images/play.png" class="normal"/></a>
		</div>
	</div>
</body>
</html>