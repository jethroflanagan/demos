package za.co.nativecode.tmon.youtubeplayer.model.global 
{
	/**
	 * @author Bradley Botha
	 */
	public class Colors 
	{
		static public const BLACK:uint = 0x0;
		static public const WHITE:uint = 0xFFFFFF;
	}

}