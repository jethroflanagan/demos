package za.co.nativecode.tmon.youtubeplayer.context 
{
	import flash.display.DisplayObjectContainer;
	import org.robotlegs.mvcs.Context;

	/**
	 * @author Jethro Flanagan
	 */
	public class ApplicationContext extends Context
	{
		public function ApplicationContext(contextView:DisplayObjectContainer = null, autoStartup:Boolean = true)
		{
			super(contextView);
		}
		
		override public function startup():void
		{
			ContextInjectors.init(injector);
			ContextCommands.init(commandMap);
			ContextMediators.init(mediatorMap);
		}
	}
	
}