	<div class="clearfix tweet" onClick="_gaq.push(['_trackEvent', 'Job', 'Job-share-twitter', 'Job-share-<JOB TITLE>']);">
	    <a href="https://twitter.com/share" class="twitter-share-button" data-count="none" data-url="<%= ApplicationManager.getBaseURL(app.getName()) + request.getRequestURI() + "?" + request.getQueryString() %>" data-text="<ccs:schemeAttribute name="Title" /> via @NATIVE">Tweet</a>
	</div>
	<div class="clearfix gplus" onClick="_gaq.push(['_trackEvent', 'Job', 'Job-share-googleplus', 'Job-share-<JOB TITLE>']);">
	    <div class="g-plusone" data-size="medium"></div>
	</div>
	<div class="clearfix fb-like" onClick="_gaq.push(['_trackEvent', 'Job', 'Job-share-facebook', 'Job-share-<JOB TITLE>']);">
	    <div class="fb-like" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false"></div>
	</div>
