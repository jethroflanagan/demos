define([
	box.lib('backbone'),
	box.lib('lodash'),
	box.lib('jquery-nc')

	//project
	box.pkg('event/app_events')
],
function (Backbone, _, $, AppEvents) {
	var UiModel = Backbone.Model.extend({

		initialize: function () {
			_.bindAll(this);
		}
	});
});