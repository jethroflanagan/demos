define([],
function () {
	var type = new box.event.EventType('app event');
	var e = box.event.createEvent(type);
	return {
		set: e('set'),
		show: e('show'),
		hide: e('hide')
	};
});