package za.co.nativecode.tmon.youtubeplayer.model.util 
{
	import com.greensock.TweenMax;
	/**
	 * @author Jethro Flanagan
	 */
	public class Timing 
	{
		
		static public function delayedFunction(fn:Function, args:Array, time:Number, useFrames:Boolean = false):void
		{
			TweenMax.delayedCall(time, fn, args, useFrames);
			/*setTimeout(function():void{
				fn.apply(this, args);
			}, time * 1000 / (useFrames? 30: 1));*/
		}
		
		static public function killDelayedFunction(fn:Function):void
		{
			TweenMax.killDelayedCallsTo(fn);
		}


	}

}