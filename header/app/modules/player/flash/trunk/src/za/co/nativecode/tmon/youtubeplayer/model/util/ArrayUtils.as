package za.co.nativecode.tmon.youtubeplayer.model.util 
{
	import za.co.stonewall.utils.Tracer;
	/**
	 * @author Bradley Botha
	 */
	public class ArrayUtils 
	{
		
		/**
		 * 
		 * @param	arr:		Vector or Array
		 * @param	properties	name-value pairs	
		 * @param	value
		 * @return
		 */
		public static function indexOfProperty(arr:*, properties:Object):int
		{
			if(!(arr is Array || arr is Vector.<*>))
				throw new Error("Must be of type Vector or Array");
			for(var i:int = 0; i < arr.length; i++)
			{
				var isFound:Boolean = true;
				for(var prop:String in properties)
				{
					if(arr[i][prop] != properties[prop])
					{
						isFound = false;
					}
				}
				if(isFound)
					return i;
			}
			return -1;
		}
		
		static public function toArray(data:*):Array 
		{
			var arr:Array = [];
			if(data != null)
			{
				data = Vector.<*>(data);
				for(var i:int = 0; i < data.length; i++)
				{
					arr.push(data[i]);
				}
			}
			return arr;
		}
		
		static public function randomise(arr:*):*
		{
			if (arr == null)
				return null;
			
			var rand:Array = [];
			var newArr:* =  new (ClassUtils.getClass(arr))();
			var i:int;
			
			for (i = 0; i < arr.length; i++)
				rand.push(i);
				
			for (i = 0; i < arr.length; i++)
				newArr.push(arr[rand.splice(int(Math.random() * rand.length), 1)[0]]);
			
			return newArr;
		}
	}

}