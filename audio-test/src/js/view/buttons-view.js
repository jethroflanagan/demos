__DEFAULT_IMPORTS__

var ButtonEvents = require('event/button-events');

var ButtonsView = Backbone.View.extend({

	events: {
		'click .sound-button': 'toggleButton',
		'change .volume-control': 'setVolume',
	},

	initialize: function () {
		Binder.allExcept(this);
	},

	toggleButton: function (e) {
		var $button = $(e.target);
		var on = 'is-on';
		var journeyId = null;
		var soundId = null;
		var filterId = null;
		var isFilter = false;
		var instanceId = null;

		if ($button.data('sound')) {
			soundId = $button.data('sound');
		}
		if ($button.data('journey')) {
			journeyId = $button.data('journey');
		}
		if ($button.data('filter')) {
			filterId = $button.data('filter');
			isFilter = true;
		}
		if ($button.data('instance')) {
			instanceId = $button.data('instance');
		}

		if ($button.hasClass(on)) {
			$button.removeClass(on);
			if (journeyId) {
				Event.dispatch(ButtonEvents.STOP_JOURNEY, { journeyId: journeyId });
			}
			else if (!isFilter) {
				Event.dispatch(ButtonEvents.TURN_OFF, {soundId: soundId, instanceId: instanceId});
			}
			else {
				Event.dispatch(ButtonEvents.WINDOW_DOWN, {soundId: soundId, filterId: filterId, instanceId: instanceId});
			}
		}
		else {
			$button.addClass(on);
			if (journeyId) {
				Event.dispatch(ButtonEvents.START_JOURNEY, { journeyId: journeyId });
			}
			else if (!isFilter) {
				Event.dispatch(ButtonEvents.TURN_ON, {soundId: soundId, instanceId: 'scene'});
			}
			else if (filterId) {
				Event.dispatch(ButtonEvents.WINDOW_UP, {soundId: soundId, filterId: filterId, instanceId: instanceId});
			}
		}

	},

	setVolume: function (e) {
		var $volume = $(e.target);
		var soundId = $volume.data('sound');
		instanceId = $volume.data('instance');
		Event.dispatch(ButtonEvents.SET_VOLUME, {soundId: soundId, volume: $volume.val() / 100, instanceId: instanceId});
	},

	setStatus: function (message) {
		var $log = $('[data-js="status"]');
		$log.text(message + '\n' + $log.text());
	},

	render: function() {
	}

});

return ButtonsView; 