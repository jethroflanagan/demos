<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->

<c:set var="baseUrl" value="<%= ApplicationManager.getBaseURL(app.getName())%>"></c:set>

<%
	String homeTitle = "Home";
	String typeParam = request.getParameter("type");
	// not just setting home title to type because of casing
	if ("Blog".equalsIgnoreCase(typeParam)) {
			homeTitle = "Blog";
	} else if ("Work".equalsIgnoreCase(typeParam)) {
			homeTitle = "Work";
	} else if ("People".equalsIgnoreCase(typeParam)) {
			homeTitle = "People";
	} else if ("Jobs".equalsIgnoreCase(typeParam)) {
			homeTitle = "Jobs";
	}

	String keywordParam = request.getParameter("keyword");
	if (keywordParam != null && keywordParam.length() > 0) {
			homeTitle = "Search Results";
	}

	request.setAttribute("homeTitle", homeTitle);
%>

<cs:head descriptionContentFields="Title">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=0.5, maximum-scale=1.0, minimum-scale=0.5">

	<title>Native - <ccs:pageTitle titleField="Title" showDetail="true" detailTitleFields="Title"/></title>
	<meta name="description" content="">
	<link rel="stylesheet" href="${templatePath}css/style.css">
	<link rel="shortcut icon" href="http://tmon.native.co.za/native/applications/native/templates/img/favicon.gif" type="image/x-icon">
	<!--
	<script src="${templatePath}js/vendor/modernizr-2.5.3.min.js"></script>
	<script src="${templatePath}js/vendor/respond.src.js"></script>
	-->
	<script data-main="${templatePath}js/config" src="${templatePath}js/vendor/require.js"></script>
	<script defer type="text/javascript" src="http://use.typekit.com/cse4ffs.js"></script>
	<style type="text/css">
		.slider
		{
			overflow: visible !important;
		}
		.slider h1
		{
			width: 820px !important;
			max-width: 820px !important;
			font-size: 72px !important;
			font-weight: 900 !important;
		}
		#banner li.bnnr-landing h1
		{
			margin-top: 4px !important;
			text-shadow: 0px -2px 1px #000 !important;
		}
		#banner li.bnnr-landing p
		{
			width: 600px !important;
			max-width: 600px !important;
			text-shadow: 1px 1px 0px #222 !important;
			margin-top: -14px !important;
		}

	</style>
</cs:head>