define([
	box.lib('backbone'),
	box.lib('lodash'),
	box.lib('jquery-nc'),

	//project
	box.pkg('model/capability'),
	box.pkg('event/block_events')
],
function (Backbone, _, $, Capability, BlockEvents) {
	var COL_SIZE = 106;
	var ROW_SIZE = 106;
	var TILE_INCREMENT = 1; // added whenever a tile increases size, per increase
	var CONTENT_PADDING = 9; // from the css
	var CONTENT_TIME = 600;
	var BACKGROUND_TIME = 300;

	// TODO Use js hooks for getting DOM
	var GridBlock = Backbone.View.extend({
		model: null,
		el: null,
		$el: null,
		width: null,
		height: null,
		direction: null,
		timeouts: null,
		animationTimeouts: null,
		isShown: null,
		$contentHider: null,

		initialize: function (params) {
			_.bindAll(this);
			this.timeouts = [];
			this.animationTimeouts = [];
			this.$el.appendTo(params.container);

			// force it to use the jquery-nc version, instead of whatever it's using
			this.$el = $(this.$el);

			this.setPosition(params.col, params.row);

			this.width = this.getColPosition(params.numCols);
			this.height = this.getColPosition(params.numRows);
			this.direction = params.direction;

			this.$contentHider = this.$el.find('.block-hider');
			this.$contentHider.width(this.width);
			this.$contentHider.height(this.height);

			this.$el.width(this.width);
			this.$el.height(this.height);

			// force setting this for animation later
			var $content = this.$el.find('.block-content');
			$content.width(this.width - CONTENT_PADDING * 2)

			this.$el.css({ display: 'none' });
			if (!Capability.isLtIE9()) {
				this.$el.css({ opacity: 0 });
			}

			this.prepContent();
			box.event.addListener(null, BlockEvents.show, this.onShow);
			//this.show();
		},

		setDelay: function (fn, delay) {
			var id = setTimeout(fn, delay);
			this.timeouts.push(id);
			return id;
		},

		clearDelay: function (id) {
			var index = this.timeouts.indexOf(id);
			if (index > -1)
				this.timeouts.splice(index, 1);
			clearTimeout(id);
		},

		addAnimation: function (fn) {
			var id = requestAnimationFrame(fn);
			this.animationTimeouts.push(id);
			return id;
		},

		cancelAnimation: function (id) {
			var index = this.animationTimeouts.indexOf(id);
			if (index > -1)
				this.animationTimeouts.splice(index, 1);
			cancelAnimationFrame(id);
		},

		getColPosition: function (col) {
			if (!col)
				col = 1;
			return col * (COL_SIZE + TILE_INCREMENT) - TILE_INCREMENT;
		},

		getRowPosition: function (row) {
			if (!row)
				row = 1;
			return row * (ROW_SIZE + TILE_INCREMENT) - TILE_INCREMENT;
		},

		setPosition: function (col, row) {
			this.$el.css({left: this.getColPosition(col), top: this.getRowPosition(row)});
		},

		/**
		 * Animate the content in if necessary. Runs after a block is fully animated in
		 * @override
		 */
		showContent: function () {
		},

		/**
		 * Prepare the content in if necessary, runs before an element animates in
		 * @override
		 */
		prepContent: function () {
		},

		hide: function (delay) {
			if (!delay)
				delay = 0;
			var time = 300;

			//this.$el.delay(delay).animate({ marginTop: this.height }, time);
			this.$contentHider.delay(delay).animate({
				//height: 0,
				top: this.height
			},
			{
				duration: time,
				complete: this.dispose
			});
			return delay + time;
		},

		onShow: function (e) {
			if (e.data.block == this)
				this.show();
		},

		show: function () {
			// prevent multiple calls
			if (this.isShown)
				return;
			this.isShown = true;
			//this.$el.css({ opacity: 1 });
			this.$el.animate({ opacity: 1 }, 500);
			var offset = 50;
			switch (this.direction) {
				case 'up':
					direction = 'top';
					break;
				// use 'top' with offset * -1 because it animates correctly
				case 'down':
					direction = 'top';
					offset *= -1;
					break;
				// use 'left' with offset * -1 because it animates correctly
				case 'right':
					direction = 'left';
					offset *= -1;
					break;
				case 'left':
				default:
					direction = 'left';
					break;
			}
			var from = {};
			var to = {};
			from[direction] = offset;
			to[direction] = 0;

			// make visible
			this.$el.css({ display: '' });
			//console.log('grid_v', this.$el.jquery);


			this.$el.find('.block-content').css(from);
			this.$el.find('.block-content').animate(to, CONTENT_TIME, 'easeOutBack');

			this.$el.find('.block-background').css(from);
			this.$el.find('.block-background').animate(to, BACKGROUND_TIME, 'easeOutQuad');

			// only show or run content once it's animated in to be kind to slower browsers (e.g. Safari, IE)
			this.setDelay(this.showContent, Math.max(CONTENT_TIME, BACKGROUND_TIME) + 1000);
		},

		// make sure to call super in any override
		dispose: function () {
			while (this.animationTimeouts.length > 0) {
				cancelAnimationFrame(this.animationTimeouts.pop());
			}
			while (this.timeouts.length > 0) {
				clearTimeout(this.timeouts.pop());
			}
		}
	});

	return GridBlock;
});