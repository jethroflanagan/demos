require.config({

    baseUrl: 'js',
    
    paths: {
        jquery: 'vendor/jquery',
        underscore: 'vendor/underscore',
        backbone: 'vendor/backbone',
        SoundJS: 'vendor/soundjs-0.5.2.combined',
        TweenMax: 'vendor/TweenMax'
    },

    deps: ['main'],

    shim: {
        SoundJS: {
            exports: 'SoundJS'
        },
        backbone: {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        underscore: {
            exports: '_'
        },
        jquery: {
            exports: '$'
        },
        TweenMax: {
            exports: 'TweenMax'
        }
    }
});
