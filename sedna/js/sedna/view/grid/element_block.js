define([
	box.lib('backbone'),
	box.lib('lodash'),
	box.lib('jquery-nc'),

	//project
	box.pkg('view/grid/number_block')
],
function (Backbone, _, $, NumberBlock) {
	var _super = NumberBlock.prototype;

	var ElementBlock = NumberBlock.extend({
		current: null,
		target: null,

		initialize: function (params) {
			_.bindAll(this);
			_super.initialize.apply(this, [params]);
		},

		showContent: function () {
			_super.showContent.apply(this);
		}

		// @Feedback TODO Use Hollard timing, make 3rd block faster
		//countUp: function () {
		//}
	});

	return ElementBlock;
});