/**
 * Define extra vendor files to include as necessary
 * Use `"vendor name": ["list", "of", "files"]`
 */
module.exports = {
	'gsap': ['vendor/gsap/src/uncompressed/easing/EasePack.js'],
}