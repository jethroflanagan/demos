package za.co.nativecode.tmon.youtubeplayer
{
	import flash.display.LoaderInfo;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.external.ExternalInterface;
	import flash.system.Capabilities;
	import za.co.nativecode.tmon.youtubeplayer.context.ApplicationContext;
	import za.co.nativecode.tmon.youtubeplayer.model.global.App;
	import za.co.nativecode.tmon.youtubeplayer.view.YouTubePlayer;

	
	/**
	 * @author Bradley Botha
	 * @version 1.0
	 * @see
	 */
	[Frame(factoryClass="za.co.nativecode.tmon.youtubeplayer.Preloader")]
	public class Main extends Sprite
	{
		
		private var _context:ApplicationContext;
		
		/**
		* Constructor
		*/
		public function Main():void 
		{
			App.mode = (Capabilities.playerType == "StandAlone" ? App.LOCAL : App.LIVE);
			if (stage) 
				init();
			else 
				addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			// 
			removeEventListener(Event.ADDED_TO_STAGE, init);
			_context = new ApplicationContext(this);
			
			var flashVars:Object = LoaderInfo(this.root.loaderInfo).parameters;
			var videoID:String = flashVars["videoID"];
			
			
			// Useful for testing the swf, when not embeded in the browser
			/*if (ExternalInterface.available)
				ExternalInterface.call("console.log", "videoID:", videoID);
			else
				videoID = "8JDDNPyvqpE";*/
			
			
			addChild( new YouTubePlayer(videoID) );
		}
	}	
}