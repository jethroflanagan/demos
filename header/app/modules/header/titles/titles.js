define(
	[
		'jquery',
		'header/core',
		'header/animation/ease',
		'header/triangles/shapes'
	],
	function ($, core, ease, shapeLib)
	{
		var HEADER_LARGE_FONT_SIZE = "72px"; //taken from the css
		var HEADER_HEIGHT = 545; //taken from the css

		//neaten up and swap these into constant style names
		var slideShowTime = 2000;
		var letterFadeInTime = 500;
		var letterFadeOutTime = 300;
		var lettersFadeTime = 500; //excludes the time for letterFadeInTime|letterFadeOutTime. This is the time over which all letters will fade in
		var slugFadeTime = 2000;
		var linkNotifierTime = 500;
		var slugFadeInDelay = 2000;
		var iconChangeDelay = 500;
		var elementTime = 1000;

		var titles;
		var title;
		var titleHolder;
		var slug;
		var icon;
		var index;
		var current;
		var triangles;
		var showShape;
		var isShapeReady = false;
		var isTitlesReady = false;
		var headerArea;
		var iconImg;
		var iconImgLink;
		var isTrianglesHidden;
		var destructionTime = 0;
		var showLetterTimeout;
		var ieVersion = core.getIE_Version();
		var linkToOpen; //not always current.link - controlled by setLink
		var headerLink; //element
		var pauseCallback;
		var isPaused = false;

		function init(header, titleTarget, slugTarget, iconTarget, triangleHolder, isTrianglesHiddenCallback)
		{
			isTrianglesHidden = isTrianglesHiddenCallback;
			triangles = triangleHolder;

			triangles.setShapeReconstructedCallback(setShapeReady);
			setupTitles();

			headerArea = $(header);
			title = $(titleTarget, header);
			headerLink = $("#header-link", header);
			titleHolder = title.parent();
			slug = $(slugTarget, header);
			icon = $(iconTarget, header);
			icon.css("display", "inline");
			index = 0;
			current = titles[index];

			icon.removeClass("banner-mark");
			//icon.append('<img src="${templatePath}/img/banner/mark.png" />');
			iconImg = $("img", icon).first();
			//iconImgLink = $("img", icon).last();
			var iconOffset = "30px";
			iconImg.css({marginTop: iconOffset, display: "inline"});
			//iconImgLink.css({opacity: 0, position: 'absolute', left:'0px', top: iconOffset});

			//slug.css({opacity: 0});
			//icon.css({opacity: 0, marginLeft: -150});
			slug.css({overflow: "hidden", lineHeight: "20px", marginTop: 0});

			if (core.hasCanvasSupport())
			{
				showShape = triangles.showShapeId;
			}
			else
			{
				showShape = triangles.showImage;
			}


			show(true);
			//show();
		}

		function setupTitles()
		{
			var slider = $("#triangleslider", "#triangles");
			var slides = slider.children();
			titles = [];
			for (var i = 0; i < slides.length; i++)
			{
				titles.push(
					{
						title: $("h1", slides[i]).text(),
						slug: $("p", slides[i]).text(),
						link: $("a", slides[i]).attr("href"),
						shape: shapeLib.shapeIds[$("img", slides[i]).data("shape")],
						image: $("img", slides[i]).data("src")
					}
				);
			}
		}

		/*function openLink(e)
		{
			//window.location.href = linkToOpen;
			//headerLink
		}*/

		function show(firstTime)
		{
			destructionTime = showShape((core.hasCanvasSupport() ? current.shape : current.image));
			if (isNaN(destructionTime))
				destructionTime = 0;
			setTimeout(function(){
			var slugline = current.slug;
			if (!firstTime)
			{
				charsToSpan(title, current.title);
				if (ieVersion != null && ieVersion < 9) //it's over here instead of a more logical place due to ie issues on 7 and 8
				{
					title.stop().animate({opacity: 0}, letterFadeOutTime);
					setTimeout(function()
						{
							title.html(current.title);
							title.animate({opacity: 1}, letterFadeInTime);
							resize();
						}, letterFadeOutTime + lettersFadeTime);
				}
				slug.stop().delay(lettersFadeTime).animate({opacity: 0}, {duration: letterFadeOutTime, easing: "swing"});

				setTimeout(function()
					{
						setLink();

						slug.html(slugline);
						slug.animate({opacity: 1}, {duration: slugFadeTime, easing: "swing"});
					}, letterFadeOutTime + lettersFadeTime);

				icon.delay(iconChangeDelay).animate({opacity: 1, marginLeft: -10}, {duration: elementTime, easing: "swing"});
			}
			else
			{
				setLink();

			}
			setTimeout(function()
				{
					isTitlesReady = true;
					next();
				}, slideShowTime);
			}, destructionTime);
		}

		function setLink()
		{
			linkToOpen = current.link;
			//headerArea.unbind("click");
			//if (current.link != undefined)
			//	headerArea.click(openLink);
			headerLink.attr("href", linkToOpen);
		}

		function next()
		{
			if (isPaused)
			{
				pauseCallback = function()
					{
						next();
					}
				return;
			}
			//return;
			if (isTitlesReady && (isShapeReady || isTrianglesHidden()))
			{
				isShapeReady = false;
				isTitlesReady = false;

				var hideTime = 500;
				if (core.hasCanvasSupport())
					showShape = triangles.destructToShapeId;
				//titleHolder.stop().delay(hideTime).animate({height: 0}, {duration: elementTime, easing: "swing"});
				if (++index > titles.length - 1)
					index = 0;
				current = titles[index];
				hideTime += elementTime;
				setTimeout(show, hideTime);
			}
		}

		function setShapeReady()
		{
			isShapeReady = true;
			next();
		}

		function charsToSpan(field, chars)
		{
			//field.html("");
			//var fadeMethod = fadeLetter;
			//ie freaks out and can only do letter fades after a few attempts.
			//Even tried resorting to visibility switches and that wouldn't help.

			if (ieVersion != null && ieVersion < 9)
			{
				//field.animate({opacity: 0}, letterFadeOutTime);
				//field.html("");
								//field.html(chars);

			}
			else
			{
				var len = field.children().length;
				for (var i = 0; i < len; i++)
				{
					var letter = (i < chars.length ? chars[i] : " ");
					/*if (letter == " ")
						letter = "&nbsp;";*/
					var span = $('.letter_' + i, field);
					if (span.length > 0)
						span.delay(Math.random() * lettersFadeTime).stop().animate({opacity: 0}, letterFadeOutTime);
					/*if(chars[i] == " ")
						span.css({width: spaces});
					else
						span.css({width: ''});*/
					//span.delay(Math.random() * 2000).animate({width: wd},  500, 'swing');
					//var anim = setInterval(function()
					//	{
					//	}, 100);

				}
			}
			setTimeout(function()
				{
					showLetters(field, chars)
				},  lettersFadeTime);
			//field.each(function(){$(this).
		}

		/*function fadeLetter(span)
		{
			span.delay(Math.random() * lettersFadeTime).stop().animate({opacity: 0}, letterFadeOutTime);
		}*/


		/*
		//can't fade opacity on spans
		function ieFadeLetter(span)
		{
			setTimeout(function()
				{
					span.css({visibility: "hidden"})
				}, Math.random() * lettersFadeTime);
		}*/

		function fadeLetterIn(span)
		{
			span.css({opacity: 0});
			span.delay(Math.random() * lettersFadeTime).animate({opacity: 1}, letterFadeInTime);
		}

		/*function ieFadeLetterIn(span)
		{
			span.css({visibility: "hidden"})
			setTimeout(function()
				{
					span.css({visibility: "visible"});
				}, Math.random() * lettersFadeTime);
		}*/

		function showLetters(field, chars)
		{
			var fadeMethod = fadeLetterIn;
			if (ieVersion != null && ieVersion < 9)
			{
				//field.html(chars);
				//field.animate({opacity: 1}, letterFadeInTime);
				return;
			}
			else
			{
				field.html("");
				var len = chars.length;
				for (var i = 0; i < len; i++)
				{
					var letter = chars[i];
					var character = "<span class='letter_" + i + "'>" + letter + "</span>";

					field.append(character);
					var span = $('.letter_' + i, field);
					fadeMethod(span);
				}
				var ht = getTitleHolderHeight();
				if (isNaN(ht) || ht == undefined)
				{
					clearTimeout(showLetterTimeout);
					showLetterTimeout = setTimeout(showLetters, 10);
					return;
				}
				titleHolder.animate({height: ht}, elementTime);
			}

		}

		function getTitleHolderHeight()
		{
			return title.height() + (!isLargeHeader() ? 15 : 35);
		}

		function resize()
		{
			var ht = getTitleHolderHeight();
			if (isNaN(ht) || ht == undefined)
			{
				clearTimeout(showLetterTimeout);
				showLetterTimeout = setTimeout(resize, 10);
				return;
			}
			titleHolder.stop().animate({height: ht}, elementTime);
			adjustHeight(ht);
		}

		function isLargeHeader()
		{
			return title.css("fontSize") == HEADER_LARGE_FONT_SIZE;
		}

		function adjustHeight(ht)
		{
			var gotoHt;
			if (isTrianglesHidden())
				gotoHt = ht + 270;
			else if (!isLargeHeader())
				gotoHt = HEADER_HEIGHT - 150;
			else
				gotoHt = HEADER_HEIGHT;
			headerArea.stop().animate({height: gotoHt}, 400);
		}

		function pause()
		{
			isPaused = true;
		}

		function play()
		{
			isPaused = false;
			if (pauseCallback != null)
				pauseCallback();
			pauseCallback = null;
		}

		return {init: init, resize: resize, play: play, pause: pause};
	});