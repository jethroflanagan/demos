define([],
function () {
	var type = new box.event.EventType('grid event');
	var e = box.event.createEvent(type);
	return {
		show: e('show'),
		hide: e('hide'),
		ready: e('ready'),
		navigateTo: e('navigateTo'),
		showAhead: e('show ahead'), // user requests a block to show ahead
		showScene: e('showScene'),
		loadScene: e('loadScene')
	};
});