(function () {

	/**
	 * Read a parameter from the current URL
	 */
	box.getUrlPar = function (name) {
		/**
		 * pulls out the value of a parameter from a query string par1=123&par2=12345
		 */
		function getUrlParam(path) {
			var vars = [], hash;
			var u = path;
			if (u.indexOf("#") > 0) {
				u = u.substring(0, u.indexOf("#"));
			}
			var hashes = u.slice(u.indexOf('?') + 1).split('&');
			for (var i = 0; i < hashes.length; i++) {
				hash = hashes[i].split('=');
				vars.push(hash[0]);
				vars[hash[0]] = hash[1];
			}
			return vars;
		}

		return getUrlParam(window.location.href)[name];
	}


	box.scriptLoaded();
})();