__DEFAULT_IMPORTS__

var SoundEngineEvents = require('event/sound-engine-events');
var StatusEvents = require('event/status-events');
var SoundLibrary = require('model/sound/sound-library');
var ArrayUtils = require('util/array-utils');
/**
 * DEMO
 * car off
 * turn on
 * engine noise
 * forest noises
 * thunder
 * windscreen wipers
 * indicator
 * gravel
 * fade out forest
 * fade in ocean
 */

var Journey = Backbone.Model.extend({
/**
 * Describe a sequence of audio files playback. 
 * A Journey doesn't work like a normal timeline, so it can't simply track
 * time as it progresses and fire off the next audio layer
 * 
 * 
 * A sequence is a list of layers (each audio track), which can contain 
 * samples (parts of an audio track).
 * ```
 *	{
 *		layerId: 'intro',		// {string}		(optional) Used for referring to
 *								//				a layer from other layers, for timing or
 *								//				stopping a layer
 *												
 *		soundId: 'engineOn',	// {string}  	(optional if using `stop`)
 *		                    	//             	Name of the id in sounds
 *		                    	             	
 *		samples: [          // {object[]}		(optional) list of sections to play.
 *							//					If left out, it plays the full file.
 *							//					If multiple examples are available,
 *							//					the order of playback will always
 *							//					be random. If specific order is needed
 *							//					use multiple layers with the same soundId
 *							//					instead.
 *		    {       	        
 *		        from: 0,	// {float}		time in seconds
 *		        to: 10, 	// {float}		time in seconds
 *		        
 *		        repeat: 0	// {int=0}		(optional) -1 repeats indefinitely, 
 *		        			//				0 means no repeats. Any other number
 *		        			//				is how many times the sample plays for again
 *		        			//				e.g. 1 means it plays *twice* (repeats once)
 *		    },
 *		],
 *		fadeIn: 0          // [{int|object}]
 *						   // fade in time, in seconds
 *		                   // Can also use: {min: int, max: int} 
 *		                   // for variable numbers
 *		                                  		
 *		fadeOut: 0         // {int|object}		(optional) fade out time. If fadeIn
 *		                                  		time of following track is not the
 *		                                  		same, then:
 *		                                  			fadeOut is longer: 
 *		                                  				next clip starts fading in, this 
 *		                                  				fades out (next clip is fully faded 
 *		                                  				in while this is still fading out.
 * 
 *													fadeOut is shorter:
 *														next clip starts fading in, this
 *														clip only begins to fade when 
 *														time remaining for fade in is 
 *														equal to fade out time.
 *
 *		         		               		Goal is to have audio as
 *		         		               		much as possible (less silence)
 *		         		               		e.g 1:
 *		         		               			{id: 'a', fadeOut: 1},
 *		         		               			{id: 'b', fadeIn: 3}
 *		         		               			'b' starts fading in. After 2
 *		         		               			seconds 'a' starts fading out.
 *		         		               			'a' is faded out fully just as
 *		         		               			'b' is fully faded in.
 *		         		               		e.g 2:
 *		         		               			{id: 'a', fadeOut: 3},
 *		         		               			{id: 'b', fadeIn: 1}
 *		         		               			'a' starts fading out, and at
 *		         		               			the same time, 'b' starts fading 
 *		         		               			in. 'a' completes fade out 2
 *		         		               			seconds after 'b' is done.
 *		         		               			
 *		         		               		Can also use: {min: int, max: int} 
 *		         		               		for variable numbers
 *
 *		startTime: 0,	// {float|object}	(optional, in seconds) The time to 
 *		             	                 	start playback (in terms of the 
 *		             	                 	start of the whole sequence).
 *		             	                 	Can also use: {min: int, max: int} 
 *		             	                 	for variable numbers
 *
 *		endTime: 0,		// {float|object}	(optional, in seconds) The time to
 *		           		                 	stop playback, regardless of where
 *		           		                 	we are in the sections
 *		           		                 	Can also use: {min: int, max: int} 
 *		           		                 	for variable numbers
 * 
 *		startAfter: 'door',	// {string}		(optional) the id of another layer. 
 *											This layer will begin playback after
 *											the mentioned layer has finished.
 *											Any delay will add to the wait time.
 *											Cannot be used in conjuntion with
 *											`startWith`
 *
 *		startWith: 'door',	// {string}		(optional) the id of another layer. 
 *											This layer will begin playback at 
 *											the same time as the mentioned layer, 
 *											first waiting for the specified delay 
 *											to elapse.
 *											Cannot be used in conjuntion with
 *											`startAfter`
 *
 *		delay: 0,	// {float|object}		(optional, in seconds) Time to start 
 *											after the the previous layer starts (e.g.  
 *		              	                 	previous plays for 3 seconds, so using 1
 *		              	                 	means that this clips starts 1 second
 *		              	                 	after the previous clip, which continues
 *		              	                 	playing for another 2 seconds.
 *		              	                 	Can also use: {min: int, max: int} 
 *		              	                 	for variable numbers
 *		              	                 	
 *		repeat: 0,		// {int}			(optional) -1 repeats indefinitely, 
 *		        		//					0 means no repeats. Any other number
 *		        		//					is how many times the sample plays
 *      volume: 1, 		// {float}			(optional, default=1) 0 to 1. set the 
 *      									volume to the specified level.
 *
 *		stop: ['footsteps']	// {string[]}	(optional) ignored if false. Stops all
 *		                   	             	soundIds in the list. Uses fadeOut time
 *		                   	             	if specified.
 * 
 * 
 *	},
 *	```
 */
	name: null, 		// the name of the journey (required for naming audio instances)

	index: 0, 			// where we are in the sequence

	sequence: null,		// override this

	activeLayers: null, // currently playing layers
	// nextLayers: null,	// layers that haven't begun playing yet (optimisation)

	initialize: function () {
		Binder.allExcept(this);
		if (!this.name)
			console.error('Name not defined');
		if (!this.sequence)
			console.error('Sequence not defined');
		this.activeLayers = []; 

		// setup all necessary extra data for the layers 
		for (var i = 0, len = this.sequence.length; i < len; i++) {
			var layer = this.sequence[i];

			// skip control layers
			if (layer.stop) {
				continue;
			}

			// works normally for layers without samples or loops. Modified for layers with samples, based on loops, etc.
			layer.playbackProgress = 0;

			layer.duration = SoundLibrary.files[layer.soundId].duration;
			layer.isPlaying = false;

			// layerIds are required for tracking sound instances, so this will generate one if necessary
			if (!layer.layerId) { 
				layer.layerId = layer.soundId + '::' + i;
			}

			// every sound needs a unique instance id. Double colons just so it's easier to read in the console
			layer.instanceId = this.name + '::' + layer.layerId + '::' + new Date().getTime();

			if (layer.samples) {
				layer.sampleIndex = 0;
				// reset sample counter
				for (var k = 0, numSamples = layer.samples.length; k < numSamples; k++) {
					var sample = layer.samples[k];
					sample.repeatCount = 0;

					// add in repeat in case
					if (!sample.repeat) {
						sample.repeat = 0;
					}
				}
			}
			this.setupRelatedLayers(layer);
			
			// @TODO this is kinda cludgy
			// Setup related layers so that lookAheads and scheduling is optimised
			var relatedLayer = null;
			if (layer.startWith) {
				relatedLayer = this.getLayerById(layer.startWith);
			}
			if (layer.startAfter) {
				relatedLayer = this.getLayerById(layer.startAfter);
			}
			if (relatedLayer) {
				// need to do this in case the relatedLayer is out of sequence
				this.setupRelatedLayers(relatedLayer);

				// Add layer to next of related layer
				// Layers can only startWith or startAfter another layer, but one layer may 
				// have multiple layers startingWith and startingAfter it
				if (layer.startWith) {
					relatedLayer.next.startWith.push(layer); 
				}
				else if (layer.startAfter) {
					relatedLayer.next.startAfter.push(layer);
				}
			}
		}
	},

	setupRelatedLayers: function (layer) {
		if (!layer.next) {
			layer.next = {
				startWith: [], // all layers that will start at the same time as this
				startAfter: [], // all layers that will start after this is complete
			};
		}
	},

	start: function () {
		var layer = this.sequence[0];
		this.addToActiveLayers(layer);
	}, 

	/**
	 * Layers are added into the system based on a current layer in the system.
	 * Checks if a layer should be scheduled into the system based on current time
	 * 
	 * @param  {object} layer from sequence that is playing
	 */
	lookAhead: function (layer) {
		// skip this if not needed
		if (layer.next.startWith.length == 0 && layer.next.startAfter.length == 0) {
			return;
		}

		var time = this.getPlaybackProgress(layer);
		var duration = layer.duration;
		// only look at potential next layers (optimisation)
		var nextLayers = layer.next.startWith;
		for (var i = 0, len = nextLayers.length; i < len; i++) {
			var nextLayer = nextLayers[i];
			if (nextLayer.startWith == layer.layerId) {
				if (!nextLayer.delay || nextLayer.delay <= time) {
					layer.next.startWith.splice(nextLayer, 1); // remove it from next layers
					this.addToActiveLayers(nextLayer);
					// no break because multiple layers may start with this layer
				}
			}
		}
	},

	removeCompleteLayer: function (completeLayer) {

		this.activeLayers.splice(this.activeLayers.indexOf(completeLayer), 1);
		completeLayer.isPlaying = false;

		var sound = {
			soundId: completeLayer.soundId,
			instanceId: completeLayer.instanceId,
		};
		Event.dispatch(StatusEvents.SET, { message: 'Removing layer: ' + completeLayer.layerId });

		// in case it's still playing, e.g. for loops
		Event.dispatch(SoundEngineEvents.STOP, sound);
		
		var layers = this.sequence;
		// add new layers to the sequence if they are affected by this layer
		for (var i = 0, len = layers.length; i < len; i++) {
			var layer = layers[i];
			if (layer.startAfter == completeLayer.layerId) {
				this.scheduleAddToActiveLayers(layer);
			}
		}
	},

	/**
	 * Only schedules based on completion of audio, not progress
	 * 
	 * @param  {Object} layer 	Layer to get added
	 */
	scheduleAddToActiveLayers: function (layer) {
		if (!layer.delay) { // if delay not included, or delay == 0. Ah, the joys of automatic casting
			this.addToActiveLayers(layer);
			return;
		}

		// TODO implement delays getting added (naive approach of setTimeout wouldn't cater for paused audio)
		// Ignores delay for now, but should also consider layer playbackProgress
		this.addToActiveLayers(layer);
	},

	addToActiveLayers: function (layer) {
		// Add only unique layers
		if (!ArrayUtils.pushUnique(this.activeLayers, layer)) {
			return;
		}
		this.playLayer(layer);

		// lookahead to see if new layers need to be added based on this layer
		var layers = layer.next.startAfter;
		for (var i = 0, len = layers.length; i < len; i++) {
			var inactiveLayer = layers[i];
			if (inactiveLayer.startWith == layer.layerId) {
				console.log('schedule', inactiveLayer.layerId,'based on', layer.layerId, '|',inactiveLayer.startWith );
				layer.next.startAfter.splice(layer, 1); // remove it from next layers
				this.scheduleAddToActiveLayers(inactiveLayer);
				// no break because multiple layers may start after this layer
			}
		}
	},

	/**
	 * @todo rename to `runLayer` and point at stopLayer/playLayer
	 *
	 * @param  {object} layer
	 */
	playLayer: function (layer) {
		// control layers
		if (layer.stop) {
			// stop all listed layers
			console.log('stopping list', layer.stop);
			for (var i = 0, len = layer.stop.length; i < len; i++) {
				Event.dispatch(StatusEvents.SET, { message: 'Stopping layer: ' + layer.stop[i] });
				this.stopLayer(this.getLayerById(layer.stop[i]));
			}

			// keep stop layers separate from playing
			return;
		}

		var sound = {
			soundId: layer.soundId,
			instanceId: layer.instanceId,
			fadeTime: layer.fadeIn,
		};

		if (layer.samples) {
			sound.fromTime = layer.samples[0].from;
		}
		layer.isPlaying = true;
		Event.dispatch(SoundEngineEvents.PREPARE, sound);
		// Event.dispatch(SoundEngineEvents.PLAY, sound);

		Event.listen(SoundEngineEvents.PLAYBACK_COMPLETE, this.onPlaybackComplete);
		Event.listen(SoundEngineEvents.PLAYBACK_PROGRESS, this.onPlaybackProgress);

		Event.dispatch(StatusEvents.SET, { message: 'Added layer: ' + layer.layerId });

	},

	/**
	 * @TODO   rename removeCompleteLayer to stopLayer
	 * @alias  removeCompleteLayer
	 * @param  {object} layer 
	 */
	stopLayer: function (layer) {
		this.removeCompleteLayer(layer);
	},

	onPlaybackComplete: function (e) {
		for (var i = 0, len = this.activeLayers.length; i < len; i++) {
			var layer = this.activeLayers[i];
			if (layer.soundId == e.data.soundId && layer.instanceId == e.data.instanceId) {
				this.removeCompleteLayer(layer);
				break;
			}
		}
	},
	
	onPlaybackProgress: function (e) {
		for (var i = 0, len = this.activeLayers.length; i < len; i++) {
			var layer = this.activeLayers[i];
			if (layer.soundId == e.data.soundId && layer.instanceId == e.data.instanceId) {
				if (!layer.isPlaying) {
					console.log('whoops');
					continue;
				}
				var time = e.data.progressSeconds;
				if (layer.samples) {
					var sample = layer.samples[layer.sampleIndex];
					sample.playbackProgress = time;

					if (time >= sample.to) {
						var sound = {
							soundId: layer.soundId,
							instanceId: layer.instanceId,
							fromTime: layer.samples[layer.sampleIndex].from,
						};

						// increment total time every loop. 
						// TODO fix issue where timestamps repeat
						layer.playbackProgress += time;

						// stop playing if complete
						if (sample.repeat > -1) {
							if (++sample.repeat > sample.repeatCount) {
								// TODO handle multiple samples
								this.removeCompleteLayer(layer);
								break;							
							}
						}
						// replay the sample
						Event.dispatch(SoundEngineEvents.PLAY, sound);
					}
				}
				else {
					layer.playbackProgress = time;
				}
				this.lookAhead(layer);
				break;
			}
		}
	},

	/**
	 * @TODO fix bug where time is repeated in loops
	 *
	 * @param  {object} layer
	 * @return {float}  time in seconds
	 */
	getPlaybackProgress: function (layer) {
		if (layer.samples) {
			return layer.playbackProgress + layer.samples[layer.sampleIndex].playbackProgress;
		}
		return layer.playbackProgress;
	},
	
	getLayerById: function (layerId) {
		for (var i = 0, len = this.sequence.length; i < len; i++) {
			if (this.sequence[i].layerId == layerId) {
				return this.sequence[i];
			}
		}
		return null;
	},
});

return Journey;