define([],
function () {
	var type = new box.event.EventType('block event');
	var e = box.event.createEvent(type);
	return {
		show: e('show')
	};
});