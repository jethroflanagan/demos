/**
//TODO: This was a custom clock that was added becuase of new requirments. It was hacked into place with hardcoded values. This must be cleaned up in future.
*/
define([
	box.lib('backbone'),
	box.lib('lodash'),
	box.lib('jquery-nc'),
	'sedna/event/player_events',
	'sedna/event/end_frame_events',
	'sedna/event/grid_events',
	//project
	box.pkg('view/grid/grid_block')
],
function (Backbone, _, $, PlayerEvents, EndFrameEvents, GridEvents, GridBlock) {

	var _super = GridBlock.prototype;

	var MultilineBlock = GridBlock.extend({

		items: 0,
		nextItemIndex: 0,
		showItemInterval: null,

		initialize: function (params) {
			_.bindAll(this);
			_super.initialize.apply(this, [params]);
			this.showItemInterval = (this.$el.data('item-show-time') === undefined) ? 750 : this.$el.data('item-show-time') * 1000;
		},

		prepContent: function () {
			this.items = this.$el.find('.js-multiline-items li');
			$(this.items).hide();
		},

		showContent: function () {
			this.showItem(true);
		},

		showItem: function( immediate ) {
			var self = this;
			var wait = (immediate === true) ? 20 : self.showItemInterval;

			setTimeout(function() {

				$(self.items[self.nextItemIndex]).fadeIn(500);
				self.nextItemIndex++;

				if(self.nextItemIndex <= self.items.length - 1)
					self.showItem();

			}, wait);
		}
	});

	return MultilineBlock;
});