<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->

<c:set var="baseUrl" value="<%= ApplicationManager.getBaseURL(app.getName())%>"></c:set>

<cs:head descriptionContentFields="Title">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=0.5, maximum-scale=1.0, minimum-scale=0.5">
	
	<title><ccs:pageTitle titleField="Title" showDetail="true" detailTitleFields="Title"/> | The Making of Native</title>
	<link rel="stylesheet" href="${templatePath}css/style.css">
	<link type="image/x-icon" href="/native/applications/native/templates/img/favicon.gif" rel="shortcut icon">
	<!--
	<script src="${templatePath}js/vendor/modernizr-2.5.3.min.js"></script>
	<script src="${templatePath}js/vendor/respond.src.js"></script>
	-->
	<script data-main="${templatePath}js/config" src="${templatePath}js/vendor/require.js"></script>
	<script defer type="text/javascript" src="http://use.typekit.com/cse4ffs.js"></script>

	<cs:facebookMeta
			siteName="The Making of NATIVE" 
			titleFieldNames="Title" 
			descriptionFieldNames="Blog Teaser" 
			imageFieldNames="Blog Header Image"
			imageConfName="Blog.Facebook Thumbnail (150x150)"
			contentType="Blog"
			baseUrl="${baseUrl}"
	/>
</cs:head>