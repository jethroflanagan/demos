package za.co.nativecode.tmon.youtubeplayer.context 
{
	import org.robotlegs.core.ICommandMap;
	import za.co.nativecode.tmon.youtubeplayer.event.AnalyticsEvent;
	import za.co.nativecode.tmon.youtubeplayer.command.InitAnalyticsCommand;
	import za.co.nativecode.tmon.youtubeplayer.command.TrackAnalyticsCommand;

	/**
	 * @author Bradley Botha
	 */
	public class ContextCommands 
	{
		
		static public function init(commandMap:ICommandMap):void
		{
			
			//analytics
			//commandMap.mapEvent(AnalyticsEvent.INIT, InitAnalyticsCommand, AnalyticsEvent);
			//commandMap.mapEvent(AnalyticsEvent.TRACK, TrackAnalyticsCommand, AnalyticsEvent);
		}
		
	}

}