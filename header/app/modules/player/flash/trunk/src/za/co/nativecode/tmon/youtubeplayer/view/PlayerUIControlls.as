package za.co.nativecode.tmon.youtubeplayer.view 
{
	import assets.UIControlls;
	import com.greensock.TweenLite;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.display.StageDisplayState;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.utils.clearTimeout;
	import flash.utils.setTimeout;
	import flash.utils.Timer;
	import za.co.nativecode.tmon.youtubeplayer.event.UIControllsEvent;
	import za.co.nativecode.tmon.youtubeplayer.model.global.FontFormatter;
	/**
	 * ...
	 * @author Bradley Botha
	 */
	public class PlayerUIControlls extends Sprite
	{
		private const INACTIVE_TIMEOUT_TIME:int = 3000;
		
		private const HORIZONTAL_PADDING:int = 3;
		private const VERTICLE_PADDING:int = 3;
		private const SCRUBBER_PADDING_LEFT:int = 95;
		private const SCRUBBER_PADDING_RIGHT:int = 162;
		
		private var _uiControlls:UIControlls;
		private var _activeScrubber:Object;
		private var _userInactiveTimeout:int;
		private var _uiControllsHidden:Boolean;
		
		public function PlayerUIControlls() 
		{
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, kill);
			
			creatUI();
			
			addListeners();
			
			// Start the no user action timer (with double the delay the first time around)
			_userInactiveTimeout = setTimeout(handleInactiveTimeutReached, INACTIVE_TIMEOUT_TIME * 2);
			
			onResize(null);
			stage.addEventListener(Event.RESIZE, onResize);
		}
		
		private function creatUI():void 
		{
			this.x = HORIZONTAL_PADDING;
			
			_uiControlls = new UIControlls();
			addChild(_uiControlls);
			
			// Initail setup of UI controlls
			_uiControlls.pauseButton.visible = false;
			_uiControlls.goWindowedButton.visible = false;
			
			_uiControlls.timelineScrubber.activeBar.scaleX = 0;
			_uiControlls.timelineScrubber.mouseChildren = false;
			
			_uiControlls.volumeScrubber.mouseChildren = false;
			_uiControlls.volumeScrubber.activeBar.scaleX = 1;
			_uiControlls.timelineScrubber.buttonMode = _uiControlls.volumeScrubber.buttonMode = true;
			
			FontFormatter.formatTextField(_uiControlls.elapsedTF, FontFormatter.DIN_REGULAR);
			FontFormatter.formatTextField(_uiControlls.durationTF, FontFormatter.DIN_REGULAR);
			_uiControlls.elapsedTF.text = _uiControlls.durationTF.text = getFormattedTimeString(0);
		}
		
		// --------------------- Controlls API --------------------- \\
		
		public function setPlaying():void 
		{
			_uiControlls.pauseButton.visible = true;
			_uiControlls.playButton.visible = false;
		}
		
		public function setPaused():void 
		{
			_uiControlls.playButton.visible = true;
			_uiControlls.pauseButton.visible = false;
		}
		
		public function updateElapsedTime(elapsedTime:Number):void 
		{
			_uiControlls.elapsedTF.text = getFormattedTimeString(elapsedTime);
		}
		
		public function updateTimelineScrubber(scale:Number):void
		{			
			scale = Math.max( Math.min(1, scale), 0 ); // Scaled clipped to 0 -> 1 (inclusive)
			_uiControlls.timelineScrubber.activeBar.scaleX = scale;
		}
		
		public function updateDurationTime(durationTime:Number):void 
		{			
			_uiControlls.durationTF.text = getFormattedTimeString(durationTime);
		}
		
		// --------------------- Controlls API --------------------- \\
		
		private function showUIControlls():void 
		{
			TweenLite.to(_uiControlls, 0.5, { y:0 } );
			_uiControllsHidden = false;
		}
		
		private function hideUIControlls():void 
		{
			TweenLite.to(_uiControlls, 0.5, { y:_uiControlls.height + VERTICLE_PADDING } );
			_uiControllsHidden = true;
		}
		
		private function handleInactiveTimeutReached():void 
		{
			hideUIControlls();
		}
		
		// ------------------ User Input Listeners ------------------ \\
		
		private function addListeners():void 
		{
			// User inactivity listeners
			stage.addEventListener(MouseEvent.MOUSE_MOVE, handleUserMouseMove, false, 0, true);
			
			// UI controlls Listeners
			_uiControlls.playButton.addEventListener(MouseEvent.CLICK, handlePlayButtonClick, false, 0, true);
			_uiControlls.pauseButton.addEventListener(MouseEvent.CLICK, handlePauseButtonClick, false, 0, true);
			_uiControlls.timelineScrubber.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDownOnScrubber, false, 0, true);
			_uiControlls.volumeScrubber.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDownOnScrubber, false, 0, true);
			_uiControlls.goFullscreenButton.addEventListener(MouseEvent.MOUSE_DOWN, toggleFullScreenState, false, 0, true);
		}
		
		private function handleUserMouseMove(e:MouseEvent):void 
		{
			if (_uiControllsHidden == true)
				showUIControlls();
			
			clearTimeout(_userInactiveTimeout);
			
			// Restart the timer
			_userInactiveTimeout = setTimeout(handleInactiveTimeutReached, INACTIVE_TIMEOUT_TIME);
		}
		
		private function handlePlayButtonClick(e:MouseEvent):void 
		{
			setPlaying();
			dispatchEvent( new UIControllsEvent(UIControllsEvent.PLAY) );
		}
		
		private function handlePauseButtonClick(e:MouseEvent):void 
		{
			setPaused();
			dispatchEvent( new UIControllsEvent(UIControllsEvent.PAUSE) );
		}
		
		private function toggleFullScreenState(e:MouseEvent):void 
		{
			if (stage.displayState == StageDisplayState.NORMAL)
			{
				stage.displayState = StageDisplayState.FULL_SCREEN;
			}
			else
			{
				stage.displayState = StageDisplayState.NORMAL;
			}
		}
		
		private function handleMouseDownOnScrubber(e:MouseEvent):void 
		{
			_activeScrubber = e.target;
			
			handleScrubberDrag(null);
			
			stage.addEventListener(MouseEvent.MOUSE_MOVE, handleScrubberDrag, false, 0, true);
			stage.addEventListener(MouseEvent.MOUSE_UP, handleScrubberDrop, false, 0, true);
		}
		
		private function handleScrubberDrag(e:MouseEvent):void 
		{
			var offsetMouseX:int = stage.mouseX - localToGlobal( new Point((_activeScrubber as DisplayObject).x, 0)).x;			
			var barBaseWidth:int = _activeScrubber.baseBar.width;
			
			var targetScale:Number = offsetMouseX / barBaseWidth / _activeScrubber.scaleX;
			targetScale = Math.max( Math.min(1, targetScale), 0 ); // Scaled clipped to 0 -> 1 (inclusive)
			_activeScrubber.activeBar.scaleX = targetScale; 
			
			// Are we scrubbing video position or volume?
			var eventType:String;
			
			if (_activeScrubber == _uiControlls.timelineScrubber)
				eventType = UIControllsEvent.SCRUB_TIMELINE;
			else
				eventType = UIControllsEvent.SCRUB_VOLUME;
				
			dispatchEvent( new UIControllsEvent(eventType, { scrubberPosition:targetScale } ) );
		}
		
		private function handleScrubberDrop(e:MouseEvent):void 
		{			
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, handleScrubberDrag);
			stage.removeEventListener(MouseEvent.MOUSE_UP, handleScrubberDrop);
			_activeScrubber = null;			
		}
		
		// ------------------ User Input Listeners ------------------ \\
		
		private function getFormattedTimeString(seconds:Number):String 
		{
			var mins:int = Math.floor(seconds / 60);
			var secs:int = Math.ceil(seconds  - mins * 60);
			var minString:String = (mins > 10) ? mins.toString() : "0" + mins.toString();
			var secString:String = (secs > 10) ? secs.toString() : "0" + secs.toString();
			
			return minString + ":" + secString;
		}
		
		private function onResize(e:Event):void 
		{
			this.y = Math.round(stage.stageHeight - _uiControlls.base.height - VERTICLE_PADDING);
			
			// Resize the BG
			_uiControlls.base.width = stage.stageWidth - HORIZONTAL_PADDING * 2;
			
			// Resize the video scrubber's timeline scrubber
			_uiControlls.timelineScrubber.width = stage.stageWidth - HORIZONTAL_PADDING * 2 - SCRUBBER_PADDING_LEFT - SCRUBBER_PADDING_RIGHT;
			
			// Duration TF
			_uiControlls.durationTF.x = _uiControlls.timelineScrubber.x + _uiControlls.timelineScrubber.width + 11;
			
			// Volume Icon
			_uiControlls.volumeIcon.x = _uiControlls.durationTF.x + 42;
			
			// Volume Scrubber
			_uiControlls.volumeScrubber.x = _uiControlls.volumeIcon.x + 15;
			
			// Toggle Screen state buttons
			_uiControlls.goFullscreenButton.x = _uiControlls.volumeScrubber.x + _uiControlls.volumeScrubber.width + 12;
			_uiControlls.goWindowedButton.x = _uiControlls.volumeScrubber.x + _uiControlls.volumeScrubber.width + 12;
		}
		
		private function removeListeners():void 
		{
			_uiControlls.playButton.removeEventListener(MouseEvent.CLICK, handlePlayButtonClick);
			_uiControlls.pauseButton.removeEventListener(MouseEvent.CLICK, handlePauseButtonClick);
			_uiControlls.timelineScrubber.removeEventListener(MouseEvent.MOUSE_DOWN, handleMouseDownOnScrubber);
			_uiControlls.volumeScrubber.removeEventListener(MouseEvent.MOUSE_DOWN, handleMouseDownOnScrubber);
			_uiControlls.goFullscreenButton.removeEventListener(MouseEvent.MOUSE_DOWN, toggleFullScreenState);
		}
		
		private function kill(e:Event):void 
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, kill);
			
			stage.removeEventListener(Event.RESIZE, onResize);
			
			removeListeners();
			
			removeChild(_uiControlls);
			_uiControlls = null;
		}		
		
	}

}