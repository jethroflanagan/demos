__DEFAULT_IMPORTS__
var Journey = require('model/journey/journey');

var IntroJourney = Journey.extend({
	sequence: [
		{
			layerId: 'intro-on',
			soundId: 'engineOn',
		},
		{
			layerId: 'intro-idle',
			soundId: 'carIdle',
			startAfter: 'intro-on',
			delay: 0,
			fadeIn: 1,
			volume: 0.5,			
		},
		{
			layerId: 'intro-indicator',
			soundId: 'indicator',
			startWith: 'intro-idle',
			delay: 7,
			samples: [
				{
					from: 0,
					to: 3,
				},
			],
		},
		// {
		// 	stop: ['intro-idle'],
		// 	delay: 1,
		// 	startWith: 'intro-indicator',
		// },
		{
			layerId: 'driving',
			soundId: 'carDrive',
			startAfter: 'intro-indicator',
			delay: 0,
			fadeIn: 3,
		},
		{
			soundId: 'ocean',
			startWith: 'driving',
			delay: 10,
			fadeIn: 10,
		},
	],
	
	name: 'intro journey',

	initialize: function () {
		Binder.all(this);
		this._super = IntroJourney.__super__;
		this._super.initialize.apply(this, arguments);
		this.start();
	},
});


return IntroJourney;