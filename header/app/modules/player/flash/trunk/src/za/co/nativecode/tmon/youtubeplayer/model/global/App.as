package za.co.nativecode.tmon.youtubeplayer.model.global 
{
	/**
	 * @author Bradley Botha
	 */
	public class App 
	{
		static public const LOCAL:String = "LOCAL";
		static public const LIVE:String = "LIVE";
		
		static private var _mode:String;
		
		static public function set mode(value:String):void 
		{
			if(!_mode)
				_mode = value;
			else
				trace("Error: Mode Can only set once");
		}

		static public function get mode():String
		{
			if (!_mode)
				trace("Error: Mode not set");
			return _mode;
		}
	}
}