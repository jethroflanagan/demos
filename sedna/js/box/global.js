(function () {
	// JS global fixes
	//------------------------------

	// Adds a splice-like functionality, but since it seems impossible to affect the original
	// string itself, fsplice (fake splice) is used to indicate that as it affects the ways
	// code is written.
	if (typeof !String.prototype.fsplice !== 'function') {
		String.prototype.fsplice = function (start, length, replacement) {
			this.substr(0, start) + (replacement ? replacement : '') + this.substr(start + length);
			return this;
		}
	}

	if (typeof String.prototype.trim !== 'function') {
		String.prototype.trim = function () {
			return this.replace(/^\s+|\s+$/g, '');
		}
	}

	// Fix the standard typeof in js
	if (typeof window.getTypeof !== 'function') {
		window.getTypeof = function (obj) {
			return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
		}
	}

	box.scriptLoaded();
})();