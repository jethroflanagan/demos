package za.co.nativecode.tmon.youtubeplayer.command 
{
	import org.robotlegs.mvcs.Command;
	import za.co.nativecode.tmon.youtubeplayer.event.AnalyticsEvent;
	import za.co.nativecode.tmon.youtubeplayer.model.analytics.AnalyticsTypeInfo;
	import za.co.nativecode.tmon.youtubeplayer.service.AnalyticsService;
	
	/**
	 * @author Bradley Botha
	 */
	public class TrackAnalyticsCommand extends Command
	{
		[Inject]
		public var e:AnalyticsEvent;

		[Inject]
		public var service:AnalyticsService;
		
		override public function execute():void
		{
			var info:AnalyticsTypeInfo = AnalyticsTypeInfo(e.data);
			switch(info.type)
			{
				case AnalyticsTypeInfo.EVENT:
					service.trackEvent(info);
					break;
				case AnalyticsTypeInfo.PAGE:
					service.trackPage(info);
					break;
			}
		}
	}
}