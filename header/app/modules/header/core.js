define(
    [
    ],
    function () {
    	var IS_DEBUG = false;

	    var requestAnimationFrameProxy;
	    var cancelAnimationFrameProxy;
	    var fps = 1000 / 30; //used when requestAnimationFrame doesn't exist
    	var browser =
    		{
	    		isIE: navigator.userAgent.indexOf('MSIE'),
				isSafari: navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1, //safari (chrome contains "safari" in its user agent)
				isOpera: navigator.userAgent.indexOf('Presto') != -1
			};


		function getIE_Version()
		{
			return function(){
				if(browser.isIE)
				{
					var start = navigator.userAgent.indexOf("MSIE") + 4;
					var version = navigator.userAgent.substring(start, navigator.userAgent.indexOf(";", start));

					return parseFloat(version);
				}
				else
				{
					return null;
				}
				//	return
			}
		}
		function hasCanvasSupport()
		{
			return Modernizr.canvas;
		}

        function log(var_args)
        {
        	if (!IS_DEBUG)
        		return;
	        //history = history || [];   // store logs to an array for reference
	        //history.push(arguments);
	        //fix an absolutely pathetic webkit bug (https://bugs.webkit.org/show_bug.cgi?id=35801) as yet unconfirmed (according to them)
	        //for 2 entire years. Even IE 7 isn't that stupid.
	        for (var i = 0; i < arguments.length; i++)
	        {
		        if (typeof arguments[i] == "object")
	                arguments[i] = clone(arguments[i]);
	        }
	       	console.log( Array.prototype.slice.call(arguments) );
        }

	    /**
	     * string to rgb
	     * @param value
	     */
	    function decomposeColor(value)
	    {
			var hex = parseInt(value.substr(1), 16);
		    var r = hex >> 16;
		    var g = hex >> 8 & 0xFF;
		    var b = hex & 0xFF;
		    return {r: r, g: g, b: b};
	    }

	    function recomposeColor(rgb)
	    {
		    var r = pad(Math.round(rgb.r).toString(16), 2);
		    var g = pad(Math.round(rgb.g).toString(16), 2);
		    var b = pad(Math.round(rgb.b).toString(16), 2);

		    var hex = "#" + r + g + b;
		    return hex;
	    }

	    function pad(val, amount)
	    {
		    var padding = "";
		    amount -= val.length;
		    for (var i = 0; i < amount; i++)
		        padding += "0";
		    return padding + val;
	    }

	    function clone(object)
	    {
		    try{
		        return JSON.parse(JSON.stringify(object));
		    }
		    catch(e)
		    {
		    }
		    return object;
	    }


	    function roughlyEqual(a, b, threshold)
	    {
		    return Math.abs(a - b) < threshold;
	    }

	    function setupRequestAnimationFrame()
	    {
			    requestAnimationFrameProxy =
				    window.requestAnimationFrame       ||
				    window.mozRequestAnimationFrame    ||
				    window.webkitRequestAnimationFrame ||
				    window.msRequestAnimationFrame     ||
				    window.oRequestAnimationFrame;

			    cancelAnimationFrameProxy = window.cancelAnimationFrame       ||
				    window.mozCancelAnimationFrame    ||
				    window.webkitCancelAnimationFrame ||
				    window.msCancelAnimationFrame     ||
				    window.oCancelAnimationFrame;

			if (requestAnimationFrameProxy == null)
			{
				requestAnimationFrameProxy = function(callback)
					{
						var timer = window.setTimeout(callback, fps);
						return timer;
					};
				cancelAnimationFrameProxy = function(id)
				{
					clearTimeout(id);
				}
			}
		    /*if (!requestAnimationFrameProxy)
			    requestAnimationFrameProxy = window.setInterval;

		    if (!window.cancelAnimationFrame)
			    cancelAnimationFrameProxy = function(id) {
				    window.clearTimeout(id);
			    };*/
	    }
	    function requestAnimationFrame(callback)
	    {
	    	if (callback)
		    	return requestAnimationFrameProxy(callback);
		    else
		    	requestAnimationFrameProxy();
	    }

	    function cancelAnimationFrame(id)
	    {
			cancelAnimationFrameProxy(id);
	    }

	    /**
	     *
	     * @param context:      canvas 2d context
	     * @param dimensions:   {x: float, y: float, width: float, height: float}
	     * @param direction:   {startX: float, startY: float, endX: float, endY: float}
	     * @param colors:       [{pos: float (0-1), color: string (rgba, rgb, #; argb not supported)}, ...] or ["color", "color", ...]
	     */
	    function drawGradient(context, dimensions, direction, colors)
	    {
	    	if (colors == null)
	    	{
	    		throw new Error("No colors");
	    		return;
	    	}
		    var grad = context.createLinearGradient(dimensions.x + direction.startX, dimensions.y + direction.startY, dimensions.x + direction.endX, dimensions.y + direction.endY);
		    for (var i = 0; i < colors.length; i++)
		    {
			    var percent = i / (colors.length - 1)//(colors[i].hasOwnProperty("pos") ? colors[i].pos : i / (colors.length - 1));
			    var color = (colors[i].hasOwnProperty("color") ? colors[i].color : colors[i]);
			    grad.addColorStop(percent, color);
		    }


		    // draw gradient
		    context.fillStyle = grad;
		    context.fillRect(dimensions.x, dimensions.y, dimensions.width, dimensions.height);
	    }

	    function reverseSort(a, b)
	    {
	    	if (a == b)
	    		return 0;
	    	else if (a < b)
	    		return 1;
	    	else
	    		return -1;
	    }

	    function shuffle(array)
	    {
		 	var len = array.length;
			var i = len;
			while (i--)
			{
				var p = parseInt(Math.random() * len);
				var t = array[i];
				array[i] = array[p];
				array[p] = t;
			}
			return array;
	    }
	    /*
	    Taken from http://www.createjs.com/Docs/TweenJS/Ease.js.html, which are based on Robert Penner's functions
	     */
	    function ease()
	    {
		    /*function backOut(currentProgress, totalProgress, startValue, endValue, energy)
		    {
			    if (energy == undefined)
			        energy = 1.70158;
			    return endValue * ((currentProgress = currentProgress / totalProgress - 1) * currentProgress * ((energy + 1) * currentProgress + energy) + 1) + startValue;
			}*/

		    function backOut(progress, start, end)
		    {
			    var energy = 0.5//1.70158;
			    return end * (--progress*progress*((energy+1)*progress + energy) + 1) + start;

		    }

		    function backIn(progress, start, end)
		    {
			    var energy = 0.5//1.70158;
			    return end * (progress * progress * ((energy + 1) * progress - energy)) + start;
		    }

		    return {backIn: backIn, backOut: backOut};
	    }


	    (function ()
	    {
		    if (!Array.prototype.indexOf) {
			    Array.prototype.indexOf = function (searchElement /*, fromIndex */ ) {
				    if (this == null) {
					    throw new TypeError();
				    }
				    var t = Object(this);
				    var len = t.length >>> 0;
				    if (len === 0) {
					    return -1;
				    }
				    var n = 0;
				    if (arguments.length > 0) {
					    n = Number(arguments[1]);
					    if (n != n) { // shortcut for verifying if it's NaN
						    n = 0;
					    } else if (n != 0 && n != Infinity && n != -Infinity) {
						    n = (n > 0 || -1) * Math.floor(Math.abs(n));
					    }
				    }
				    if (n >= len) {
					    return -1;
				    }
				    var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);
				    for (; k < len; k++) {
					    if (k in t && t[k] === searchElement) {
						    return k;
					    }
				    }
				    return -1;
			    }
		    }
		    setupRequestAnimationFrame();
	    })();
        return {getIE_Version: getIE_Version(), IS_DEBUG: IS_DEBUG, hasCanvasSupport: hasCanvasSupport, sort: {reverse: reverseSort}, shuffle: shuffle, browser: browser, log: log, ease: ease, clone: clone, decomposeColor: decomposeColor, recomposeColor: recomposeColor, roughlyEqual: roughlyEqual, requestAnimationFrame: requestAnimationFrame, cancelAnimationFrame: cancelAnimationFrame, drawGradient: drawGradient};
    });