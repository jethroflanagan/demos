/**
//TODO: This was a custom clock that was added becuase of new requirments. It was hacked into place with hardcoded values. This must be cleaned up in future.
*/
define([
	box.lib('backbone'),
	box.lib('lodash'),
	box.lib('jquery-nc'),
	'sedna/event/player_events',
	'sedna/event/end_frame_events',
	'sedna/event/grid_events',
	//project
	box.pkg('view/grid/grid_block')
],
function (Backbone, _, $, PlayerEvents, EndFrameEvents, GridEvents, GridBlock) {
	var _super = GridBlock.prototype;

	var ClickBlock = GridBlock.extend({
		initialize: function (params) {
			_.bindAll(this);
			_super.initialize.apply(this, [params]);
			$(this.el).find('.js-block-play').on("click",this.onClickHandler);
			$(this.el).find('.js-block-share').on("click",this.onShareHandler);
		},
		showContent: function () {
			_super.showContent.apply(this); 
		},
		onClickHandler: function(e) {
			//TODO: Don't hard code values. @Jethro I'm only hardcoding them because I don't have time to find another way of doing it.
			box.event.dispatch(null, GridEvents.hide, {});
			
			//Remove the click event
			$(this.el).find('.js-block-play').off("click");
			$(this.el).find('.js-block-share').off("click");
		},
		onShareHandler: function(e) {
			//Share code here			
			//$(this.el).find('.js-block-play').off("click");
			//$(this.el).find('.js-block-share').off("click");
			window.open('https://www.facebook.com/sharer/sharer.php?u=http://youtu.be/uCqxsqH2fF8');
		},
		dispose: function (e) {
			_super.dispose.apply(this);
			//Tell the player which movie to play now that the block has been removed fully
			//Hide the image that gets drawn over the movie.
			box.event.dispatch(null, EndFrameEvents.hide, { image: "result" }); 
			box.event.dispatch(null, PlayerEvents.loadVideo, { videoId: "uCqxsqH2fF8" });
		}
	});

	return ClickBlock;
});