__DEFAULT_IMPORTS__

var ButtonsView = require('view/buttons-view');
var SoundEngine = require('model/sound/sound-engine');
var SoundUiController = require('controller/sound-ui-controller');

var AppView = Backbone.View.extend({
	buttonsView: null,
	soundEngine: null,
	
	initialize: function () {
		Binder.allExcept(this);
		
		this.buttonsView = new ButtonsView({ el: '.ui' });
		this.soundEngine = new SoundEngine();
		this.soundUiController = new SoundUiController({ soundEngine: this.soundEngine, view: this.buttonsView });
	},

	render: function() {
	}

});

return AppView; //single instance only