define([
	box.lib('backbone'),
	box.lib('lodash'),
	box.lib('jquery-nc'),

	//project
	box.pkg('event/app_events'),
	box.pkg('event/player_events'),
	box.pkg('event/grid_events'),
	box.pkg('event/end_frame_events'),
	box.pkg('event/menu_events'),
	box.pkg('event/navigation_events'),

	'text!assets/config.json'
],
function (Backbone, _, $, AppEvents, PlayerEvents, GridEvents, EndFrameEvents, MenuEvents, NavigationEvents, Config) {
	var SCENE_SEPARATOR = '/';

	var MenuModel = Backbone.Model.extend({
		scenes: null,
		menu: null,
		readyList: null,
		scene: null,
		player: null,

		initialize: function () {
			_.bindAll(this);
			this.readyList = {
				player: false,
				grid: false
			};
			this.parseMenu('en', JSON.parse(Config));
			box.event.addListener(null, AppEvents.loadScene, this.onLoadScene);
			box.event.addListener(null, GridEvents.ready, this.onSceneLoaded);
			box.event.addListener(null, PlayerEvents.readyStatus, this.onPlayerReadyStatus);
			box.event.addListener(null, PlayerEvents.done, this.onPlayerDone);
			box.event.addListener(null, PlayerEvents.playing, this.onPlayerPlaying);
			box.event.addListener(null, PlayerEvents.ready, this.onPlayerReady);
			box.event.addListener(null, NavigationEvents.open, this.onNavigation);
		},
		
		onPlayerReady: function (e) {
			this.player = e.data.player;
		},

		parseMenu: function (languageCode, json) {
			this.languageCode = languageCode;
			var scenes = json.language[languageCode].scenes;
			
			this.menu = [];
			this.scenes = scenes;

			for (var i = 0, len = scenes.length; i < len; i++) {
				this.menu.push({
					id: scenes[i].id,
					title: scenes[i].title
				});
			}
		},
		
		getMenu: function () {
			return this.menu;
		},

		/**
		 * The scene and the player need to sync up so that the scene is fully loaded when the player begins to
		 * load/play a video. The player can only attempt to load a video when the API is ready, so that is 
		 * also part of the wait time.
		 */
		onLoadScene: function (e) {
			this.scene = this.getSceneById(e.data.scene);
			this.readyList.player = false;
			this.readyList.grid = false;

			box.event.dispatch(null, PlayerEvents.getReadyStatus);
			box.event.dispatch(null, GridEvents.loadScene, { scene: this.scene.blocks, sceneId: this.languageCode + SCENE_SEPARATOR + this.scene.id});
			box.event.dispatch(null, MenuEvents.disable);
		},

		/**
		 * Scene ids are set by the config.json
		 * 
		 * @return {Object}    The scene info from the config file
		 */
		getSceneById: function (id) {
			for (var i = 0, len = this.scenes.length; i < len; i++) {
				if (this.scenes[i].id == id)
					return this.scenes[i];
			}
			return null;
		},
		
		onPlayerPlaying: function (e) {
			box.event.dispatch(null, MenuEvents.enable);
		},

		onSceneLoaded: function (e) {
			this.readyList.grid = true;
			this.checkSceneReady();
		},

		onPlayerReadyStatus: function (e) {
			this.readyList.player = true;
			this.checkSceneReady();
		},

		onPlayerDone: function (e) {
			box.event.dispatch(null, EndFrameEvents.show);
			var nextSceneIndex = -1;
			for (var i = 0, len = this.scenes.length; i < len; i++) {
				if (this.scene == this.scenes[i]) {
					nextSceneIndex = i + 1;
					break;
				}
			}
			
			//This method was hacked in to get the final movie to trigger the grid blocks.
			if(this.player != null){
				var videoId = $.data(this.player).videoId;
				if(videoId == "uCqxsqH2fF8"){
					//box.event.dispatch(null, MenuEvents.highlight, { index: this.scenes.length-1 });
					//console.log("show menu");
					//box.event.dispatch(null, GridEvents.show, {});
					//box.event.dispatch(null, GridEvents.showScene);
					this.onLoadScene({data:{scene:"result"}});
					return;
				}
			}

			// not found (shouldn't happen), or at the end of show
			if (nextSceneIndex == -1 || nextSceneIndex >= this.scenes.length)
				return;

			box.event.dispatch(null, MenuEvents.highlight, { index: nextSceneIndex });
		},

		/**
		 * Once both are sync'd up. load the video (autoplays), show the scene
		 */
		checkSceneReady: function () {
			if (this.readyList.grid && this.readyList.player) {
				box.event.dispatch(null, EndFrameEvents.hide, { image: this.scene.id });
				//box.event.dispatch(null, EndFrameEvents.set, { image: this.scene.id });
				box.event.dispatch(null, PlayerEvents.loadVideo, { videoId: this.scene.video });
				box.event.dispatch(null, GridEvents.showScene);
			}
		},

		/** 
		 * for when a link block is clicked
		 * @param  {[type]} e 	contains {link: String}
		 *                    	##<int> (e.g. "##3") is used to jump to menu section 3
		 *                    	where int starts at 0
		 */
		onNavigation: function (e) {
			var link = e.data.link;

			// check if it should be an internal link
			var hashMarker = '##';
			var hashIndex = link.indexOf(hashMarker);
			if (hashIndex > -1) {
				var menuIndex = link.substr(hashIndex + hashMarker.length);
				box.event.dispatch(null, MenuEvents.activate, { id: this.menu[menuIndex].id });
			}
		}

	});

	return MenuModel;
});