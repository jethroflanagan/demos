return {
	getNumObjectProperties: function (obj) {
		var count = 0;
		for (var prop in obj) {
			if (obj.hasOwnProperty(prop)) {
				count++;
			}
		}
		return count;
	},

	// TODO do a nicer version
	deepCopy: function (obj) {
		return JSON.parse(JSON.stringify(obj));
	},
}