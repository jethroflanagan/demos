package za.co.nativecode.tmon.youtubeplayer.model.util 
{
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.utils.Dictionary;
	import mx.events.StateChangeEvent;
	/**
	 * @author Bradley Botha
	 */
	public class Listeners 
	{
		static private var _listeners:Dictionary;
		
		static public function init():void
		{
			_listeners = new Dictionary();
		}
		
		static public function add(object:*, eventType:String, fire:Function, weakReference:Boolean = false):void
		{
			addListener(object, eventType, fire, false, weakReference);
		}
		
		static public function addOnce(object:*, eventType:String, fire:Function, weakReference:Boolean = false):void
		{
			addListener(object, eventType, fire, true, weakReference);
		}
		
		static public function addEnterFrameListener(object:*, fire:Function, killCheck:Function = null, weakReference:Boolean = false):void
		{
			add(object, Event.ENTER_FRAME, fire, weakReference);
		}
		
		static public function removeEnterFrameListener(object:*, fire:Function, killCheck:Function = null, weakReference:Boolean = false):void
		{
			remove(object, Event.ENTER_FRAME, fire);
		}
		
		static public function addLoaderComplete(loader:Loader, fire:Function, weakReference:Boolean = false):void
		{
			addOnce(loader.contentLoaderInfo, Event.COMPLETE, fire, weakReference);
/*			//in case needed
			addOnce(loader.contentLoaderInfo, Event.COMPLETE, handleRemoveProgress, weakReference);
*/		}
		
		static public function addLoaderProgress(loader:Loader, fire:Function, weakReference:Boolean = false):void
		{
			add(loader.contentLoaderInfo, ProgressEvent.PROGRESS, fire, weakReference);
		}
		
		static private function addListener(object:*, eventType:String, fire:Function, removeAfterFire:Boolean = false, weakReference:Boolean = false):void
		{
			if(_listeners == null)
				init();
			object.addEventListener(eventType, fire, false, 0, weakReference);
			if(removeAfterFire)
			{
				object.addEventListener(eventType, handleRemoveAfterFire);
				_listeners[object] = fire;
			}
		}
		
		static public function remove(object:*, eventType:String, fire:Function):void
		{
			object.removeEventListener(eventType, fire);
		}
		
		static public function removeProgress(loader:Loader, fire:Function):void 
		{
			remove(loader.contentLoaderInfo, ProgressEvent.PROGRESS, fire);
		}
		
		static public function addStateListener(object:*, fire:Function, removeAfterFire:Boolean = true, weakReference:Boolean = false):void 
		{
			if(removeAfterFire)
				addOnce(object, StateChangeEvent.CURRENT_STATE_CHANGE, fire, weakReference);
			else
				add(object, StateChangeEvent.CURRENT_STATE_CHANGE, fire, weakReference);
		}
		
		static private function handleRemoveAfterFire(e:Event):void
		{
			var object:* = e.currentTarget;
			if(_listeners[object] != null) //already removed if null
			{
				object.removeEventListener(e, _listeners[object]);
				_listeners[object] = null;
			}
		}
	}

}