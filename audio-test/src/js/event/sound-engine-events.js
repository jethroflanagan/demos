var Event = require('model/event');

return {
	PLAY: Event.create('play'),
	STOP: Event.create('stop'),
	STOP_MASTER: Event.create('stop master'),
	SET_VOLUME: Event.create('set volume'),
	SET_MASTER_VOLUME: Event.create('set master volume'),
	ADD_FILTER: Event.create('add filter'),
	REMOVE_FILTER: Event.create('remove filter'),
	PLAYBACK_COMPLETE: Event.create('playback complete'),
	PLAYBACK_PROGRESS: Event.create('playback progress'), // subscribe to this for updates
	PREPARE: Event.create('prepare sound'), // shortcut to create a sound without playing it, allows you to add filters, set the fade in time, set a volume, etc.
}