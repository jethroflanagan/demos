define([
	box.lib('backbone'),
	box.lib('lodash'),
	box.lib('jquery-nc'),

	//project
	box.pkg('view/grid/grid_block'),
	box.pkg('view/grid/number_block'),
	box.pkg('event/grid_events')
],
function (Backbone, _, $, GridBlock, NumberBlock, GridEvents) {
	var Grid = Backbone.View.extend({
		model: null,
		blocks: null,
		partial: null,
		isSceneDisposed: null,

		initialize: function () {
			_.bindAll(this);
			this.blocks = [];
			//this.createBlock();
			box.event.addListener(null, GridEvents.loadScene, this.onLoadScene);
			box.event.addListener(null, GridEvents.showScene, this.onShowScene);
			box.event.addListener(null, GridEvents.hide, this.onHide);
		},
		
		onHide: function (e) {
			// already disposed?
			if (this.blocks.length == 0) {
				this.isSceneDisposed = true;
				this.checkReady();
				return;
			}

			this.isSceneDisposed = false;
			var wait = 0;
			var time = 150;
			while (this.blocks.length > 0) {
				var block = this.blocks.pop();
				wait = Math.max(wait, block.hide(this.blocks.length * time));
			}
			this.model.clearBlocks();
			this.isSceneDisposed = true;
		},

		onLoadScene: function (e) {
			this.partial = null;
			this.isSceneDisposed = false;
			this.disposeScene();
			this.model.loadScene(e.data, this.onGetPartial, this.onGetPartialError);
		},

		onGetPartial: function (result) {
			this.partial = result;
			this.checkReady();
		},

		onShowScene: function (e) {
			this.render(this.partial);
			var self = this;
			var i = 0;
			this.$el.find('.js-block').each(function (index) {
				var $this = $(this);
				var blockType = $this.data('type');
				if (!blockType)
					blockType = 'grid';
				var blockClass = self.model.getBlockClass(blockType);
				var blockConfig = { 
					el: $this, 
					col: $this.data('col'), 
					row: $this.data('row'),
					// if these are not defined, 1 will be used as default
					numCols: $this.data('num-cols'),
					numRows: $this.data('num-rows'),
					direction: $this.data('direction'),
					options: $this.data() // send any outstanding data through
				};
				var block = new (blockClass)(blockConfig);
				var time = $this.data('time');
				// set to zero for undefined
				if (!time)
					time = 0;
				self.model.addBlock(block, time);
				self.blocks.push(block);
			});
		},

		onGetPartialError: function (error) {
			console.error('Partial load failed:', error);
		},

		render: function (content) {
			this.$el.html(content);
		},

		disposeScene: function () {
			// already disposed?
			if (this.blocks.length == 0) {
				this.isSceneDisposed = true;
				this.checkReady();
				return;
			}

			this.isSceneDisposed = false;
			var wait = 0;
			var time = 150;
			while (this.blocks.length > 0) {
				var block = this.blocks.pop();
				wait = Math.max(wait, block.hide(this.blocks.length * time));
				//block.dispose();
			}
			this.model.clearBlocks();

			var self = this;
			setTimeout(function () {
				self.isSceneDisposed = true;
				self.checkReady();
			}, wait + time); // longer delay for safety
		},

		checkReady: function () {
			if (this.partial && this.isSceneDisposed)
				box.event.dispatch(null, GridEvents.ready);
		}
	});

	return Grid;
});