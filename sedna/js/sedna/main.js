define([
	box.lib('backbone'),
	box.lib('lodash'),
	box.lib('jquery-nc'),

	//project
	box.pkg('view/ui')

],
function (Backbone, _, $, Ui) {
	var Main = Backbone.View.extend({

		initialize: function () {
			_.bindAll(this);

			$(document).ready(this.onReady);
		},

		onReady: function (e) {
			if (typeof JSON == 'undefined') {
				return;
			}
			var ui = new Ui({ el: '.stream' });
			$('.stream').slideDown();
		}
	});

	return Main;
});
