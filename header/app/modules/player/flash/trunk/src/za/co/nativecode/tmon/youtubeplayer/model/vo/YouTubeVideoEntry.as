package za.co.nativecode.tmon.youtubeplayer.model.vo
{
	import flash.utils.describeType;
	
	/**
	 * ...
	 * @author Brad
	 * 
	 * Sample of XML returned by YouTube:
	 * http://code.google.com/apis/youtube/2.0/developers_guide_protocol_understanding_video_feeds.html
	 * 
	 */

	public class YouTubeVideoEntry
	{
		public var thumbnails:Vector.<Object>;
		public var videoID:String;
		
		public var published:String;
		public var updated:String;
		public var content:String;		
		public var links:Object;
		public var authorName:String;
		public var authorURI:String;
		public var commentsURL:String;
		public var title:String;
		public var description:String;
		public var duration:String;
		public var rating:String;
		public var favorites:String;
		public var views:String;
		
		public function YouTubeVideoEntry(entry:XML)
		{			
			var atomNS:Namespace = new Namespace("http://www.w3.org/2005/Atom");
			var mediaNS:Namespace = new Namespace("http://search.yahoo.com/mrss/");
			var gdNS:Namespace = new Namespace("http://schemas.google.com/g/2005");
			var ytNS:Namespace = new Namespace("http://gdata.youtube.com/schemas/2007");
			
			// VIDEO ID
			videoID = entry.atomNS::id;
			videoID = videoID.substr(videoID.lastIndexOf("/") + 1, videoID.length); // STRIP ALL THE BULLSHIT THAT IS PART OF THE ID, JUST USE THE STRING AFTER LAST "/"
			
			// THUMBNAILS
			var numberOfThumbnails:int = entry.mediaNS::group.mediaNS::thumbnail.length();
			thumbnails = new Vector.<Object>(numberOfThumbnails, true);
			
			for (var i:int = 0; i < numberOfThumbnails; i++) 
			{
				var thumbnailDetails:XML = entry.mediaNS::group.mediaNS::thumbnail[i];
				
				var thumbObject:Object = new Object();
				thumbObject.url = thumbnailDetails.@url;
				thumbObject.width = thumbnailDetails.@width;
				thumbObject.height = thumbnailDetails.@height;
				thumbObject.time = thumbnailDetails.@time;
				
				thumbnails[i] = thumbObject;
			}
			
			// PUBLISHED
			published = entry.atomNS::published;
			
			// UPDATED
			updated = entry.atomNS::updated;
			
			// CONTENT
			content = entry.atomNS::content;
			
			// LINKS
			links = new Object();
			links.alternate = entry.atomNS::link[0].@href;
			links.responses = entry.atomNS::link[1].@href;
			links.related = entry.atomNS::link[2].@href;
			links.self = entry.atomNS::link[3].@href;
			
			// AUTHOR NAME
			authorName = entry.atomNS::author.atomNS::name;
			
			// AUTHOR URI
			authorURI = entry.atomNS::author.atomNS::uri;
			
			// COMMENTS URL
			commentsURL = entry.gdNS::comments.gdNS::feedLink.@href;
			
			// TITLE
			title = entry.mediaNS::group.mediaNS::title;
			
			// DESCRIPTION
			description = entry.mediaNS::group.mediaNS::description;
			
			// DURATION
			duration = entry.mediaNS::group.ytNS::duration.@seconds;
			
			// RATING
			rating =  entry.gdNS::rating.@average;
			
			// FAVOURITES
			favorites = entry.ytNS::statistics.@favoriteCount;
			
			// VIEWS
			views = entry.ytNS::statistics.@viewCount;
		}
	}
}