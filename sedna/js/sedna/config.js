require.config({
	deps: ['sedna/app'],
	baseUrl: 'js/',

	//urlArgs: 'v=1',

	paths: {
		modernizr: 'vendor/modernizr',
		'jquery-nc': 'vendor/jquery-nc',
		jquery: 'http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min',
		backbone: 'vendor/backbone',
		lodash: 'vendor/lodash',
		partial: '../partial',
		assets: '../assets',
		plugin: 'vendor/plugin',
		text: 'vendor/plugin/require/text'
	},

	shim: {
		jquery: {
			exports: 'jQuery'
		},
		backbone: {
			exports: 'Backbone',
			deps: ['lodash', 'jquery']
		},
		lodash: {
			exports: '_'
		},
		modernizr: {
			exports: 'Modernizr'
		}
	}
});