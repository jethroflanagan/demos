// Builds the JS into 

var gulp = require('gulp');
var paths = require('../paths');
var clean = require('gulp-clean');
var rjs = require('requirejs');
//var buildConfig = require(paths.BUILD_CONFIG);

// get app info from package.json in the root
var app = require('./../../package.json');

gulp.task('bundle', function () {
	try {
		rjs.optimize({
			baseUrl: paths.TMP_JS,
			mainConfigFile: paths.BUILD_CONFIG,
			//paths: buildConfig.paths, // import the paths from the config file
			include: 'main',
			insertRequire: ['main'],
			name: '../../node_modules/almond/almond',
			out:  paths.DEST_JS + '/' + app.name + '.js',
			wrap: true,
			optimize: 'none', // uglify2, etc. make reading the scope variables a pain when debugging, so keep source as is.
			generateSourceMaps: true,
			preserveLicenseComments: false
		},
		function (result) {
			// console.log('ftw', result)
		},
		function (error) {
			console.log('Subtract 9 from line numbers till I do my own stack output');
			console.log(error);
		});
	}
	catch (e) {
		console.log('***Massive failure***');
		console.log(e);
	}
});

gulp.task('bundle:prod', function () {
	try {
		rjs.optimize({
			baseUrl: paths.TMP_JS,
			mainConfigFile: paths.BUILD_CONFIG,
			//paths: buildConfig.paths, // import the paths from the config file
			include: 'main',
			insertRequire: ['main'],
			name: '../../node_modules/almond/almond',
			out:  paths.DEST_JS + '/' + app.name + '.js',
			wrap: true,
			optimize: 'uglify2',
			generateSourceMaps: false,
			preserveLicenseComments: true
		});
	}
	catch (e) {
		console.log('***Massive failure***');
		console.log(e);
	}
});