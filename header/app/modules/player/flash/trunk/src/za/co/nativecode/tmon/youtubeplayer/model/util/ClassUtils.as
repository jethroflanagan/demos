package za.co.nativecode.tmon.youtubeplayer.model.util 
{
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	/**
	 * @author Bradley Botha
	 */
	public class ClassUtils 
	{
		
		static public function getClass(instance:*):Class
		{
			return getDefinitionByName(getClassName(instance)) as Class;
		}
		
		static public function getClassName(instance:*):String
		{
			return getQualifiedClassName(instance);
		}
		
		
	}

}