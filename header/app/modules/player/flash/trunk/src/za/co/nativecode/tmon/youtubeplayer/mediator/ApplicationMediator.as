package za.co.nativecode.tmon.youtubeplayer.mediator 
{
	import org.robotlegs.mvcs.Mediator;
	import za.co.nativecode.tmon.youtubeplayer.event.AnalyticsEvent;
	import za.co.nativecode.tmon.youtubeplayer.Main;

	/**
	 * @author Bradley Botha
	 */
	public class ApplicationMediator extends Mediator
	{
		[Inject] public var view:Main;
		
		override public function onRegister():void
		{
			mapContextListeners();
			mapViewListeners();

			dispatch(new AnalyticsEvent(AnalyticsEvent.INIT, view));
		}
		
		private function mapContextListeners():void
		{
		}
		
		private function mapViewListeners():void
		{
		}
	}
}