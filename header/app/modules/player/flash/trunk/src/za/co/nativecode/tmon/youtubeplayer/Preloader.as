package za.co.nativecode.tmon.youtubeplayer
{
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.utils.getDefinitionByName;
	
	/**
	 * @author Bradley Botha
	 */
	public class Preloader extends MovieClip 
	{
		public function Preloader() 
		{
			if (stage)
				init();
			else
				addEventListener(Event.ADDED_TO_STAGE, handleInit);
		}
		
		private function handleInit(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, handleInit);
			init();
		}
		
		private function init():void
		{
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			addEventListener(Event.ENTER_FRAME, handleCheck);
			loaderInfo.addEventListener(ProgressEvent.PROGRESS, handleProgress);
			loaderInfo.addEventListener(IOErrorEvent.IO_ERROR, handleError);
		}
		
		private function handleError(e:IOErrorEvent):void 
		{
			//trace(e.text);
		}
		
		private function handleProgress(e:ProgressEvent):void 
		{
			//trace((e.bytesLoaded / e.bytesTotal));
		}
		
		private function handleCheck(e:Event):void
		{
			if (currentFrame == totalFrames) 
			{
				stop();
				cleanup();
			}
		}
		
		private function cleanup():void 
		{
			while (numChildren > 0)
				removeChildAt(0);
			removeEventListener(Event.ENTER_FRAME, handleCheck);
			loaderInfo.removeEventListener(ProgressEvent.PROGRESS, handleProgress);
			loaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, handleError);
			
			startup();
		}
		
		private function startup():void 
		{
			var mainClass:Class = getDefinitionByName("za.co.nativecode.tmon.youtubeplayer.Main") as Class;
			addChild(new mainClass() as DisplayObject);
		}
		
	}
	
}