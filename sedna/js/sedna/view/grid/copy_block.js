define([
	box.lib('backbone'),
	box.lib('lodash'),
	box.lib('jquery-nc'),

	//project
	box.pkg('view/grid/grid_block')
],
function (Backbone, _, $, GridBlock) {
	var _super = GridBlock.prototype;

	var CopyBlock = GridBlock.extend({
		initialize: function (params) {
			_.bindAll(this);
			_super.initialize.apply(this, [params]); 
		},
		
		showContent: function () {
			_super.showContent.apply(this); 
		}
	});

	return CopyBlock;
});