return {
	path: 'audio',
	files: {
		forest: {
			path: '231537__vkproduktion__forest-birds-loop-02',
			duration: 30,
		},
		ocean: {
			path: '161700__xserra__ocean-3',
			duration: 10,
		},
		music: {
			path: 'ANW1934_05_Midnite-Swing',
			duration: 176,
		},
		engineOn: {
			path: '138099__snakebarney__car-start',
			duration: 2,
		},
		carIdle: {
			path: '235507__ceberation__car-engine',
			duration: 21,
		},
		carDrive: {
			path: '164620__adam-n__car-interior-driving-2',
			duration: 290,
		},
		windscreenWipers: {
			path: '124737__robinhood76__02513-loopable-car-wipers',
			duration: 14,
		},
		windowUp: {
			path: '31459__digifishmusic__car-window-down',
			duration: 3,
		},
		storm: {
			path: '157486__loopbasedmusic__rain-strong-with-thunders',
			duration: 121,
		},
		indicator: {
			path: '144055__nlm__car-indicator-interior',
			duration: 16,
		},
		gravel: {
			path: '196674__soundmary__car-arrive-on-gravel',
			duration: 27,
		},
	},
	MASTER: {},
};