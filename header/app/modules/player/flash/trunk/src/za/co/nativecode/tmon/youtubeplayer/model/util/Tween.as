package za.co.nativecode.tmon.youtubeplayer.model.util 
{
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import flash.utils.Dictionary;
	import flash.utils.setTimeout;
	/**
	 * @author Bradley Botha
	 */
	public class Tween 
	{
		static private var _elasticInfo:Dictionary;
		
		static public function timed(obj:*, properties:Object, time:Number, useFrames:Boolean = false):void
		{
			if(useFrames)
				properties.useFrames = true;
			TweenMax.to(obj, time, properties);
		}

		static public function elastic(obj:*, properties:Object, speed:Number, friction:Number):void
		{
			if (!_elasticInfo)
				_elasticInfo = new Dictionary(false);
			if (!_elasticInfo[obj])
				_elasticInfo[obj] = {};
			
			for(var prop:String in properties)
			{
				var velocity:String = prop + "_velocity";
				if (!_elasticInfo[obj][velocity])
					_elasticInfo[obj][velocity] = 0;
				_elasticInfo[obj][velocity] += (properties[prop] - obj[prop]) * speed;
				_elasticInfo[obj][velocity] *= friction;
				obj[prop] += _elasticInfo[obj][velocity];
				if (obj[prop] == properties[prop] && _elasticInfo[obj][velocity] == 0)
					_elasticInfo[obj] = null;
			}
		}
		
		static public function timedFrom(obj:*, properties:Object, time:Number, useFrames:Boolean = false):void
		{
			if(useFrames)
				properties.useFrames = true;
			TweenMax.from(obj, time, properties);
		}
		
		static public function constant(obj:*, properties:Object, speed:Number):void
		{
			for(var prop:String in properties)
			{
				obj[prop] = (obj[prop] + properties[prop] * speed) / (speed + 1);
			}
		}		

		static public function kill(obj:*):void
		{
			TweenMax.killTweensOf(obj);
		}
	}

}