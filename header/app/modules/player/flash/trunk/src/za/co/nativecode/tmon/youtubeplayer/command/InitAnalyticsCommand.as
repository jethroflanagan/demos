package za.co.nativecode.tmon.youtubeplayer.command 
{
	import flash.display.DisplayObject;
	import org.robotlegs.mvcs.Command;
	import za.co.nativecode.tmon.youtubeplayer.event.AnalyticsEvent;
	import za.co.nativecode.tmon.youtubeplayer.service.AnalyticsService;
	
	/**
	 * @author Bradley Botha
	 */
	public class InitAnalyticsCommand extends Command
	{
		[Inject]
		public var e:AnalyticsEvent;

		[Inject]
		public var service:AnalyticsService;
		
		override public function execute():void
		{
			service.init(DisplayObject(e.data));
		}
		
	}
	
}