var Event = require('model/event');

return {
	PLAYBACK_COMPLETE: Event.create('playback complete'),
	PLAYBACK_PROGRESS: Event.create('playback progress'),
}