<header id="header">
	<div class="divider-line"></div>
	<div class="clearfix l-wrap">
		<a id="header-link" href="/about#nav-main-area"></a>
		<nav class="clearfix nav-main l-wrap-main">
			<ul class="clearfix menu-social">
				<li><a class="main-sprite main-logo-alt" href="/" onClick="_gaq.push(['_trackEvent', 'Header', 'Header-logo', '']);" title="Native Home">Native</a></li>
				<li>
					<ul>
						<li><a class="main-sprite main-fb-alt" href="http://www.facebook.com/nativesa" target="_blank" onClick="_gaq.push(['_trackEvent', 'Header', 'Header-facebook', '']);" title="Like us on Facebook">Facebook</a></li>
						<li><a class="main-sprite main-twttr-alt" href="http://twitter.com/native" target="_blank" title="Follow us on Twitter" onClick="_gaq.push(['_trackEvent', 'Header', 'Header-twitter', '']);">Twitter</a></li>
						<li><a class="main-sprite main-youtube-alt" href="http://www.youtube.com/nativewatch" target="_blank" title="Subscribe to our YouTube Channel" onClick="_gaq.push(['_trackEvent', 'Header', 'Header-youtube', '']);">Youtube</a></li>
						<li><a class="main-sprite main-gplus-alt" href="https://plus.google.com/u/0/114600710942171886070/" target="_blank" title="Follow us on Google+" onClick="_gaq.push(['_trackEvent', 'Header', 'Header-googleplus', '']);">Google+</a></li>
						<li><a class="btn btn-contact btn-alt" href="/contact-us" title="Get in touch with us">Contact</a></li>
					</ul>
				</li>
			</ul>
		</nav> <!-- .nav-main -->
		<ul class="l-wrap-main" id="banner">
			<li class="bnnr-landing">
				<canvas id="triangles" class="featured-image" width="484" height="484">
					<div style="width: 100%; height: 100%; bottom: 0px; position:absolute;">
						<img id="render" src="${templatePath}img/banner/renders/africa.gif" width="484" height="484" />
					</div>
					<ul id="triangleslider" style="display: none;">
						<li>
							<h1>A DIGITALLY-LED FULL-SERVICE MARKETING AGENCY</h1>
							<p>Informed by insight. Governed by strategy. Sparked by intuition. Inspired by excellence. Measured by effectiveness.</p>
							<img data-src="${templatePath}img/banner/renders/africa.gif" data-shape="africa"/>
							<a href="/about#nav-main-area">*</a>
						</li>
						<li>
							<h1>PROMOTING WATER AWARENESS VIA A TWITTER POWERED TAP</h1>
							<p>It took 10 000 social media pledges to close a running tap and raise global awareness  of the importance of clean fresh water. NATIVE made it happen.</p>
							<img data-src="${templatePath}img/banner/renders/tap.gif" data-shape="tap"/>
							<a href="/work-detail/closethetap">*</a>
						</li>
						<li>
							<h1>NATIVE SECURES KEY PERNOD RICARD BRANDS</h1>
							<p>NATIVE has been awarded the Chivas Regal, Jameson, Malibu, Absolut and Pernod Ricard digital marketing accounts.</p>
							<img data-src="${templatePath}img/banner/renders/chivas.gif" data-shape="chivas"/>
							<a href="/blog-detail/native-secures-key-pernod-ricard-brands">*</a>
						</li>
					</ul>
				</canvas>
				<div class="slider">
					<h1>A DIGITALLY-LED FULL-SERVICE MARKETING AGENCY</h1>
				</div>
				<p><span class="banner-mark"><img src="${templatePath}img/banner/mark.png" style="margin-top: 30px;" /></span></p>
				<p style='display: block; margin-top: 35px;'>Informed by insight. Governed by strategy. Sparked by intuition. Inspired by excellence. Measured by effectiveness.</p>
			</li>
		</ul><!-- #banner -->
	</div><!-- .l-wrap -->
	<div class="texture">
		<img src="${templatePath}img/header-glow.png" />
	</div>
</header>
<div class="divider-line"></div>