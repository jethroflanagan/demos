package za.co.nativecode.tmon.youtubeplayer.event 
{
	import flash.events.Event;
	
	/**
	 * @author Bradley Botha
	 */
	public class AnalyticsEvent extends Event 
	{
		static public const INIT:String = "init";
		static public const TRACK:String = "track";
		
		private var _data:Object;
		
		public function AnalyticsEvent(type:String, data:Object = null, bubbles:Boolean=false, cancelable:Boolean=false)
		{ 
			super(type, bubbles, cancelable);
			_data = data;
		} 
		
		public function get data():Object
		{
			return _data;
		}
		
		public override function clone():Event
		{ 
			return new AnalyticsEvent(type, _data, bubbles, cancelable);
		} 
		
		public override function toString():String
		{ 
			return formatToString("AnalyticsEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}