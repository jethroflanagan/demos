define([
	// Global
	'app',

	// Libs
	'lodash',
	'jquery',
	'backbone'
],

function(app, _, $, Backbone, prettydate){

	var List = app.module();

	List.Item = Backbone.Model.extend({
		defaults: {
			visible: false
		}
	});

	List.Collection = Backbone.Collection.extend({
		model: List.Item,
		initialize: function(){
			_.bindAll(this, 'formatItems', 'JSONItemsExist');
		},
		formatItems: function(listItem){

			var item = new List.Item({
				contentType	: listItem.systemFields.contentType, // barely used
				date 		: listItem.displayDate,
				excerpt		: listItem.listingTeaser ? listItem.listingTeaser : null,
				nosId 		: listItem.id,
				prettyDate	: app.prettyDate(listItem.displayDate),
				size 		: 1,
				title 		: listItem.title,
				type 		: listItem.systemFields.contentType
			});

			this.JSONItemsExist(listItem, item);

			switch(listItem.systemFields.contentType){
				case 'Blog': // Tweet or Blog
						item.set({
							type: listItem.catBlogType[0]
						});

					if ( listItem.catBlogType[0] == 'Blog' ) {
						item.set({
							excerpt: listItem.blogTeaser,
							image: listItem.blogCornerImage_sprite_,
							size: 2
						});
					} else { // Tweet
						item.set({
							excerpt : listItem.tweetCopy,
							tweetUrl : encodeURIComponent( listItem.tweetCopy.replace(/(<([^>]+)>)/ig,"") ) // Strip html tags and encodeURIComponent
						});
					};
					break;

				case 'Work': // Work - Could be large or small
					var itemSize = listItem.catWorkType[0] == 'Big' ? 2 : 1;

					item.set({
						image 		: listItem.listingImageSmall_sprite_,
						imageLarge 	: listItem.listingImageBig_sprite_,
						size 		: itemSize,
						type 		: listItem.systemFields.contentType
					});
					break;

				case 'Job': // Job
					item.set({
						excerpt : listItem.teaser
					});
					break;

				case 'People': // Quote or Picture
					item.set({
						type: listItem.catPeopleType[0]
					});
					if(listItem.catPeopleType[0] == 'Picture'){
						item.set({
							data 		: listItem.date,
							excerpt 	: listItem.pictureCaption,
							image 		: listItem.listingPicture_sprite_,
							imageLarge 	: listItem.picture,
							title 		: listItem.name
						});
					} else if (listItem.catPeopleType[0] == 'Quote'){
						item.set({
							excerpt 	: listItem.quote
						});
					}
					break;
			}; // switch

			this.add(item);
		},
		JSONItemsExist: function(listItem, item){
			// if items exist, add them
			if(typeof listItem.writtenBy === 'object'){
				item.set({
					author: {
						id 		: listItem.writtenBy.id,
						image 	: listItem.writtenBy.cornerImage_sprite_,
						name 	: listItem.writtenBy.name,
						twitter : listItem.writtenBy.twitterHandle
					}
				});
			}; // writtenBy == object
			if(typeof listItem.detailUrl === 'string'){
				item.set({
					url: listItem.detailUrl
				});
			}; // detailUrl == string
			if(typeof listItem.catCity === 'object'){
				item.set({
					location: listItem.catCity[0]
				});
			};
		}
	});

	List.Views.TimelineView = Backbone.View.extend({
		el: $('body'),
		events: {
			'click .timeline a': 'renderDate'
		},
		initialize: function(){
			_.bindAll(this, 'renderDate', 'filterDate', 'update');
			this.update();
		},
		renderDate: function(e){
			e.preventDefault();
		},
		filterDate: function(date){
			var self = this,
				list = List.Views.ListView,
				startDate = date;

			_.each(List.Views.ListView.collection.models, function(item, i){
				if(i >= list.limit){
					return false;
				};
				var itemDate = new Date(item.get('date'));
				if(itemDate.getTime() - startDate.getTime() > 0){
					return true;
				} else {
					return false;
				};
			});
		},
		update: function(){
			var self = this;
			$(window).scroll(
				_.throttle(function(){
					var scrollPos = $(window).scrollTop() + $('.nav-main').eq(1).height();
					_.all(self.collection.where({visible: true}), function(model){
						var $model = $('#item-' + model.get('nosId'));

						if($model.length == 0) return;

						if($model.offset().top > scrollPos){

							function changeCurrentMonth(){
								$('.timeline').find('li').filter(function(){
									return $(this).data('date') == model.get('date').substring(0,7);
								}).addClass('is-active').siblings().removeClass('is-active');
							};

							var modelYear = model.get('date').substring(0,4),
								timelineItemYear = $('.timeline').find('.is-active').attr('data-date').substring(0,4), // .attr used over data because data retains the type - this must be a string
								toMonth = modelYear > timelineItemYear ? '12' : '01';

							if(modelYear !== timelineItemYear){
								$('.timeline').load('/home?date=' + modelYear + '-' + toMonth + ' .timeline > ul', function(){
									changeCurrentMonth();
								});
							} else {
								changeCurrentMonth();
							}

							return false;
						} else {
							return true;
						}
					});
					//console.timeEnd('profile 1');
				}, 500)
			);
		}
	});

	List.Views.NavView = Backbone.View.extend({
		el: $('.menu-main'),
		currentType: 'All',
		initialize: function(){
			_.bindAll(this, 'updateCurrentType');
		},
		breadcrumbs: [],
		// popStateUpdate: function(){
		// 	var $lastItem = this.breadcrumbs.pop();

		// 	$lastItem.addClass('is-active').siblings().removeClass('is-active');
		// },
		updateCurrentType: function(){
			// this.currentType = $('.is-active', this.el).find('a').data('type');
			// listView.hideItems( listView.collection.models );
		}
		// updateListing: function(e){
		// 	//window.test = arguments.callee
		// 	console.log('fire')
		// 	var $el = this.$el.find('.is-active'),
		// 		href = $el.get(0).href,
		// 		type = $el.data('type');

		// 	app.history.push( [ type, $el ]);

		// 	this.breadcrumbs.push($el);
		// 	//history.pushState(null, null, href);
		// 	listView.renderFilter(type);

		// 	return this;
		// },
		// reverseListing: function(){
		// 	this.history.pop();
		// 	var breadcrumbLength = this.breadcrumbs.length,
		// 		$el = this.breadcrumbs[breadcrumbLength - 1];

		// 		console.log(breadcrumbLength, $el)
		// 		//type = $el.data('type');

		// 	//listView.renderFilter(type);

		// }
	});

	List.Views.ItemView = Backbone.View.extend({
		initialize: function(){
			_.bindAll(this, 'render', 'template');
			this.render();
		},
		template: function(model){
			switch( model.get('type') ){
				case 'Blog':
					return 'native/applications/native/templates/app/templates/blog';
					break;

				case 'Tweet':
					return 'native/applications/native/templates/app/templates/tweet';
					break;

				case 'Work':
					return 'native/applications/native/templates/app/templates/work';
					break;

				case 'Job':
					return 'native/applications/native/templates/app/templates/job';
				 	break;

				 case 'Quote':
					return 'native/applications/native/templates/app/templates/quote';
			 		break;

				 case 'Picture':
					return 'native/applications/native/templates/app/templates/picture';
			 		break;
			}; // switch
		},
		render: function(){
			var templ = app.fetchTemplate( this.template(this.model) );
			return templ( this.model.toJSON() );
		}
	});


	List.Views.ListView = Backbone.View.extend({
		el: $('body'),
		events: {
			'click .load-more': 'loadMore'
		},
		limit: 15,
		currentType: null,
		initialize: function(){
			_.bindAll(this, 'template', 'render', 'renderSuccess', 'addJSONItems', 'writeItems', 'appendItem', 'filter', 'renderFilter', 'appendFiltered', 'hideItems', 'showItems', 'showInitialItems', 'JSONItemsExist', 'toggleItems', 'loadMore', 'checkForMore');
			this.collection = new List.Collection();
			this.timeline = new List.Views.TimelineView({
				collection: this.collection
			});
			this.nav = new List.Views.NavView();
			//$('.content-wrap').on('click', '.load-more', this.loadMore);

			var self = this;

			this.render().autoLoadMore();
		},
		render: function(){
			$.ajax({
				url: '/native/rest/native/en/timeline/getItems',
				dataType: 'json',
				success: this.renderSuccess
			});

			return this;
		},
		renderSuccess: function(res){
			this.JSONListing = res.data;
			this.addJSONItems();
		},
		addJSONItems: function(){
			_.each(this.JSONListing, this.collection.formatItems);

			var self = this;
			_.each(this.collection.models, function(item, i){
				item.on('change:visible', self.toggleItems);
			});
		},
		appendItem: function(model, i){
			var itemView = new List.Views.ItemView({
					model: model
				});

			$itemView = $(itemView.render());
			$itemView.appendTo( $('#main').find('.listing') );
		},
		filter: function(type){
			if(typeof type !== 'undefined'){
				var items = this.collection.where({contentType: type});
			} else {
				var items = this.collection.where({contentType: this.nav.currentType});
			}

			if(this.nav.currentType == 'All') {
				items = this.collection.models;
			};
			return items;
		},
		appendFiltered: function(exclude){ // exclude is an array
			var items = this.filter(), // all items of current category type
				lastVisibleItemIndex = _.indexOf(items, _.last(exclude) ),
				targetIndex = lastVisibleItemIndex + this.limit;
			// Find the last visible item in the array
			// Find the indexOf that item in all items (filtered by type)
			// Slice the array from that point + limit
			this.showItems(exclude).showItems( items.slice(lastVisibleItemIndex, targetIndex) );

			// if there are no more items to load, remove the load more button
			if(_.rest(items, lastVisibleItemIndex + 1).length < 1){
				$('.load-more').slideUp(function(){
					$(this).remove();
				});
			};
		},
		hideItems: function(items){
			_.each(items, function(item){
				item.set({ visible: false });
			});
			return this;
		},
		showItems: function(items){
			_.each(items, function(item){
				item.set({ visible: true });
			});
			return this;
		},
		toggleItems: function(model, visible){

			if(visible === true){
				this.appendItem(model);
			} else {
				$('#item-' + model.get('nosId')).remove();
			}
		},
		checkForMore: function(){

		},
		loadMore: function(e){
			if (typeof e === 'object'){
				e.preventDefault();
			};
			if($('#content').find('.listing').children().length){
				var self = this,
					visibleNosIds = [],
					visibleItems = [];

				$('.listing', '#main').children().each(function(){
					visibleNosIds.push( parseFloat($(this).attr('id').substring(5)) );
				});
				_.each(visibleNosIds, function(nosId, i){
					if($('#item-' + nosId).length && typeof $('#item-' + nosId).data('json') === 'undefined'){
						//self.collection
						$('#item-' + nosId).remove();
					}

					visibleItems.push( self.collection.where({ nosId: nosId })[0] );
				});
				var contentTypeArr = [],
					validItems = _.compact(visibleItems);

				_.each(validItems, function(item){
					contentTypeArr.push(item.get('contentType'));
				});
				var contentTypeUnique = _.unique(contentTypeArr);
				if(contentTypeUnique.length < 2 && !$('.menu-main').find('.is-active').hasClass('menu-main-all') ){
					this.nav.currentType = contentTypeUnique[0];
				} else {
					this.nav.currentType = 'All';
				}

				this.appendFiltered(validItems); // hide visible items
			} else { // if items exist
				$('.menu-main').find('.menu-main-all').addClass('is-active');
				$('.load-more').slideUp();
			};
		},
		autoLoadMore: function(){
			/*var scrollTimer = window.setInterval(function(){
				if($('.load-more').length){
					var scrollPosition = $('body').scrollTop() ? $('body').scrollTop() : $('html').scrollTop(),
						bottomPosition = $('#footer').position().top,
						windowHeight = $(window).height();

					if ( (scrollPosition + windowHeight) >= bottomPosition - 20 ) {
						$('.load-more').trigger('click');
					};
				};
			}, 2);*/
		}
	});

	return List;

});
