define([
	box.lib('backbone'),
	box.lib('lodash'),
	box.lib('jquery-nc'),

	//project
	box.pkg('view/menu/menu_item'),
	box.pkg('event/menu_events'),
	box.pkg('event/app_events')
],
function (Backbone, _, $, MenuItem, MenuEvents, AppEvents) {
	var EL = '.js-menu';
	var Menu = Backbone.View.extend({
		buttons: null,
		el: null,
		model: null,

		initialize: function () {
			_.bindAll(this);
			this.buttons = [];
			var menu = this.model.getMenu();
			this.createMenu(menu);
			box.event.addListener(null, MenuEvents.disable, this.onDisable);
			box.event.addListener(null, MenuEvents.enable, this.onEnable);
			box.event.addListener(null, MenuEvents.highlight, this.onHighlight);
		},

		createMenu: function (menu) {
			for (var i = 0, len = menu.length; i < len; i++) {
				var item = new MenuItem({container: EL, menuId: menu[i].id, number: i + 1, title: menu[i].title});
				this.buttons.push(item);
				box.event.addListener(item, MenuEvents.activate, this.onActivateMenu);
			}
			// for link blocks, etc.
			box.event.addListener(null, MenuEvents.activate, this.onActivate);
			this.setActive(menu[0].id);
			this.onDisable(null);
		},

		onActivateMenu: function (e) {
			box.event.dispatch(null, AppEvents.loadScene, {scene: e.data.id});
		},

		onActivate: function (e) {
			this.setActive(e.data.id);
		},

		setActive: function (id) {
			for (var i = 0, len = this.buttons.length; i < len; i++) {
				if (this.buttons[i].menuId == id) {
					this.buttons[i].setActive();
					break;
				}
			}
		},

		onEnable: function (e) {
			for (var i = 0, len = this.buttons.length; i < len; i++) {
				this.buttons[i].enable();
			}
		},

		onDisable: function (e) {
			for (var i = 0, len = this.buttons.length; i < len; i++) {
				this.buttons[i].disable();
			}
		},

		onHighlight: function (e) {
			this.buttons[e.data.index].highlight();
		}

	});

	return Menu;
});