define([
	box.lib('backbone'),
	box.lib('lodash'),
	box.lib('jquery-nc'),

	//project
	box.pkg('event/player_events'),

	//partials
	'text!partial/player.html'
],
function (Backbone, _, $, PlayerEvents, ApiTemplate) {
	var FAIL_LIMIT = 1;
	var FAIL_RETRY_BASE = 1000;
	var FAIL_RETRY_EXPONENT = 3;
	var SCRIPT_CLASS = 'js-youtube-api';

	var Player = Backbone.View.extend({
		model: null,
		videoId: null,
		failCount: null,

		initialize: function (params) {
			_.bindAll(this);
			this.isReady = true;
			box.event.addListener(null, PlayerEvents.loadVideo, this.onLoadVideo);
		},

		connectApi: function (videoId) {
			this.resetFail();
			// leave it the same otherwise (for recovering from fail state)
			if (videoId)
				this.videoId = videoId;
			window.onYouTubePlayerAPIReady = this.onYouTubePlayerAPIReady;
			window.onYouTubeIframeAPIReady = this.onYouTubePlayerAPIReady;

			$('head').append($('<script>', {
				src: 'https://www.youtube.com/player_api',
				'class': SCRIPT_CLASS
			}));
		},

		onYouTubePlayerAPIReady: function () {
			try { 
			    delete window.onYouTubePlayerAPIReady; 
			    delete window.onYouTubeIframeAPIReady; 
			} 
			catch(e) { // delete doesn't exist in IE8
				// ensure undefined hasn't been overwritten
				var undefined = (function(u) { return u; })();
			    window["onYouTubePlayerAPIReady"] = undefined; 
			    window["onYouTubeIframeAPIReady"] = undefined; 
			}
			this.createPlayer();
		},

		createPlayer: function () {
			this.player = new YT.Player('player', {
				height: '535',
				width: '962',
				videoId: this.videoId,
				events: {
					'onReady': this.onPlayerReady,
					'onStateChange': this.onPlayerStateChange

					// disabled due to bugginess, especially in IE.
					//'onError': this.onPlayerError
				},
				playerVars: {
					autoplay: 0,
					controls: 0,
					disablekb: 1,
					enablejsapi: 1,
					modestbranding: 1,
					rel: 0,
					showinfo: 0,
					title: '',
					wmode:'opaque'
				}
			});
		},
		
		resetFail: function () {
			this.failCount = 0;
		},

		onPlayerReady: function (e) {
			box.event.dispatch(null, PlayerEvents.ready, {player: this.player});
		},

		onPlayerStateChange: function (e) {
			this.resetFail();
			box.event.dispatch(null, PlayerEvents.stateChange);
		},

		onPlayerError: function (e) {

			this.failCount ++;

			this.$el.html('')
				.append($('<div>', {
					id: 'player'
				}));
			if (this.failCount > FAIL_LIMIT) {
				var self = this;
				this.showError('Connection error, please refresh the page.', function () {
					//self.resetFail();
					//self.createPlayer();
					window.location.reload();
				}, 'Refresh');
				return;
			}
			var time = Math.ceil(FAIL_RETRY_BASE);// * Math.pow(this.failCount, FAIL_RETRY_EXPONENT));
			console.error('YT failed', time);
			setTimeout(this.createPlayer, time);
		},

		/**
		 * Quick hack to add an error to the page
		 * TODO make this a real class
		 * @param  {[type]} message    [description]
		 * @param  {[type]} onAccept   [description]
		 * @param  {[type]} acceptText [description]
		 * @return {[type]}            [description]
		 */
		showError: function (message, onAccept, acceptText) {
				var errorClass = 'error-box';

				var $button = $('.js-error').find('.js-button');
				var $message = $('.js-error').find('.js-message');
				if (!acceptText)
					acceptText = 'OK';
				$message.text(message);
				$button.text(acceptText);
				$button.click(onAccept);
				$('.js-error').show();
				
				$('.' + errorClass).unbind('click').click(onAccept);
		},

		dispose: function () {
			this.player = null;
			if (window.onYouTubePlayerAPIReady)
				delete window.onYouTubePlayerAPIReady;
			
			this.$el.html('');
		},

		onLoadVideo: function (e) {
			var videoId = e.data.videoId;
			if (this.videoId == null){
				this.connectApi(videoId);
			}else if (this.player && typeof this.player.loadVideoById != 'undefined'){
				this.player.loadVideoById(videoId);
				$.data(this.player,"videoId", videoId);
			}
		}

	});

	return Player;
});
