// Set the require.js configuration for your application.
require.config({
	// Initialize the application with the main application file
	deps: [
		'respond',
		'main'
	],

	paths: {
		// JavaScript folders
		header: '../app/modules/header',
		libs: 'vendor',
		plugins: 'plugins',

		// Libraries
		respond: 'vendor/respond.src',
		greensock: 'vendor/greensock',
		modernizr: 'vendor/modernizr-2.5.3.min',
		jquery: 'vendor/jquery-1.7.2.min',
		lodash: 'vendor/lodash',
		backbone: 'vendor/backbone-min',
		swfobject: 'vendor/swfobject'
	},

	shim: {
		backbone: {
			deps: ['lodash', 'jquery'],
			exports: 'Backbone'
		},
		plugins: {
			deps: ['jquery']
		}
	}
});
