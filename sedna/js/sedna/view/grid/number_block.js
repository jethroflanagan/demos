define([
	box.lib('backbone'),
	box.lib('lodash'),
	box.lib('jquery-nc'),

	//project
	box.pkg('view/grid/grid_block')
],
function (Backbone, _, $, GridBlock) {
	var _super = GridBlock.prototype;

	var NumberBlock = GridBlock.extend({
		startValue: null,
		currentIteration: null,
		changeInValue: null,
		numIterations: null,
		targetValue: null,
		countUpDecimals: null,
		padding: null,

		initialize: function (params) {
			_.bindAll(this);
			_super.initialize.apply(this, [params]);
			this.padding = 0;
			if (params.options.countUpDecimals) {
				this.countUpDecimals = params.options.countUpDecimals * 10; // to get an actual decimal place
				this.padding = params.options.countUpDecimals;
			}
			else {
				this.countUpDecimals = 0;
			}
			/*if (params.options.numberPadding) {
				this.padding = params.options.numberPadding;
			}*/
		},

		prepContent: function () {
			_super.prepContent.apply(this);
			var $target = this.$el.find('.js-number');
			var target = $target.text();
			this.targetValue = parseInt(target);
			this.padding += target.length;
			this.startValue = 0;

			$target.text(this.padNumber(this.startValue));
		},

		showContent: function () {
			_super.showContent.apply(this);

			this.currentIteration = 0;
			this.numIterations = 100;
			this.changeInValue = this.targetValue - this.startValue;

			this.countUp();
		},

		countUp: function () {
			var currentValue = this.easeOutExpo(++this.currentIteration, this.startValue, this.changeInValue, this.numIterations);
			if (this.countUpDecimals)
				currentValue = Math.round(currentValue * this.countUpDecimals) / this.countUpDecimals;
			else
				currentValue = Math.round(currentValue);
			if (this.padding)
				currentValue = this.padNumber(currentValue);
			this.$el.find('.js-number').text(currentValue);
			if (currentValue < this.targetValue) {
				setTimeout(this.countUp, 12);
			}
		},

		padNumber: function (val) {
			var str = val.toString();
			for (var i = str.length, len = this.padding; i < len; i++) {
				str = '0' + str;
			}
			return str;
		},

		// from Robert Penner's equations
		easeOutExpo: function (currentIteration, startValue, changeInValue, numIterations) {
			return changeInValue * (-Math.pow(2, -10 * currentIteration / numIterations) + 1) + startValue;
		}

	});

	return NumberBlock;
});