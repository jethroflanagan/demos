package za.co.nativecode.tmon.youtubeplayer.service 
{
	import flash.display.DisplayObject;
	import org.robotlegs.mvcs.Actor;
	import za.co.nativecode.tmon.youtubeplayer.model.analytics.Analytics;
	import za.co.nativecode.tmon.youtubeplayer.model.analytics.AnalyticsTypeInfo;
	import za.co.nativecode.tmon.youtubeplayer.model.global.App;
	
	/**
	 * @author Bradley Botha
	 */
	public class AnalyticsService extends Actor 
	{
		//public var tracker:AnalyticsTracker;
		
		public function init(display:DisplayObject):void
		{
			//trace("AnalyticsService: Init");
			
			if (App.mode == App.LIVE)
			{
				tracker = new GATracker(display, Analytics.CODE, "AS3", false);
				tracker.setDomainName(Analytics.DOMAIN);
			}
		}
		
		public function trackPage(info:AnalyticsTypeInfo):void
		{
			trace("track page", info.path);
			
			if (tracker)
				tracker.trackPageview(info.path);
		}
		
		public function trackEvent(info:AnalyticsTypeInfo):void
		{
			trace("track event", info.category, info.action, info.label);
			
			if (tracker)
				tracker.trackEvent(info.category, info.action, info.label);
		}
	}

}