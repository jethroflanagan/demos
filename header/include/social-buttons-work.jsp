	<div class="clearfix tweet" onClick="_gaq.push(['_trackEvent', 'Work', 'Work-share-twitter', 'Work-share-<WORK TITLE>']);">
	    <a href="https://twitter.com/share" class="twitter-share-button" data-count="none" data-url="<%= ApplicationManager.getBaseURL(app.getName()) + request.getRequestURI() + "?" + request.getQueryString() %>" data-text="<ccs:schemeAttribute name="Title" /> via @NATIVE">Tweet</a>
	</div>
	<div class="clearfix gplus" onClick="_gaq.push(['_trackEvent', 'Work', 'Work-share-googleplus', 'Work-share-<WORK TITLE>']);">
	    <div class="g-plusone" data-size="medium"></div>
	</div>
	<div class="clearfix fb-like" onClick="_gaq.push(['_trackEvent', 'Work', 'Work-share-facebook', 'Work-share-<WORK TITLE>']);">
	    <div class="fb-like" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false"></div>
	</div>
