<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->

<c:set var="baseUrl" value="<%= ApplicationManager.getBaseURL(app.getName())%>"></c:set>

<cs:head descriptionContentFields="Title">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=0.5, maximum-scale=1.0, minimum-scale=0.5">

	<title><ccs:pageTitle showDetail="true" detailTitleFields="Title"/> | The Making of Native</title>
	<meta name="viewport" content="width=device-width">
	<link rel="stylesheet" href="${templatePath}css/style.css">
	<script src="${templatePath}js/vendor/modernizr-2.5.3.min.js"></script>
	<script src="${templatePath}js/vendor/respond.src.js"></script>
	<script data-main="${templatePath}js/config" src="${templatePath}js/vendor/require.js"></script>
	<script type="text/javascript" src="http://use.typekit.com/cse4ffs.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

	<meta property="og:image" content="http://tiger-ext.native.co.za/native/applications/native/templates/img/native-fb.jpg" />  <!-- I have major concerns around this - not generic, what happens when you go live?? -->
	<cs:facebookMeta
           siteName="The Making of NATIVE" 
           titleFieldNames="Title" 
           descriptionFieldNames="Listing Teaser" 
           contentType="Work"
           baseUrl="${baseUrl}"
   />
</cs:head>
