box.configLoaded( // ![ignore]
{
	"scripts": [
		{
			"file": "debug.js"
		},
		{
			"file": "global.js"
		},
		{
			"file": "event.js"
		},
		{
			"file": "util.js"
		},
		{
			"file": "polyfills.js"
		}
	],
	"box": {
		"SCRIPT_ID": "box",
		"IS_DEBUG": false,
		"IS_VERBOSE": false
	}
}
); // ![ignore]