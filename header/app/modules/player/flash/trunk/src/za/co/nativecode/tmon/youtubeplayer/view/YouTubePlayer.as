package za.co.nativecode.tmon.youtubeplayer.view 
{
	import assets.BigPlayButton;
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.system.Security;
	import flash.utils.Timer;
	import za.co.nativecode.tmon.youtubeplayer.event.UIControllsEvent;
	import za.co.nativecode.tmon.youtubeplayer.model.vo.YouTubeVideoEntry;
	/**
	 * ...
	 * @author Bradley Botha
	 */
	public class YouTubePlayer extends Sprite
	{
		private const DOMAIN_YOUTUBE:String = "http://www.youtube.com";
		private const DOMAIN_YOUTUBE_IMG:String = "http://s.ytimg.com";
		private const PLAYER_URL:String = "http://www.youtube.com/apiplayer?version=3";
		
		private static const STATE_UNSTARTED:Number = -1;
		private static const STATE_ENDED:Number = 0;
		private static const STATE_PLAYING:Number = 1;
		private static const STATE_PAUSED:Number = 2;
		private static const STATE_BUFFERING:Number = 3;
		private static const STATE_CUED:Number = 5;
		
		private var _videoID:String;
		private var _videoPlayerReady:Boolean;
		private var _bigPlayButtonClicked:Boolean;
		private var _displayingVideoPlayer:Boolean;
		
		private var _player:Object;
		private var _playerControlls:PlayerUIControlls;
		private var _timer:Timer;
		private var _thumbnail:Bitmap;
		private var _bigPlayButton:MovieClip;
		private var _thumbnailLoader:Loader;		
		
		public function YouTubePlayer(videoID:String) 
		{
			_videoID = videoID;
			
			trace(_videoID);
			if (_videoID == "" || _videoID == null) trace("no video ID was set! The video ID needs to be passed in as a 'flashVar' - example { videoID:\"h99SBtgWZUs\"};");
			
			Security.allowDomain(DOMAIN_YOUTUBE, DOMAIN_YOUTUBE_IMG);
			
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, kill);
			
			// Start loading in the videos data - and then it's thumbnail image
			createPreVideoUI();
			
			// Start loading in Youtube's chromless player
			getVideoPlayer();
			
			// A timer to monitor the elapsed time of the video
			_timer = new Timer(500, 0);
			_timer.addEventListener(TimerEvent.TIMER, handleTimerTick);
			
			onResize(null);
			stage.addEventListener(Event.RESIZE, onResize);
		}
		
		/**
		 * Create the inital UI state. Users will this state before the click on the player.
		 * It is made up of the big play button, and the videos thumbnail.
		 * The thumbnail is retrieved through data API - This UI state does NOT display the chromless player DisplayObject
		 */
		private function createPreVideoUI():void 
		{
			// Create Big play button (only used to initialize the player)
			_bigPlayButton = new BigPlayButton();
			_bigPlayButton.buttonMode = true;
			addChild(_bigPlayButton);
			
			// Use youtube's data api, to get a URL to the video's thumbnails
			var urlLoader:URLLoader = new URLLoader();
			urlLoader.addEventListener(Event.COMPLETE, onVideoDataLoadComplete, false, 0, true);
			urlLoader.addEventListener(IOErrorEvent.IO_ERROR, handleDataAPIError, false, 0, true);
			urlLoader.load( new URLRequest("http://gdata.youtube.com/feeds/api/videos/" + _videoID) );
			
			stage.addEventListener(MouseEvent.CLICK, handleBigButtonClick, false, 0, true);
			stage.addEventListener(MouseEvent.MOUSE_OVER, handleBigButtonOver, false, 0, true);
			stage.addEventListener(MouseEvent.MOUSE_OUT, handleBigButtonOut, false, 0, true);
		}
		
		/**
		 * Called when the initial UI state is clicked.
		 * Remove the big play button, and the thumbnail (if it has been loaded)
		 * If the video player has been download, start playing the video
		 */
		private function handleBigButtonClick(e:MouseEvent):void 
		{
			_bigPlayButtonClicked = true;
			
			removeChild(_bigPlayButton);
			
			stage.removeEventListener(MouseEvent.CLICK, handleBigButtonClick);
			stage.removeEventListener(MouseEvent.MOUSE_OVER, handleBigButtonOver);
			stage.removeEventListener(MouseEvent.MOUSE_OUT, handleBigButtonOut);
			
			// If the thumbnail is still loading, stop it - it is no longer needed
			if (_thumbnailLoader)
				cleanThubnailLoader();
			
			// If the thumbnail has already been loaded and added to the display list - Remove it
			if (_thumbnail)
			{
				removeChild(_thumbnail);
				_thumbnail = null;
			}
			
			// Finally, if the video player is ready (has been loaded), play the video
			if(_videoPlayerReady)
				playVideo();
		}
		
		/**
		 * Set hover state for the inital UI
		 */
		private function handleBigButtonOver(e:MouseEvent):void 
		{
			_bigPlayButton.gotoAndStop(2);
		}
		
		/**
		 * Set normal state for the inital UI
		 */
		private function handleBigButtonOut(e:MouseEvent):void 
		{
			_bigPlayButton.gotoAndStop(1);
		}
		
		/**
		 * Called when the data is retuened from Youtube's data API
		 * This data contains info about the video, it gives us the URL to the video's thumbnail images
		 */
		private function onVideoDataLoadComplete(e:Event):void 
		{
			var urlLoader:URLLoader = e.target as URLLoader;
			var videoEntry:YouTubeVideoEntry = new YouTubeVideoEntry( new XML(urlLoader.data) );
			
			// Find the biggest thubmail (always seems to be the first thumbnail returned, but lets just make sure)
			var biggestWidth:int = 0, biggestWidthIndex:int = 0;
			for (var i:int = 0; i < videoEntry.thumbnails.length; i++) 
			{
				if (videoEntry.thumbnails[i].width > biggestWidth)
				{
					biggestWidth = videoEntry.thumbnails[i].width;
					biggestWidthIndex = i;
				}
			}
			
			// Load the biggest thumbnail found - keep reference to the loader object, so that it can be cancelled, if the user clicks the video, before it is loaded
			_thumbnailLoader = new Loader();
			_thumbnailLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onVideoThumbnailLoadComplete, false, 0, true);
			_thumbnailLoader.load( new URLRequest(videoEntry.thumbnails[biggestWidthIndex].url) );
		}
		
		/**
		 * Called when the thumbnail for the video has been loaded
		 */
		private function onVideoThumbnailLoadComplete(e:Event):void 
		{
			trace("video thumbnail loaded");
			
			_thumbnail = _thumbnailLoader.content as Bitmap;
			_thumbnail.smoothing = true;
			addChildAt(_thumbnail, 0);
			
			cleanThubnailLoader();
			
			updatePreVideoUI();
		}
		
		/**
		 * Called when the thumbnail, or the videos info couldn't be retrieved
		 */
		private function handleDataAPIError(e:ErrorEvent):void 
		{
			// No error state in the design
			trace("ERROR!!!!!!!!", e);
		}
		
		/**
		 * Stops the Loader object from loading the thumbnail (if it is still running), remove listeners etc...
		 */
		private function cleanThubnailLoader():void 
		{
			_thumbnailLoader.unloadAndStop(true);
			_thumbnailLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onVideoThumbnailLoadComplete);
			_thumbnailLoader = null;
		}
		
		/**
		 * Start loading the chromeless player
		 */
		private function getVideoPlayer():void 
		{
			var loader:Loader = new Loader();
			loader.contentLoaderInfo.addEventListener(Event.INIT, handlePlayerLoaded, false, 0, true);
			loader.load( new URLRequest(PLAYER_URL) );
		}
		
		/**
		 * Called when the chromless player has been downloaded.
		 */
		private function handlePlayerLoaded(e:Event):void 
		{
			_player = DisplayObject(LoaderInfo(e.target).content);
			
			_player.addEventListener("onReady", handlePlayerReady);
			_player.addEventListener("onError", handlePlayerError);
			_player.addEventListener("onStateChange", handlePlayerStateChange);
		}
		
		/**
		 * Called owhen the player has been downloaded, and fully initialized.
		 * If the user has already clicked the big play button - start playing the video, else do nothing
		 */
		private function handlePlayerReady(e:Event):void 
		{
			trace("video player loaded");
			
			_videoPlayerReady = true;
			
			if (_bigPlayButtonClicked)
				playVideo();
		}
		
		/**
		 * Adds the chromless player onto the display list, and plays the video.
		 * The player's UI controlls are also created.
		 */
		private function playVideo():void 
		{
			_displayingVideoPlayer = true;
			
			// Add the video player, and start playing the video
			addChild(_player as DisplayObject);
			_player.loadVideoById(_videoID);
			
			// Add the player's UI controlls
			_playerControlls = new PlayerUIControlls();
			addChild(_playerControlls);
			addUIControllsListeners();
			
			onResize(null);
		}
		
		/**
		 * Add listeners for the player's UI controlls
		 */
		private function addUIControllsListeners():void 
		{
			_playerControlls.addEventListener(UIControllsEvent.PLAY, handleUIControllsPlayClicked, false, 0, true);
			_playerControlls.addEventListener(UIControllsEvent.PAUSE, handleUIControllsPauseClicked, false, 0, true);
			_playerControlls.addEventListener(UIControllsEvent.SCRUB_TIMELINE, handleUIControllsTimelineScrubbed, false, 0, true);
			_playerControlls.addEventListener(UIControllsEvent.SCRUB_VOLUME, handleUIControllsVolumeScrubbed, false, 0, true);
		}
		
		/**
		 * Remove listeners for the player's UI controlls
		 */
		private function removeUIControllsListeners():void 
		{
			_playerControlls.removeEventListener(UIControllsEvent.PLAY, handleUIControllsPlayClicked);
			_playerControlls.removeEventListener(UIControllsEvent.PAUSE, handleUIControllsPauseClicked);
			_playerControlls.removeEventListener(UIControllsEvent.SCRUB_TIMELINE, handleUIControllsTimelineScrubbed);
			_playerControlls.removeEventListener(UIControllsEvent.SCRUB_VOLUME, handleUIControllsVolumeScrubbed);
		}
		
		private function handlePlayerError(e:Event):void 
		{
			// No error state in the design
			trace("ERROR!!", e);
		}
		
		/**
		 * Called whenever the video players state changes (playing/paused/buffering etc...)
		 */
		private function handlePlayerStateChange(e:Event):void 
		{
			var data:Object = (e as Object).data;
			
			switch (data)
			{
				case STATE_UNSTARTED:
					break;
				  
				case STATE_ENDED:
					_timer.stop();
					break;
				  
				case STATE_PLAYING:					
					_timer.start();
					
					_playerControlls.setPlaying();
					_playerControlls.updateDurationTime( _player.getDuration() );
					break;
				  
				case STATE_PAUSED:
					//_timer.stop();					
					_playerControlls.setPaused();
					break;
				  
				case STATE_BUFFERING:
					break;
				  
				case STATE_CUED:
					break;
			}
		}
		
		/**
		 * Called every 500ms by the Timer object
		 * This allows the player's UI controlls to update it's elaped time etc...
		 */
		private function handleTimerTick(e:TimerEvent):void 
		{
			_playerControlls.updateElapsedTime( _player.getCurrentTime() );
			
			_playerControlls.updateTimelineScrubber( _player.getCurrentTime() /  _player.getDuration() );
		}
		
		// -------------------------------- UI Controlls Events -------------------------------- \\
		
		private function handleUIControllsPlayClicked(e:UIControllsEvent):void 
		{
			_player.playVideo();
		}
		
		private function handleUIControllsPauseClicked(e:UIControllsEvent):void 
		{
			_player.pauseVideo();
		}
		
		private function handleUIControllsTimelineScrubbed(e:UIControllsEvent):void 
		{
			_player.seekTo( _player.getDuration() * e.data.scrubberPosition );
		}
		
		private function handleUIControllsVolumeScrubbed(e:UIControllsEvent):void 
		{
			_player.setVolume(e.data.scrubberPosition * 100);
		}
		
		// -------------------------------- UI Controlls Events -------------------------------- \\
		
		
		private function onResize(e:Event):void 
		{
			// When a resize evnt occurs during the video watching state
			if (_displayingVideoPlayer)
			{
				// update player size
				_player.setSize(stage.stageWidth, stage.stageHeight);
			}
			else // When a resize evnt occurs in the initial UI state
			{
				updatePreVideoUI();
			}			
		}
		
		private function updatePreVideoUI():void 
		{
			if (_thumbnail)
			{
				_thumbnail.width = stage.stageWidth;
				_thumbnail.scaleY = _thumbnail.scaleX;
				if (_thumbnail.height > stage.stageHeight)
				{
					_thumbnail.height = stage.stageHeight;
					_thumbnail.scaleX = _thumbnail.scaleY;
				}
				
				_thumbnail.x = stage.stageWidth * 0.5 - _thumbnail.width * 0.5;
				_thumbnail.y = stage.stageHeight * 0.5 - _thumbnail.height * 0.5;
			}
			if (_bigPlayButton)
			{
				_bigPlayButton.x = stage.stageWidth * 0.5 - _bigPlayButton.width * 0.5;
				_bigPlayButton.y = stage.stageHeight * 0.5 - _bigPlayButton.height * 0.5;
			}
		}
		
		private function kill(e:Event):void 
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, kill);
			
			stage.removeEventListener(Event.RESIZE, onResize);
			
			if (_player)
			{
				_player.removeEventListener("onReady", handlePlayerReady);
				_player.removeEventListener("onError", handlePlayerError);
				_player.removeEventListener("onStateChange", handlePlayerStateChange);
				
				_player.destroy();				
				_player = null;
			}
			
			_timer.stop();
			_timer.removeEventListener(TimerEvent.TIMER, handleTimerTick);
			_timer = null;
			
			if (_playerControlls)
			{
				removeUIControllsListeners();
				_playerControlls = null;
			}
			
		}
		
	}

}