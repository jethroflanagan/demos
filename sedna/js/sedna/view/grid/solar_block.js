define([
	box.lib('backbone'),
	box.lib('lodash'),
	box.lib('jquery-nc'),

	//project
	box.pkg('view/grid/grid_block')
],
function (Backbone, _, $, GridBlock) {
	
	var ORIGIN_X = 108;
	var ORIGIN_Y = 106;

	var MAJOR_AXIS = 130;
	var MINOR_AXIS = 41;
	var ROTATION = -Math.PI / 4.16;
	var MAX_DEGREES = 360;

	var _super = GridBlock.prototype;
	var SolarBlock = GridBlock.extend({

		angle: 0,
		halfImageWidth: null,
		halfImageHeight: null,
		$planetoid: null,

		initialize: function (params) {
			_.bindAll(this);
			_super.initialize.apply(this, [params]); 
		},
		
		prepContent: function () {
			_super.prepContent.apply(this);
			this.$planetoid = this.$el.find('.js-planetoid');
			this.halfImageWidth = this.$planetoid.width() / 2;
			this.halfImageHeight = this.$planetoid.height() / 2;
			this.angle = 100;
			this.placePlanetoid();
		},

		showContent: function () {
			_super.showContent.apply(this);
			this.setDelay(this.hideLabel, 1300);
		},

		hideLabel: function () {
			this.$el.find('.js-label').fadeOut();
			this.movePlanetoid();
		},

		movePlanetoid: function () {
			this.angle += 1;
			if (this.angle > MAX_DEGREES)
				this.angle = 0;
			this.placePlanetoid();
			this.addAnimation(this.movePlanetoid);
		},

		placePlanetoid: function () {
			var maxDegrees = 360;
			var point = this.getEllipsePoint(this.angle / maxDegrees);
			this.$planetoid.css({ left: point.x - this.halfImageWidth, top: point.y - this.halfImageHeight });
		},

		getEllipsePoint: function (percent) {
			var angle = percent * Math.PI * 2;
			//base ellipse
			var x = MAJOR_AXIS * Math.cos(angle);
			var y = MINOR_AXIS * Math.sin(angle);

			//rotatedEllipse, rotate the coord system
			var rotatedX = x * Math.cos(ROTATION) - y * Math.sin(ROTATION) + ORIGIN_X;
			var rotatedY = x * Math.sin(ROTATION) + y * Math.cos(ROTATION) + ORIGIN_Y;
			return {x: rotatedX, y: rotatedY};
		},

		//draw(108, 106, 130, 41, -Math.PI / 4.16)
		debug: function () {
			var canvas, ctx;

			canvas = $('canvas').get(0); // get dom element
			ctx = canvas.getContext("2d");

			// move into the middle of the canvas, just to make room
			//ctx.translate(30, 30);
			

			// initial offset ROTATION so our star is straight

			// make a point, 5 times
			for (var i = 0; i < 360; i++) {
			    var angle = i / 360 * Math.PI * 2;
			    //base ellipse
			    var x = MAJOR_AXIS * Math.cos(angle);
			    var y = MINOR_AXIS * Math.sin(angle);
			    
			    //rotatedEllipse, rotate the coord system
			    var rotatedX = x * Math.cos(ROTATION) - y * Math.sin(ROTATION) + ORIGIN_X;
			    var rotatedY = x * Math.sin(ROTATION) + y * Math.cos(ROTATION) + ORIGIN_Y;
			    if (i == 0)
			        ctx.moveTo(rotatedX, rotatedY);
			    else
			        ctx.lineTo(rotatedX, rotatedY);
			}
			// last line to connect things up
			ctx.closePath();
			ctx.strokeStyle = '#ff0000';
			ctx.stroke();

		}
	});

	return SolarBlock;
});