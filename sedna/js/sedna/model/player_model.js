define([
	box.lib('backbone'),
	box.lib('lodash'),
	box.lib('jquery-nc'),

	//project
	box.pkg('model/capability'),
	box.pkg('event/player_events'),
	box.pkg('event/timing_events')
],
function (Backbone, _, $, Capability, PlayerEvents, TimingEvents) {
	var STOP_BEFORE_END = 0.9; //seconds, float

	var PLAYER_UNSTARTED = -1;
	var PLAYER_ENDED = 0;
	var PLAYER_PLAYING = 1;
	var PLAYER_PAUSED = 2;
	var PLAYER_BUFFERING = 3;
	var PLAYER_CUED = 5;

	var PlayerModel = Backbone.Model.extend({
		player: null,
		timeouts: null,
		isStopped: null,
		forcePause: null,
		duration: null,

		initialize: function () {
			_.bindAll(this);
			this.timeouts = {
				monitorTime: null
			};

			this.setupListeners();
		},

		setupListeners: function () {
			box.event.addListener(null, PlayerEvents.ready, this.onPlayerReady);
			box.event.addListener(null, PlayerEvents.stateChange, this.onPlayerStateChange);
			box.event.addListener(null, PlayerEvents.getReadyStatus, this.onGetReadyStatus);
			box.event.addListener(null, PlayerEvents.loadVideo, this.onLoadVideo);
		},

		onPlayerReady: function (e) {
			this.player = e.data.player;
			this.play();
		},

		onPlayerStateChange: function (e) {
			this.stopMonitoringTime();
			this.isStopped = false;

			// this is unreliable when called constantly e.g. monitorTime(), so just call it on state change (multiple calls, but won't overload api)
			this.duration = this.player.getDuration();

			switch (this.player.getPlayerState()) {
				case PLAYER_UNSTARTED: // unstarted
					break;
				case PLAYER_ENDED: // ended
					this.isStopped = true;
					box.event.dispatch(null, PlayerEvents.done);
					this.player.seekTo(this.player.getDuration() - STOP_BEFORE_END);
					this.player.pauseVideo();
					break;
				case PLAYER_PLAYING: // playing
					if (this.forcePause) {
						this.player.pauseVideo();
						break;
					}
					this.monitorTime(true);
					box.event.dispatch(null, PlayerEvents.playing);
					break;
				case PLAYER_PAUSED: // paused
					this.isStopped = true;
					break;
				case PLAYER_BUFFERING: // buffering
					box.event.dispatch(null, PlayerEvents.buffering);
					break;
				case PLAYER_CUED: // cued
					break;
			}
		},

		/**
		 * Check if the player can load a video
		 */
		onGetReadyStatus: function (e) {
			var isReady = false;
			if (this.player) {
				isReady = this.player.getPlayerState() != -1;
			}
			else {
				isReady = true;
			}
			box.event.dispatch(null, PlayerEvents.readyStatus, { status: isReady });
		},

		play: function () {
			this.forcePause = false;
			// cannot force playback programatically on iOS, must await user interaction
			if (!Capability.isIOS()) {
				try {
					this.player.playVideo();
				}
				catch (e) {
					//console.error('Can\'t play video');
				}
			}
		},

		stopMonitoringTime: function () {
			cancelAnimationFrame(this.timeouts.monitorTime);
			this.timeouts.monitorTime = null;
		},

		monitorTime: function (start) {
			if (start)
				this.timeouts.monitorTime = -1;
			if (!this.timeouts.monitorTime) {
				return;
			}
			var currentTime = this.player.getCurrentTime();
			box.event.dispatch(null, TimingEvents.tick, {time: currentTime});
			// force pause on a video near the end to prevent player chrome from showing
			if (this.player.getPlayerState() == PLAYER_PLAYING && currentTime > this.duration - STOP_BEFORE_END) {
				//console.log('EVENT ME UP DOG!!!!!!!!!!!!!')
				this.player.pauseVideo();
				//this.stopMonitoringTime();
				this.forcePause = true;
				box.event.dispatch(null, PlayerEvents.done);
				return;
			}

			// ease off on the API as otherwise it crashes in IE8.
			if (Capability.isLtIE9()) {
				this.timeouts.monitorTime = setTimeout(this.monitorTime, 300);
			}
			// be more chilled for iOS or it will ignore the timeouts
			else if (Capability.isIOS()) {
				this.timeouts.monitorTime = setTimeout(this.monitorTime, 100);
			}
			else {
				this.timeouts.monitorTime = requestAnimationFrame(this.monitorTime);
			}
		},

		onLoadVideo: function (e) {
			this.forcePause = false;
		},

		dispose: function () {
		}

	});

	return PlayerModel;
});
