define([
	// Libs
	"jquery",
	"lodash",
	"backbone",
	"swfobject"
], 

function($, _, Backbone, swfobject) {
	// Localize or create a new JavaScript Template object.
	var JST = window.JST = window.JST || {};

	// Keep active application instances namespaced under an app object.
	return _.extend({

		// This is useful when developing if you don't want to use a
		// build process every time you change a template.
		//
		// Delete if you are using a different template loading method.
		init: function(){
			$('#slider').plusSlider({
				createPagination: false,
				sliderType: 'fader',
				onInit: function(base){
					//base.$slider.height();
					base.$arrows.addClass('clearfix').find('.prev').addClass('main-sprite main-slider-prev');
					base.$arrows.find('.next').addClass('main-sprite main-slider-next');
				},
				onSlide: function(base){
					//base.el.closest('section').children('h2').find('span').text('lol')
					var $h3 = base.$wrapContainer.closest('section').children('h3');
					$h3.find('.current').text( base.currentSlideIndex + 1 );
					$h3.find('.length').text( base.slideCount );
				}
			});

			//random shitty stuff that needs to be run
			var $areaTemplate = $('.related_posts_area').clone();
			$('.related_posts_area').remove();
			$('.related_posts_scheme').append( $areaTemplate );

			var $areaTemplateTwo = $('.related_work_area').clone();
			$('.related_work_area').remove();
			$('.related_posts_scheme').append( $areaTemplateTwo );
		},
		fetchTemplate: function(path) {
			// Append the file extension.
			path += ".html";

			// Should be an instant synchronous way of getting the template, if it
			// exists in the JST object.
			if (!JST[path]) {
				// Fetch it asynchronously if not available from JST, ensure that
				// template requests are never cached and prevent global ajax event
				// handlers from firing.
				$.ajax({
					url: "/" + path,
					dataType: "text",
					cache: false,
					async: false,

					success: function(contents) {
						JST[path] = _.template(contents);
					}
				});
			}

			// Ensure a normalized return value.
			return JST[path];
		},
		replaceTargetVideo: function(target, videoID){
			var flashvars = {
				videoID: videoID
			};
			var params = {
				menu: "false",
				scale: "noScale",
				allowFullscreen: "true",
				allowScriptAccess: "always",
				bgcolor: "#000000"
			};
			var attributes = {
				id:"YouTubePlayer"
			};
			swfobject.embedSWF("youtube_player.swf", target, "100%", "100%", "9.0.0", "expressInstall.swf", flashvars, params, attributes);
		}, // replaceTargetVideo()
		embedVideo: function(){
			console.log('test')
			var self = this;
			$(".video").each(function(){
				var element = $(this).find("div").first();
				var videoID = element.data("videoID");
				var target = element.attr("id");
				self.replaceTargetVideo(target, videoID);
			});
		}, // embedVideo()
		prettyDate: function(time){
			/*
			 * JavaScript Pretty Date
			 * Copyright (c) 2011 John Resig (ejohn.org)
			 * Licensed under the MIT and GPL licenses.
			 */

			// Takes an ISO time and returns a string representing how
			// long ago the date represents.
			stringTime = time + '';
			var date = new Date(stringTime),
				diff = (((new Date()).getTime() - date.getTime()) / 1000),
				day_diff = Math.floor(diff / 86400);
					
			if ( isNaN(day_diff) || day_diff < 0 /*|| day_diff >= 31 */)
				return;
					
			return (
					diff < 60 && "just now" ||
					diff < 120 && "1 minute" ||
					diff < 3600 && Math.floor( diff / 60 ) + " minutes" ||
					diff < 7200 && "1 hour" ||
					diff < 86400 && Math.floor( diff / 3600 ) + " hours") ||
				day_diff == 1 && "Yesterday" ||
				day_diff < 7 && day_diff + " days" ||
				day_diff < 62 && Math.ceil( day_diff / 7 ) + " weeks" ||
				day_diff < 365 && Math.round( (day_diff / 7) / 4 ) + " months" ||
				Math.round( day_diff / 365 ) + " years";
		},
		// Create a custom object with a nested Views object
		module: function(additionalProps) {
			return _.extend({ Views: {} }, additionalProps);
		},
		history: [],
		scrollTopEl: function(){
			var htmlScrollTop = $('html').scrollTop(), 
				bodyScrollTop = $('body').scrollTop();

			if( $('html').scrollTop() == 0 && $('body').scrollTop() == 0 ) {
				return 'html';
			} else if($('html').scrollTop() !== 0) {
				return 'html';
			} else if($('body').scrollTop() !== 0){
				return 'body';
			}
		}

	// Mix Backbone.Events into the app object.
	}, Backbone.Events);
});
