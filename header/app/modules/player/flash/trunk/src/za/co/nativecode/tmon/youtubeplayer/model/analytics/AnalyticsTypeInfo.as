package za.co.nativecode.tmon.youtubeplayer.model.analytics 
{
	/**
	 * @author Bradley Botha
	 */
	public class AnalyticsTypeInfo 
	{
		static public const EVENT:String = "event";
		static public const PAGE:String = "page";
		
		private var _type:String;
		private var _category:String;
		private var _action:String;
		private var _label:String;
		private var _path:Array = [];
		
		public function AnalyticsTypeInfo()
		{
		}
		
		public function createEvent(category:String, action:String, label:String = ""):AnalyticsTypeInfo
		{
			_category = category;
			_action = action;
			_label = label;

			_type = EVENT;
			
			return this;
		}
		
		public function createPageView(...path:Array):AnalyticsTypeInfo
		{
			_type = PAGE;
			_path = path;
			
			return this;
		}
		
		public function get type():String 
		{ 
			return _type; 
		}
		
		public function get path():String
		{
			return _path.join("/");
		}
		
		public function get category():String
		{
			return _category;
		}
		
		public function get action():String
		{
			return _action;
		}
		
		public function get label():String
		{
			return _label;
		}
		
	}

}