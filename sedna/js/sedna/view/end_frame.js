define([
	box.lib('backbone'),
	box.lib('lodash'),
	box.lib('jquery-nc'),

	//project
	box.pkg('event/end_frame_events')
],
function (Backbone, _, $, 
          EndFrameEvents) {
	var EndFrame = Backbone.View.extend({
		model: null,
		player: null,
		grid: null,
		menu: null,
		endFrame: null,

		initialize: function () {
			_.bindAll(this);
			//box.event.addListener(null, EndFrameEvents.set, this.onSet);
			box.event.addListener(null, EndFrameEvents.show, this.onShow);
			box.event.addListener(null, EndFrameEvents.hide, this.onHide);
		},

		setImage: function (image) {
			this.$el.find('.js-end-frame-image').attr('src', 'img/end_frames/' + image + '.jpg')
				.load(this.onLoaded);
		},

		onLoaded: function (e) {
			//this.$el.show();
		},

		onShow: function (e) {
			this.$el.show();
			this.$el.css({ opacity: 0 });
			this.$el.animate({ opacity: 1 }, 500);	
		},

		onHide: function (e) {
			//this.$el.hide();	
			var self = this;
			this.$el.animate({ opacity: 0 }, { 
				duration: 100, 
				complete: function () {
					self.setImage(e.data.image);
				} 
			});	
		}

	});
	return EndFrame;
});