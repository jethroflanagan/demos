define([],
function () {
	var type = new box.event.EventType('player event');
	var e = box.event.createEvent(type);
	return {
		ready: e('onReady'),
		buffering: e('buffering'),
		stateChange: e('onStateChange'),
		loadVideo: e('loadVideo'),
		getReadyStatus: e('getReadyStatus'),
		readyStatus: e('readyStatus'),
		done: e('done'),
		playing: e('playing')
	};
});