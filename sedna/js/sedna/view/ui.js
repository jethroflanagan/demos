define([
	box.lib('backbone'),
	box.lib('lodash'),
	box.lib('jquery-nc'),

	//project
	box.pkg('model/player_model'),
	box.pkg('view/player'),
	box.pkg('model/menu/menu_model'),
	box.pkg('view/menu/menu'),
	box.pkg('model/grid/grid_model'),
	box.pkg('view/grid/grid'),
	box.pkg('view/end_frame'),
	box.pkg('model/capability'),
	box.pkg('event/app_events'),
	box.pkg('event/player_events'),

	//partials
	'text!partial/ui.html'

],
function (Backbone, _, $, 
          PlayerModel, Player, MenuModel, Menu, GridModel, Grid, EndFrame, Capability, AppEvents, PlayerEvents,
          Template) {
	var Ui = Backbone.View.extend({
		model: null,
		player: null,
		grid: null,
		menu: null,
		endFrame: null,

		initialize: function () {
			_.bindAll(this);
			this.render();
			this.createPlayer();

			this.grid = new Grid({ el: '.js-grid-blocks', model: new GridModel() });
			this.menu = new Menu({ el: '.js-menu', model: new MenuModel() });
			this.endFrame = new EndFrame({ el: '.js-end-frame' });

			if (Capability.isIOS()) {
				this.showPlayerAbove();
				box.event.addListener(null, PlayerEvents.buffering, this.onMovePlayerBack);
				box.event.addListener(null, PlayerEvents.playing, this.onMovePlayerBack);
			}
		},

		createPlayer: function () {
			this.player = new Player({ el: '.js-player', model: new PlayerModel() });
		},

		render: function () {
			$(this.el).html(Template);
		},

		showPlayerAbove: function (e) {
			this.player.$el.css({ position: 'absolute', zIndex: 2 });
			//$('.menu-bk').css({ display: 'none' });
		},

		onMovePlayerBack: function (e) {
			box.event.removeListener(null, PlayerEvents.buffering, this.onMovePlayerBack);
			box.event.removeListener(null, PlayerEvents.playing, this.onMovePlayerBack);
			this.player.$el.css({ position: '', zIndex: '' });
			//$('.menu-bk').css({ display: '' });
		}
	});
	return Ui;
});