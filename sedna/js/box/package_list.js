box.depLoaded({
	"packages": {
		"ajax-loader": "",
		"blank": "",
		"jquery.smoothZoom": "",
		".sedna": {
			"app": "",
			"config": "",
			"event": {
				"app_events": "",
				"block_events": "",
				"end_frame_events": "",
				"grid_events": "",
				"menu_events": "",
				"navigation_events": "",
				"player_events": "",
				"timing_events": ""
			},
			"main": "",
			"model": {
				"capability": "",
				"grid": {
					"grid_model": ""
				},
				"menu": {
					"menu_model": ""
				},
				"player_model": "",
				"ui_model": ""
			},
			"view": {
				"end_frame": "",
				"grid": {
					"click_block": "",
					"copy_block": "",
					"element_block": "",
					"grid": "",
					"grid_block": "",
					"link_block": "",
					"multiline_block": "",
					"number_block": "",
					"solar_block": "",
					"title_block": ""
				},
				"menu": {
					"menu": "",
					"menu_item": ""
				},
				"player": "",
				"ui": ""
			}
		},
		"site": "",
		"!vendor": {
			"backbone": "",
			"jquery-nc": "",
			"jquery": "",
			"lodash": "",
			"modernizr": "",
			"plugin": {
				"jquery": {
					"jquery.color": "",
					"jquery.easing": ""
				},
				"require": {
					"text": ""
				}
			},
			"require": ""
		}
	},
	"lib_folder": "!vendor",
	"naming_convention": "file_system"
});