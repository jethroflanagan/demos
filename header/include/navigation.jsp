<%@ page import="java.net.HttpURLConnection" %>
<%@ page import="com.cambrient.pangaea.web.WebConnection" %>
<%@ page import="net.sf.json.JSONObject" %>
<%@ page import="com.cambrient.pangaea.utils.JSONUtil" %>
<%
   int jobCount = 0;
   String baseUrl = ApplicationManager.getProperty(app.getName(), "application.hostname");
   HttpURLConnection conn = null;
   try {
      String results = WebConnection.getURLOutput(baseUrl + contextPath + "/rest/native/en/timeline/getJobCount");
      JSONObject jsonObject = JSONUtil.fromObject(results);
      jobCount = jsonObject.getInt("count");
   } catch (Exception e) {
      // this implies we couldn't make a connection to the master at all. send email, throw exception.
      e.printStackTrace();
   }

   String type = request.getParameter("type");
   String keyword = request.getParameter("keyword");

   // get page-name to determine which thing to highlight too (if we are on blog-detail
   String requestURI = request.getRequestURI();
   if (requestURI.indexOf("blog") >= 0) {
      type = "blog";
   } else if (requestURI.indexOf("work") >= 0) {
      type = "work";
   } else if (requestURI.indexOf("job") >= 0) {
      type = "job";
   }
%>

<div class="l-wrap">
	<nav id="nav-main-area" class="clearfix nav-main l-wrap-main">
		<ul class="clearfix menu-social">
			<li><a class="main-sprite main-logo" href="/" onClick="_gaq.push(['_trackEvent', 'Header', 'Header-logo', '']);">Native</a></li>
			<li>
				<ul>
					<li><a class="main-sprite main-fb" href="http://www.facebook.com/nativesa" target="_blank" onClick="_gaq.push(['_trackEvent', 'Header', 'Header-facebook', '']);" title="Like us on Facebook">Facebook</a></li>
					<li><a class="main-sprite main-twttr" href="http://twitter.com/native" target="_blank" onClick="_gaq.push(['_trackEvent', 'Header', 'Header-twitter', '']);" title="Follow us on Twitter">Twitter</a></li>
					<li><a class="main-sprite main-youtube" href="http://www.youtube.com/nativewatch" target="_blank" onClick="_gaq.push(['_trackEvent', 'Header', 'Header-youtube', '']);" title="Subscribe to our YouTube Channel">Youtube</a></li>
					<li><a class="main-sprite main-gplus" href="https://plus.google.com/u/0/114600710942171886070/" target="_blank" onClick="_gaq.push(['_trackEvent', 'Header', 'Header-googleplus', '']);" title="Follow us on Google+">Google+</a></li>
					<li><a class="btn btn-contact" href="/contact-us" title="Get in touch with us">Contact</a></li>
				</ul>
			</li>
		</ul>
		<ul class="clearfix menu-main">
			<li class="<%= ((type == null || type.length() < 1) && (keyword == null || keyword.length() < 1)) ? "is-active" : ""%> menu-main-all"><a href="/#nav-main-area" data-type="All" onClick="_gaq.push(['_trackEvent','Header','Header-Navigation','Header-Navigation-All']);">All</a></li>
			<li class="<%= (type != null && "Work".equalsIgnoreCase(type)) ? "is-active" : ""%> menu-main-work">
				<span class="arrow main-sprite main-nav-arrow-1"></span>
				<a href="/home?type=work#nav-main-area" data-type="Work" onClick="_gaq.push(['_trackEvent','Header','Header-Navigation','Header-Navigation-Work']);">Work</a>
			</li>
			<li class="<%= (type != null && "Blog".equalsIgnoreCase(type)) ? "is-active" : ""%> menu-main-blog">
				<span class="arrow main-sprite main-nav-arrow-2"></span>
				<a href="/home?type=blog#nav-main-area" data-type="Blog" onClick="_gaq.push(['_trackEvent','Header','Header-Navigation','Header-Navigation-Blog']);">Blog</a>
			</li>
			<li class="<%= (type != null && "People".equalsIgnoreCase(type)) ? "is-active" : ""%> menu-main-people">
				<span class="arrow main-sprite main-nav-arrow-3"></span>
				<a href="/home?type=people#nav-main-area" data-type="People" onClick="_gaq.push(['_trackEvent','Header','Header-Navigation','Header-Navigation-People']);">People</a>
			</li>
			<li class="<%= (type != null && "Job".equalsIgnoreCase(type)) ? "is-active" : ""%> menu-main-jobs">
            <% if (jobCount > 0) { %><span class="main-sprite main-jb-count"><%= jobCount %></span><% } %> <%-- job counter won't display if there are no jobs in the db --%>
            <span class="arrow main-sprite main-nav-arrow-4"></span>
				<a href="/home?type=job#nav-main-area" data-type="Job" onClick="_gaq.push(['_trackEvent','Header','Header-Navigation','Header-Navigation-Jobs']);">Jobs</a>
			</li>
			<li class="menu-main-services">
				<a href="/services#nav-main-area" onClick="_gaq.push(['_trackEvent','Header','Header-Navigation','Header-Navigation-Services']);">Services</a>
			</li>
			<li class="menu-main-about">
				<a href="/about#nav-main-area" onClick="_gaq.push(['_trackEvent','Header','Header-Navigation','Header-Navigation-About']);">About</a>
			</li>
			<li class="menu-main-search" onClick="_gaq.push(['_trackEvent','Header','Header-Navigation','Header-Navigation-Search']);">
				<form method="get" action="/">
					<input class="input-search" type="search" id="search-search" placeholder="SEARCH" name="keyword" />
					<input class="input-submit main-sprite main-search" type="submit" id="search-submit" value="Search" />
				</form>
			</li>
		</ul>
	</nav> <!-- .nav-main -->
</div> <!-- .l-wrap -->