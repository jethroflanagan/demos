# Projects

[Repo for source](https://stringycustard@bitbucket.org/stringycustard/demos.git)

[Source files in a zip if there are issues](http://isatthroughintermission.com/demo/repo.zip)

## Audio engine

### Links

**source:** `/audio-test`

**demo:** [/demo/audio-test/](http://isatthroughintermission.com/demo/audio-test/)

**assets:** If you want to test this locally, you can grab the assets here: [audio.zip](http://isatthroughintermission.com/demo/audio-test/audio.zip) and extract the files into `/src/audio`.

**demo notes:** Please wait for it to load all sounds before clicking on `Test Journey`. The other button isn't implemented for this part yet.

### Overview

*Early work-in-progress.*

This is for an immersive audio experience so that people can experience what it sounds like to ride in a car that will be launched next year. People will be able to choose a journey, which will simulate travelling through a particular scenario using environmental sounds layered over the sound of the car travelling.

It will use recorded and produced audio from the car in the final implementation, all current audio is for testing purposes.

My responsibility is to implement an audio engine with multiple layers. The goal is to have something that can play endlessly (like [noisli](http://www.noisli.com/)), while limiting file size. 

Audio layers will loop or play random repeatable sections of themselves for amounts of time, and other layers will start based on when files end or begin, not according to a specific global timeline (e.g. Sound A ending will cause Sound B to play, but Sound A can play for a random amount of time). 

Layers need to cross-fade into each other and also have filters so we can muffle them when the user "winds up the window".

The API implementation may seem odd, but it's to cater for multiple possible audio engine requirements per browser (thankfully SoundJS turned out to be much better than expected and I only need to modify the Flash engine for it to work for IE8+; mobile support is already pretty good).

### Technical Description

I also implemented the gulp deployment process for this (inside `/gulp`). I haven't yet finalised the production workflow, but development should run by typing `gulp` or `gulp dev` from the command line (in the project root). It uses [bower](http://bower.io/) for dependencies.


## Native website header

### Links

**source:** `/header`

**demos:** 

[Normal version](http://isatthroughintermission.com/demo/header/)

[Slow version](http://isatthroughintermission.com/demo/header-slow/)

[Artwork tool](http://isatthroughintermission.com/demo/header-art/)

### Overview

I only worked on the text and triangle animation in the heading area. The code I wrote is in `/app/modules/header`. This was one of my first proper JavaScript projects, and as such it's not very tidy. The text animation is trivial, but the triangles were a lot of fun to work with.

Triangles are used to create shapes/artwork. Each shape is then destroyed in a random number of steps, then end result of which is used to build the next shape. It would have been extremely complex to build a shape from a destroyed/partial state, so I had to cheat. Building a shape is actually just a reversal of that shape being destroyed. I destroy the current shape a number of times, and then also destroy the next shape a few times. The triangles that end up on the same locations are retained, all others are removed, and I reverse the destruction process of the next shape, adding in new triangles as needed.

Since people aren't really going to choose graphics options, and Canvas at the time was quite slow, I added in a graphics tester that would try upgrade the user's graphics options and test how well they performed while running, degrading as necessary.

I wrote a tool for drawing the different artworks. Source not included since it's mostly a cut back version of the normal header with a bunch of hacks.

Use `+` and `-` to change colours in the art tool. Left click to draw. It may require using Chrome.

## Sedna Gold

### Links

**source:** `/sedna`

**demo:** [site](http://www.omegawatches.com/planet-omega/watchmaking/sedna-gold)

### Overview

I only wrote the header area animation, not the rest of the page (so please don't blame that other stuff on me). This is a promotional for a watch. I had to sequence animated blocks on top of video. The JS API of YouTube at the time was quite poor, so there may be some odd hacks in there to cater for it.

## AMD in a box

### Links

**source:** [github](https://github.com/stringycustard/amd-in-a-box)

### Overview

Built for NodeJS, this was the beginnings of something before Grunt and Gulp and all the rest came along. I was getting frustrated with limitations and issues of RequireJS, and this was written to get around those problems. It was going to be AMD-agnostic, but I eventually started using other tools and so never kept up with this project. There are later versions of the one shown here, but I just used them directly in projects, and modified it slightly for project needs at the time. I don't think there's a need to show the newer stuff since it's still quite old anyway.