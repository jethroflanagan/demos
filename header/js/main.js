require([
	// Global
	'app',

	// Libs
	'lodash',
	'jquery',
	'plugins',
	'../app/modules/list',
	'../app/modules/header/header'
],

function(app, _, $, plugins, List, Header){

	_.bindAll(this, 'updatePosition', 'toggleNav');

	$(document).ready(function(){

		// Load font
		try{Typekit.load();}catch(e){};

		var listView = new List.Views.ListView();
		Header.init();

		app.toPage = function(href, callback){
			listView.hideItems( listView.collection.models ); // hide all listView items

			var self = this;
			window.sessionStorage.setItem('nos-visited', true); // check if user has visited page before
			this.history.push(this.toPage);

			$.get(href, function(data){
				// The HTML won't allow 2 body or head elements
				// By default that means that these elements will be destroyed
				// This means you'll only be able to .find() elements WITHIN the body
				// I need the classes on the body so to stop it from being destroyed I'm
				// forced to reformat the data
				function cleanPage(data){
					var openBody = '<body',
						closeBody = '</body>',
						closeFooter = '</footer>', // Scripts appear after footer and children scripts are
						// functionaly siblings within $()
						clean = data.substring(data.indexOf(openBody));
					clean = clean.substring(0, clean.lastIndexOf(closeFooter) + closeFooter.length); // remove after /footer
					clean = clean.replace(openBody, '<div id="body"'); // turn body into a div
					clean = clean + '</div>'; // we've lost the closing body, 'recreate' it

					return clean;
				}

				var $newBody = $( cleanPage(data) ).filter('#body'),
					$innerContentNew = $newBody.find('.inner-content'),
					bodyClasses = $newBody.attr('class');
				// End weirdness

				if( $newBody.hasClass('template-detail') || $('body').hasClass('template-detail') ){
					$('html, body').animate({
						scrollTop: 0//$('.content-wrap').position().top
					}, 300);
				};

				var $contentWrap = $('.content-wrap');
				$('title').text( $newBody.data('page-title') );
				$('.inner-content').fadeOut(function(){

					function pageTransition(){
						Header.resize(); // make sure Jethros banner is doing it's thing
						$('body').attr('class', bodyClasses);
						$('.content-wrap').html( $innerContentNew.css('opacity', 0) );
						// This #inner-content is the new element
						$('.inner-content').animate({
							opacity: 1
						}, function(){ // scroll so nav is at the top - ux
							var scrollToPos = $('#header').next('.divider-line').position().top;
							if(scrollToPos !== $(window).scrollTop){
								$('html, body').animate({
									scrollTop: scrollToPos
								});
							};
						});
					};

					// If the $newBody is a listing page
					if( $newBody.hasClass('template-listing') ){
						if( $('body').hasClass('template-listing') ){
							pageTransition();

							// nav stuff
							if($('body').scrollTop() < 100) {
								$('.menu-social').eq(1).hide();
								$('#content').css('paddingTop', '110px');
							}
						} else {
							$('.divider').eq(1).show();
							$('#header').slideDown(function(){
								pageTransition();
							});
						};
					// otherwise if it's a detail page
					} else {

						function detailTransition(){
							pageTransition();
							self.init();

							$('.menu-social').eq(1).slideDown();
							$('#content').css('paddingTop', '168px');

							// refresh social widgets after ajax load
							if(typeof(twttr) === "object"){
								twttr.widgets.load();
							};
							if(typeof(FB) === "object"){
								FB.XFBML.parse();
							};
							if(typeof(gapi) === "object"){
								gapi.plusone.go();
							};
						};

						// If the current page is a listing
						if( $('body').hasClass('template-listing') ){
							$('#header').hide()
								detailTransition();
							//});
						} else {
							$('.divider-line').eq(1).removeClass('is-hidden');
							detailTransition();
						};

					};

				});

				if(typeof( callback ) === 'function') callback();

			});

		};
		app.init = function(){

			// Random details page crap
			if($('body').hasClass('template-detail')){
				$('#slider').plusSlider({
					createPagination: false,
					sliderType: 'fader',
					onInit: function(base){
						//base.$slider.height();
						base.$arrows.addClass('clearfix').find('.prev').addClass('main-sprite main-slider-prev');
						base.$arrows.find('.next').addClass('main-sprite main-slider-next');
					},
					onSlide: function(base){
						//base.el.closest('section').children('h2').find('span').text('lol')
						var $h3 = base.$wrapContainer.closest('section').children('h3');
						$h3.find('.current').text( base.currentSlideIndex + 1 );
						$h3.find('.length').text( base.slideCount );
					}
				});

				$(document).ready(function(){
					//random shitty stuff that needs to be run
					$('.related_posts_area').appendTo('.related_posts_scheme');
					$('.related_work_area').appendTo('.related_posts_scheme');
				});

				// turn tabs into actual tabs
				$('.tabs').each(function(){
					var $tabs = $(this),
						$nav = $('<ul />', {
								'class': 'tabs-nav'
							});

					$tabs.find('h2').each(function(){
						var $this = $(this);

						$listItem = $('<li />', {
							text: $(this).text()
						});

						$listItem.appendTo( $nav )

						$this.remove();

					});

					$tabs.find('img').each(function(){
						var $this = $(this),
							$img = $this.clone();

						$('<li />', {
							'class': 'tabs-img',
							html: $img
						}).prependTo( $this.next('ul') );
						$this.remove();
					});

					$tabs.wrapInner(
						$('<div />', {
							'class': 'tabs-wrap'
						})
					);
					var $wrap = $tabs.find('.tabs-wrap');
					$wrap.children().hide().eq(0).show();

					$nav.prependTo($tabs);

					// Create bullet
					//<span class="main-sprite main-bullet-tick"></span>
					var $bullet = $('<span />', {
							'class': 'main-sprite main-bullet-tick'
						}).prependTo( $wrap.find('li') ),
						$arrow = $('<span />', {
							'class': 'main-sprite main-tabs-arrow'
						}).appendTo( $nav.find('li') );

					$nav.find('li').click(function(){
						var $this = $(this);

						$this.addClass('is-active').siblings().removeClass('is-active');
						$wrap.children().eq( $this.index() ).show().siblings().hide();

					}).eq(0).addClass('is-active');
				});

			}; // must be template-detail page
		}; // app.init();

		if (Modernizr.history){

			function navCurrentItem(href){
				if(href.indexOf('type=') !== -1){
					var urlArr = href.split('type='),
						urlType;
					if(typeof urlArr === 'object'){
						urlType = urlArr[1];
					};
				} else if(href.indexOf('-detail/') !== -1){
					var detailPos = href.indexOf('-detail'),
						urlArr = href.slice(0, detailPos);

					urlArr = urlArr.split('/')
					var urlType = urlArr[urlArr.length -1];
				}

				if(typeof urlType === 'string'){
					$('.menu-main').find('a').filter(function(){
						return $(this).attr('href').indexOf('' + urlType) != -1;
					}).parent().addClass('is-active').siblings().removeClass('is-active');
				};
			};

			app.ajaxClick = function(e){
				var $this = $(this),
					href = $this.attr('href'),
					hostname = window.location.hostname;

				// Remove the hash from the url
				if(href.indexOf('#nav-main-area') !== -1){
					href = href.slice(0, href.indexOf('#nav-main-area'));
				};

				// Skip .main-close elements
				if($this.hasClass('main-close') && $this.closest('.detail').length && window.sessionStorage.getItem('nos-visited') === 'true'){
					window.history.back(-1);
					navCurrentItem(window.location.href);
					return false;
				};

				if($this.get(0).href.indexOf(hostname) !== -1 && $this.attr('href') !== '#'){
					e.preventDefault();
					if(href !== '#'){
						history.pushState(null, null, href);

						/* Make sure correct item is targetted in the menu */
							navCurrentItem(href);
						/* End menu stuff */

						if(!typeof listView !== 'undefined') app.toPage(href); // toPage uses the listView object
					};
				} else {
					return;
				};
			}; // app.ajaxClick()

			$(document).on('click', 'a:not([data-bypass])', app.ajaxClick);

			// Handle the pushstate/ajax stuff on the search form
			$('.menu-main-search').find('form').submit(function(e){
				e.preventDefault();

				var searchString = $('#search-search').val();
				searchString = searchString.split(' ');
				searchString = searchString.join('+');

				var href = '/home?keyword=' + searchString;
				history.pushState(null, null, href);
				if(!typeof listView !== 'undefined'){ // toPage uses the listView object
					app.toPage(href, function(){
						$('.load-more').remove();
					});
				}
				$('#search-search').val('');
			});

			$('.content-wrap').on('submit', '.search-form', function(e){
				var $search = $('#search-search');
				$search.val( $(this).find('input[type="search"]').val() );
				$search.closest('form').trigger('submit');
				return false;
			});

			// handle pushstate/ajax stuff when you navigate with the back/forward button
			// $(window).bind('popstate', function(){
			// 	if ( window.sessionStorage.getItem('nos-visited') == true ){
			// 		var href = window.location.href;

			// 		if(app.history.length > 0){
			// 			var lastFunction = app.history.pop();
			// 			lastFunction();
			// 		}
			// 	};
			// });
			window.addEventListener('popstate', function(e) {
				if ( window.sessionStorage.getItem('nos-visited') == 'true' ){
					var href = window.location.href;
					if(!typeof listView !== 'undefined') app.toPage(href); // toPage uses the listView object
				};
			});
			// navigation stuff
			$('.menu-main').find('a').click(function(e){
				e.preventDefault();
				$(this).parent().addClass('is-active').siblings().removeClass('is-active');
			});

			$('.main-logo').click(function(e){
				e.preventDefault();
				$(this).parent().addClass('is-active').siblings().removeClass('is-active');
			});

		}; // modernizr.history


		// Modal Overlay
		$(document).on('click', '.instgrm', function(){
			var $this 		= $(this),
				src 		= $this.data('image'),
				title 		= $this.data('title'),
				time 		= $this.data('time'),
				location 	= $this.data('location'),
				$figcaption = $('#overlay').find('figcaption');

				// Preload Image
				// var img = new Image();
				// img.src = src;

				/* Creating this markup */
				// <p class="title"></p>
				// <ul class="meta-general">
				// 	<li class="meta-time">
				// 		<span class="main-sprite main-time"></span>
				// 		<span class="meta-time-text"></span>
				// 	</li>
				// 	<li class="meta-location">
				// 		<span class="main-sprite main-location"></span>
				// 		<span class="meta-location-text"></span>
				// 	</li>
				// </ul>

				$figcaption.children().not('.main-close').remove(); // Clear markup
				$('#overlay').find('img').attr('src', ''); // Clear markup

				var $metaGeneral = $('<ul />', { // Recreating the above markup
						'class': 'meta-general'
					}).prependTo( $figcaption ),
						$timeMeta = $('<li />', {
							'class': 'meta-time'
						}).appendTo( $metaGeneral ),
							$timeIcon = $('<span />', {
								'class': 'main-sprite main-time'
							}).appendTo( $timeMeta ),
							$timeText = $('<span />', {
								'class': 'meta-time-text',
								text: time
							}).appendTo( $timeMeta ),
						$locationMeta = $('<li />', {
							'class': 'meta-location'
						}).appendTo( $metaGeneral ),
							$locationIcon = $('<span />', {
								'class': 'main-sprite main-location'
							}).appendTo( $locationMeta ),
							$locationText = $('<span />', {
								'class': 'meta-location-text',
								text: location
							}).appendTo( $locationMeta ),
					$title = $('<p />', {
						text: title
					}).prependTo( $figcaption );

				$('#overlay').find('img').css('opacity', 0).load(function(){
					$(this).css('opacity', 1);
				}).attr('src', src);

			$('#overlay').fadeIn();
		}).on('click', '#overlay', function(){
			$('#overlay').fadeOut();
		});

		$(document).on('click', '.personnel li', function(){
			var $overlayContent = $(this).find('.popup').clone(),
				$img = $(this).find('img'),
				srcArr = $img.attr('src').split('.');

			$('#overlay').find('img').attr('src', $overlayContent.data('image'));
			$('#overlay').find('figcaption').children().not('.main-close').remove();
			$('#overlay').find('figcaption').prepend( $overlayContent ).end().fadeIn();
		});

		$('.content-wrap').on('click', '.listing > *', function(){
			var type = $(this).find('.meta-type').text(),
				section = $('.menu-main').find('is'),
				title = $(this).find('h1').text(),
				cat = $('.menu-main').find('.is-active').find('a').text();

				if(title !== ''){
					var gaString = section + '-' + title;
				} else {
					var gaString = section + '-No Title';
				}

			_gaq.push(['_trackEvent','ContentCards','ContentCards-'+type,'ContentCards-'+cat+'-'+gaString]);
		}); // sigh Google Analytics

		if(!Modernizr.input.placeholder){
			$('input[placeholder]').each(function(){
				var $el = $(this),
					val = $el.attr('placeholder');
				$el.val( val ).focus(function() {
					if ( $el.val() == val ){
							$el.val('');
					};
				}).blur(function(){
					if ( $el.val() == '' ) {
						$el.val( val );
					};
				});
			});
		}; // modernizr.placeholder

		$('#header').find('.main-logo-alt').click(function(){
			$('.menu-main').find('.menu-main-all').addClass('is-active').siblings().removeClass('is-active');
		});

		function updatePosition($el, offset, pos, paddingTop, slide, wrapLeft){ // this
			var scrollPos = $(app.scrollTopEl).scrollTop() - offset, // webkit uses body as the scrollTop element, firefox/IE uses HTML. Opera uses both but the method will allow it to work correctly
				paddingTop = paddingTop ? paddingTop : 0;
			// if the window has scrolled past the menu's lowest position
			if($(window).width() < 945) {
				$el.removeAttr('style');
			} else if(scrollPos > 0){
				if(slide === true){
					$el.css('top', scrollPos + pos + paddingTop + 'px'); // older browsers
				} else { // for position fixed, no animation
					$el.css({
						position: 'fixed',
						left: wrapLeft === true ? $('.listing').offset().left + 10 : 'auto'
					});
					$('.divider-line').eq(1).addClass('is-fixed');
				}
			} else {
				$('.divider-line').eq(1).removeClass('is-fixed');
				$el.removeAttr('style');
			};
		}; // updatePosition()

		function toggleNav(){
			if($('body').hasClass('template-listing')){ // only on listing pages!
				if($(window).width() < 945) {
					$('.menu-social').show();
					$('#content').removeAttr('style');
					$('.divider-line').eq(1).removeClass('is-hidden');
				} else {
					if($(app.scrollTopEl).scrollTop() >= 100) { // if scrolled down past 100px
						$('.menu-social').eq(1).slideDown(); // open up the .menu-social bar
						$('.divider-line').eq(1).removeClass('is-hidden');
						if(Modernizr.csstransitions){
							$('#content').css('paddingTop', '168px');
						} else {
							$('#content').animate({
								paddingTop: '168px'
							}, 300);
						}
					} else{
						$('.menu-social').eq(1).slideUp();
						$('.divider-line').eq(1).addClass('is-hidden');
						if(Modernizr.csstransitions){
							$('#content').css('paddingTop', '110px');
						} else {
							$('#content').animate({
								paddingTop: '110px'
							}, 300);
						}
					};
				};
			} else {
				$('.divider-line').eq(1).removeClass('is-hidden');
				$('#content').removeAttr('style');
			}
		}; // toggleNav()

		// $(window).scroll(function(){
		// 	if($('body').hasClass('template-listing')){ // can only take affect on the listing page
		// 		_.throttle(updatePosition($('.nav-main').eq(1), 553, 0, -20), 150);
		// 		_.throttle(updatePosition($('.timeline'), 753, 200, 200, true), 50);
		// 		//toggleNav()
		// 	};
		// 	_.throttle(toggleNav(), 300);
		// });
		/*
		var movingItems = window.setInterval(function(){
			if($('body').hasClass('template-listing')){ // can only take affect on the listing page
				_.throttle(updatePosition($('.nav-main').eq(1), 553, 0, -20, false, true), 150);
				_.throttle(updatePosition($('.timeline'), 553, 200, 0, true), 50);
				//toggleNav()
			};
			_.throttle(toggleNav(), 300);
		}, 50);*/

		// Nav animation and scroll position
		if($('body').hasClass('template-listing') && $('body').scrollTop() < 100) {
			$('.menu-social').eq(1).hide();
			$('#content').css('paddingTop', '110px');
		};

		app.init();
	});
});
