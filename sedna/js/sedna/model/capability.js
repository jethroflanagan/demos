define([
	box.lib('backbone'),
	box.lib('lodash'),
	box.lib('jquery-nc'),

	//project
	box.pkg('event/app_events')
],
function (Backbone, _, $, AppEvents) {
	// Singleton
	var CapabilityModel = Backbone.Model.extend({

		initialize: function () {
			_.bindAll(this);
		},

		hasCssTransitions: function () {
			return $('html').hasClass('csstransitions');
		},

		isLtIE9: function () {
			return $('html').hasClass('lt-ie9');
		},

		// user agent sniffing for this is unfortunately the best solution
		isIOS: function () {
			// force true or false for null vs array
			return navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false;
		}
	});
	return new CapabilityModel();
});