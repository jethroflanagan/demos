package za.co.nativecode.tmon.youtubeplayer.model.util 
{
	import flash.display.DisplayObject;
	import flash.geom.Matrix3D;
	import flash.geom.PerspectiveProjection;
	import flash.geom.Point;
	/**
	 * @author Jethro Flanagan
	 */
	public class Math3DUtils 
	{
		
		static public function updateProjectionPoint(root:DisplayObject, point:Point = null, fov:Number = NaN, focalLength:Number = NaN):void
		{
			var perspective:PerspectiveProjection = new PerspectiveProjection();
			if (point != null)
				perspective.projectionCenter = new Point(point.x, point.y);
			if (!isNaN(fov))
				perspective.fieldOfView = fov;
			if (!isNaN(focalLength))
				perspective.focalLength = focalLength;
			root.transform.perspectiveProjection = perspective;
		}
		
		/**
		 * check whether an object is facing the "camera". With love from senocular, since I was too lazy to refresh my vector multiplication
		 * @param	displayObject
		 * @return
		 */
		static public function isFrontFacing(displayObject:DisplayObject):Boolean 
		{
			// define 3 arbitary points in the display object for a
			// global path to test winding
			var p1:Point = displayObject.localToGlobal(new Point(0,0));
			var p2:Point = displayObject.localToGlobal(new Point(100,0));
			var p3:Point = displayObject.localToGlobal(new Point(0, 100));
			// use the cross-product for winding which will determine if
			// the front face is facing the viewer
			return ((p2.x-p1.x) * (p3.y-p1.y) - (p2.y-p1.y) * (p3.x-p1.x) > 0);
		}
				
		static public function sortByZ(a:DisplayObject, b:DisplayObject):int
		{
			var z:Number = getZRelativeTo(a, b);
			return (z == 0 ? 0 : (z < 0 ? 1 : -1));
		}
		
		static public function getZRelativeTo(object:DisplayObject, relativeTo:DisplayObject):Number
		{
			if (object != null && object.transform != null)
				return getTransformRelativeTo(object, relativeTo).position.z;
			else
				return NaN;
		}
		
		static public function getTransformRelativeTo(object:DisplayObject, relativeTo:DisplayObject):Matrix3D
		{
			if (object.transform != null)
				return Matrix3D(object.transform.getRelativeMatrix3D(relativeTo));
			else
				return null;
		}

	}

}