define(
    [
        'header/core',
        'header/triangles/triangles',
        'header/titles/titles'
    ],
    function (core, triangles, titles) {


	    var canvasSize;
	    var sizes =	//"space" magic number = too close for comfort
		    [
		        {visible: false/*, space: 0, size: 250, marginRight: "280px"*/},
			    //{space: 400, size: 300, marginRight: "-200px"},
			    {space: 400, size: 360, marginRight: "-220px", bottom: "155px"},
			    {space: 700, size: 497, marginRight: "-300px", bottom: "4px"},
			    {space: 900, size: 497, marginRight: "-200px", bottom: "4px"}
		    ];

	    var logo;
	    var fb;
	    var canvas;
	    var render;
	    var isAtTopOfScreen = false; //user focus is actually on the thing
	    var isFocused = true; //the tab is active
	    var isHidden = false; //just the triangles (hidden in smaller screen sizes).
	    var delayedResizeTimer;

	    function init()
        {
	        logo = $(".main-logo-alt", "#header");
	        fb = $(".main-fb-alt", "#header");
	        canvas = $("#triangles", "#header");
	        render = $("#render", "#triangles");

	        triangles.init("triangles", isTrianglesHidden);
	        titles.init("#header", ".slider h1", "p:last", ".banner-mark", triangles, isTrianglesHidden);
   			if (core.IS_DEBUG)
				$("#header").append('<div id="fps" style="position:absolute; left:0px, top:0px, z-index: 1000; background: #fff; color: #000; margin-top: -40px"></div>');

	        $(window).resize(monitorSize);
	        $(window).scroll(monitorFocus);
	        if (window.addEventListener != null)
	        	window.addEventListener('pushstate', monitorFocus);

	        if (/*@cc_on!@*/false)  // check for Internet Explorer
	        {
				document.onfocusin = play;
				document.onfocusout = pause;
			}
			else
			{
				window.onfocus = play;
				window.onblur = pause;
			}

	        monitorFocus();
	        monitorSize();
        }

        function isTrianglesHidden()
        {
        	return isHidden;
        }

	    function monitorFocus(e)
	    {
		    //core.log("paused? " + triangles.isPaused(), e);
		    if (canvas.is(':visible')) //set up to play
		    {
			    var pos = Math.max($("html").scrollTop(), $("body").scrollTop());
			    if (pos < canvas.offset().top + canvas.height())
			    {
			    	if (isFocused)
				    	triangles.play();
				    isAtTopOfScreen = true;
			    }
			    resize();
		    }
		    else if (!triangles.isPaused()) //prevents pausing multiple times
		    {
			    isAtTopOfScreen = false;
		        triangles.pause();
		    }
		    //if (pos < canvas.offset().top + canvas.
	    }

	    function resize()
	    {
	    	titles.resize();
	    }

	    function monitorSize(e, noDelayedCheck)
	    {
		    clearTimeout(delayedResizeTimer);
		    var space = fb.offset().left - logo.offset().left;
		    var sizeDef = sizes[0];
		    for (var i = 0; i < sizes.length; i++)
		    {
			    if (space > sizes[i].space)
			    {
					sizeDef = sizes[i];
			    }
		    }
		    //blocks only run once per change
		    if (canvasSize != sizeDef)
		    {
			    if (sizeDef.visible == undefined)
			    	sizeDef.visible = true;

		    	if (!sizeDef.visible)
		    	{
			    	isHidden = true;
			    	canvas.css({display: 'none'});
			    	triangles.pause();
			    }
			    else
		    	{
			    	isHidden = false;
			    	canvas.css({display: 'block'});
			    	if (isFocused)
			    		triangles.play();
			    	canvas.css({width: sizeDef.size, height: sizeDef.size, marginRight: sizeDef.marginRight, bottom: sizeDef.bottom});
			    	render.css({width: sizeDef.size, height: sizeDef.size});
			    }


			    canvasSize = sizeDef;
		    }
		    titles.resize();

		    //fix issue where screen animates into place after resize, thus reporting incorrect positions
		    //since elements aren't in final locations
		    if (noDelayedCheck == undefined)
		    {
			    delayedResizeTimer = setTimeout(function(){monitorSize(e, true);}, 1000);
			}
	    }

	    function play()
	    {
	    	isFocused = true;
	    	if (isAtTopOfScreen)
	    	{
	    		titles.play();
	    		triangles.play();
	    	}
	    }

	    function pause()
	    {
	    	isFocused = false;
	    	triangles.pause();
	    	titles.pause();
	    }

        return {init: init, resize: resize};
    });