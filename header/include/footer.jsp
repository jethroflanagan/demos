<footer id="footer">
	<div class="l-wrap">
		<nav>
			<ul>
				<li>&copy; 2012 NATIVE</li>
				<li><a href="about" onClick="_gaq.push(['_trackEvent', 'Footer', 'Footer-About-us', '']);">About us</a></li>
				<li><a href="services" onClick="_gaq.push(['_trackEvent', 'Footer', 'Footer-Services', '']);">Services</a></li>
				<li><a href="/contact-us" onClick="_gaq.push(['_trackEvent', 'Footer', 'Footer-Contact-us', '']);">Contact us</a></li>
				<li><a href="https://twitter.com/Native" class="twitter-follow-button" data-show-count="false">Follow @Native</a></li>
				<li><div class="fb-like-box" data-href="http://www.facebook.com/nativesa" data-width="190" data-show-faces="false" data-stream="false" data-header="true"></div></li>
			</ul>
		</nav>
	</div> <!-- .l-wrap -->
</footer> <!-- #footer -->


<div id="overlay">
 <div class="polaroid">
    <figure class="clearfix">
       <img src="#" alt="" />
       <figcaption>

          <div class="main-sprite main-close"></div>
       </figcaption>
    </figure>
 </div><!-- .polaroid -->
</div>

<!-- FB -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&appId=354212117941163";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- Twitter -->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
<!-- gplus -->
<script type="text/javascript">
	(function() {
		var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
		po.src = 'https://apis.google.com/js/plusone.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
	})();
</script>
<!-- GA -->
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-18404734-3']);
_gaq.push(['_trackPageview']);

(function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>