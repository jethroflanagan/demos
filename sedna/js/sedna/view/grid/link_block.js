define([
	box.lib('backbone'),
	box.lib('lodash'),
	box.lib('jquery-nc'),

	//project
	box.pkg('view/grid/copy_block'),
	box.pkg('event/navigation_events')
],
function (Backbone, _, $, CopyBlock, NavigationEvents) {
	var _super = CopyBlock.prototype;

	var LinkBlock = CopyBlock.extend({
		link: null,
		$link: null,

		initialize: function (params) {
			_.bindAll(this);
			_super.initialize.apply(this, [params]); 
		},
		
		prepContent: function () {
			_super.prepContent.apply(this);
			this.$link = this.$el.find('.js-link');
			this.link = this.$link.data('link');
		},

		showContent: function () {
			_super.showContent.apply(this); 
			this.$link.click(this.gotoLink);
		},

		gotoLink: function (e) {
			box.event.dispatch(null, NavigationEvents.open, { link: this.link });
		},

		dispose: function () {
			_super.dispose.apply(this); 
			this.$link.unbind('click');
		}

	});

	return LinkBlock;
});