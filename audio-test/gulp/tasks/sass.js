var gulp = require('gulp');
var paths = require('../paths');
var sass = require('gulp-sass');
var uncss = require('gulp-uncss');

gulp.task('sass', function () {
	return gulp.src(paths.SRC_SASS)
		.pipe(sass())
		.pipe(gulp.dest(paths.DEST_CSS));
});

gulp.task('sass:prod', function () {
	return gulp.src(paths.SRC_SASS)
		.pipe(sass())
		.pipe(uncss())
		.pipe(gulp.dest(paths.DEST_CSS));
});