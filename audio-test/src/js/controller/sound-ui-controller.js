__DEFAULT_IMPORTS__

var ButtonEvents = require('event/button-events');
var StatusEvents = require('event/status-events');
var SoundEngineEvents = require('event/sound-engine-events');
var IntroJourney = require('model/journey/journey-intro');

// TODO change back to events for engine?

var SoundUiController = Backbone.Model.extend({
	soundEngine: null,
	journey: null,

	initialize: function (params) {
		Binder.allExcept(this);
		if (params) {
			if (!params.soundEngine) {
				console.error('Needs a sound engine');
			}
			if (!params.view) {
				console.error('Needs a view');
			}
		}
		else {
			console.error('Missing parameters');
		}
		this.soundEngine = params.soundEngine;
		this.view = params.view;

		Event.listen(ButtonEvents.START_JOURNEY, this.onStartJourney);
		Event.listen(ButtonEvents.STOP_JOURNEY, this.onStopJourney);
		Event.listen(ButtonEvents.TURN_ON, this.onPlaySound);
		Event.listen(ButtonEvents.TURN_OFF, this.onStopSound);
		Event.listen(ButtonEvents.SET_VOLUME, this.onSetVolume);
		Event.listen(ButtonEvents.WINDOW_UP, this.onWindWindowUp);
		Event.listen(ButtonEvents.WINDOW_DOWN, this.onWindWindowDown);
		Event.listen(StatusEvents.SET, this.onStatusEvent);
	},

	onStartJourney: function (e) {
		this.journey = new IntroJourney();
	},

	onStopJourney: function (e) {
		console.log('stop journey');
	},

	onPlaySound: function (e) {
		//Event.dispatch(SoundEngineEvents.PLAY, e);
		this.soundEngine.play(e.data.soundId, e.data.instanceId);
	},

	onStopSound: function (e) {
		this.soundEngine.stop(e.data.soundId, e.data.instanceId);
	},

	onSetVolume: function (e) {
		this.soundEngine.setVolume(e.data.soundId, e.data.instanceId, e.data.volume, 1);
	},

	onWindWindowUp: function (e) {
		console.log('window up');
		var soundId = e.data.soundId;
		var instanceId = e.data.instanceId;
		var crossfadeId = this.soundEngine.setupSyncedCrossFade(soundId, instanceId);
		this.soundEngine.addFilter(soundId, crossfadeId, e.data.filterId);
		var crossfadeTime = 3;
		// // fade current sound out
		// this.soundEngine.setVolume(soundId, instanceId, 0, crossfadeTime);
		
		// // fade filtered sound in
		// this.soundEngine.setVolume(soundId, crossfadeId, 1, crossfadeTime);
		this.soundEngine.syncCrossfade(soundId, instanceId, crossfadeId, crossfadeTime, true);
	},

	onWindWindowDown: function (e) {
		var soundId = e.data.soundId;
		var instanceId = e.data.instanceId;

		this.soundEngine.removeFilter(soundId, instanceId, e.data.filterId);
	},

	onStatusEvent: function (e) {
		this.view.setStatus(e.data.message);
	},

});

return SoundUiController;