// usage: log('inside coolFunc', this, arguments);
// paulirish.com/2009/log-a-lightweight-wrapper-for-consolelog/
window.log = function f(){ log.history = log.history || []; log.history.push(arguments); if(this.console) { var args = arguments, newarr; try { args.callee = f.caller } catch(e) {}; newarr = [].slice.call(args); if (typeof console.log === 'object') log.apply.call(console.log, console, newarr); else console.log.apply(console, newarr);}};

// make it safe to use console.log always
(function(a){function b(){}for(var c="assert,count,debug,dir,dirxml,error,exception,group,groupCollapsed,groupEnd,info,log,markTimeline,profile,profileEnd,time,timeEnd,trace,warn".split(","),d;!!(d=c.pop());){a[d]=a[d]||b;}})
(function(){try{console.log();return window.console;}catch(a){return (window.console={});}}());


// place any jQuery/helper plugins in here, instead of separate, slower script files.

/*
 * jQuery Easing v1.3 - http://gsgd.co.uk/sandbox/jquery/easing/
 *
 * Uses the built in easing capabilities added In jQuery 1.1
 * to offer multiple easing options
 *
 * TERMS OF USE - jQuery Easing
 * 
 * Open source under the BSD License. 
 * 
 * Copyright © 2008 George McGinley Smith
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * Redistributions of source code must retain the above copyright notice, this list of 
 * conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list 
 * of conditions and the following disclaimer in the documentation and/or other materials 
 * provided with the distribution.
 * 
 * Neither the name of the author nor the names of contributors may be used to endorse 
 * or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE. 
 *
*/

// t: current time, b: begInnIng value, c: change In value, d: duration
if(typeof jQuery === 'object'){
	jQuery.easing['jswing'] = jQuery.easing['swing'];

	jQuery.extend( jQuery.easing,
	{
		def: 'easeOutQuad',
		swing: function (x, t, b, c, d) {
			//alert(jQuery.easing.default);
			return jQuery.easing[jQuery.easing.def](x, t, b, c, d);
		},
		easeInQuad: function (x, t, b, c, d) {
			return c*(t/=d)*t + b;
		},
		easeOutQuad: function (x, t, b, c, d) {
			return -c *(t/=d)*(t-2) + b;
		},
		easeInOutQuad: function (x, t, b, c, d) {
			if ((t/=d/2) < 1) return c/2*t*t + b;
			return -c/2 * ((--t)*(t-2) - 1) + b;
		},
		easeInCubic: function (x, t, b, c, d) {
			return c*(t/=d)*t*t + b;
		},
		easeOutCubic: function (x, t, b, c, d) {
			return c*((t=t/d-1)*t*t + 1) + b;
		},
		easeInOutCubic: function (x, t, b, c, d) {
			if ((t/=d/2) < 1) return c/2*t*t*t + b;
			return c/2*((t-=2)*t*t + 2) + b;
		},
		easeInQuart: function (x, t, b, c, d) {
			return c*(t/=d)*t*t*t + b;
		},
		easeOutQuart: function (x, t, b, c, d) {
			return -c * ((t=t/d-1)*t*t*t - 1) + b;
		},
		easeInOutQuart: function (x, t, b, c, d) {
			if ((t/=d/2) < 1) return c/2*t*t*t*t + b;
			return -c/2 * ((t-=2)*t*t*t - 2) + b;
		},
		easeInQuint: function (x, t, b, c, d) {
			return c*(t/=d)*t*t*t*t + b;
		},
		easeOutQuint: function (x, t, b, c, d) {
			return c*((t=t/d-1)*t*t*t*t + 1) + b;
		},
		easeInOutQuint: function (x, t, b, c, d) {
			if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
			return c/2*((t-=2)*t*t*t*t + 2) + b;
		},
		easeInSine: function (x, t, b, c, d) {
			return -c * Math.cos(t/d * (Math.PI/2)) + c + b;
		},
		easeOutSine: function (x, t, b, c, d) {
			return c * Math.sin(t/d * (Math.PI/2)) + b;
		},
		easeInOutSine: function (x, t, b, c, d) {
			return -c/2 * (Math.cos(Math.PI*t/d) - 1) + b;
		},
		easeInExpo: function (x, t, b, c, d) {
			return (t==0) ? b : c * Math.pow(2, 10 * (t/d - 1)) + b;
		},
		easeOutExpo: function (x, t, b, c, d) {
			return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
		},
		easeInOutExpo: function (x, t, b, c, d) {
			if (t==0) return b;
			if (t==d) return b+c;
			if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
			return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
		},
		easeInCirc: function (x, t, b, c, d) {
			return -c * (Math.sqrt(1 - (t/=d)*t) - 1) + b;
		},
		easeOutCirc: function (x, t, b, c, d) {
			return c * Math.sqrt(1 - (t=t/d-1)*t) + b;
		},
		easeInOutCirc: function (x, t, b, c, d) {
			if ((t/=d/2) < 1) return -c/2 * (Math.sqrt(1 - t*t) - 1) + b;
			return c/2 * (Math.sqrt(1 - (t-=2)*t) + 1) + b;
		},
		easeInElastic: function (x, t, b, c, d) {
			var s=1.70158;var p=0;var a=c;
			if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
			if (a < Math.abs(c)) { a=c; var s=p/4; }
			else var s = p/(2*Math.PI) * Math.asin (c/a);
			return -(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
		},
		easeOutElastic: function (x, t, b, c, d) {
			var s=1.70158;var p=0;var a=c;
			if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
			if (a < Math.abs(c)) { a=c; var s=p/4; }
			else var s = p/(2*Math.PI) * Math.asin (c/a);
			return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
		},
		easeInOutElastic: function (x, t, b, c, d) {
			var s=1.70158;var p=0;var a=c;
			if (t==0) return b;  if ((t/=d/2)==2) return b+c;  if (!p) p=d*(.3*1.5);
			if (a < Math.abs(c)) { a=c; var s=p/4; }
			else var s = p/(2*Math.PI) * Math.asin (c/a);
			if (t < 1) return -.5*(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
			return a*Math.pow(2,-10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )*.5 + c + b;
		},
		easeInBack: function (x, t, b, c, d, s) {
			if (s == undefined) s = 1.70158;
			return c*(t/=d)*t*((s+1)*t - s) + b;
		},
		easeOutBack: function (x, t, b, c, d, s) {
			if (s == undefined) s = 1.70158;
			return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
		},
		easeInOutBack: function (x, t, b, c, d, s) {
			if (s == undefined) s = 1.70158; 
			if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
			return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
		},
		easeInBounce: function (x, t, b, c, d) {
			return c - jQuery.easing.easeOutBounce (x, d-t, 0, c, d) + b;
		},
		easeOutBounce: function (x, t, b, c, d) {
			if ((t/=d) < (1/2.75)) {
				return c*(7.5625*t*t) + b;
			} else if (t < (2/2.75)) {
				return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
			} else if (t < (2.5/2.75)) {
				return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
			} else {
				return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
			}
		},
		easeInOutBounce: function (x, t, b, c, d) {
			if (t < d/2) return jQuery.easing.easeInBounce (x, t*2, 0, c, d) * .5 + b;
			return jQuery.easing.easeOutBounce (x, t*2-d, 0, c, d) * .5 + c*.5 + b;
		}
	});
}

/*
 *
 * TERMS OF USE - EASING EQUATIONS
 * 
 * Open source under the BSD License. 
 * 
 * Copyright © 2001 Robert Penner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * Redistributions of source code must retain the above copyright notice, this list of 
 * conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list 
 * of conditions and the following disclaimer in the documentation and/or other materials 
 * provided with the distribution.
 * 
 * Neither the name of the author nor the names of contributors may be used to endorse 
 * or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE. 
 *
 */

/*
 * jQuery Plus Slider 1.4.5
 * By Jamy Golden
 * http://css-plus.com
 * @jamygolden
 *
 * Copyright 2011
 * Free to use under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 */

(function(a){a.plusSlider=function(b,c){var d=this;d.$el=a(b);d.el=b;d.$el.data("plusSlider",d);d.init=function(){d.options=a.extend({},a.plusSlider.defaults,c);d.$el.addClass("plusslider-container").wrap('<div class="plusslider '+d.$el.attr("id")+'" />');d.$wrap=d.$el.parent();d.$slides=d.$el.children();d.$wrapContainer=d.$wrap.parent();d.slideCount=d.$slides.length;d.slideIndexCount=d.slideCount-1;d.sliderWidth=0;d.animating=false;d.wrapContainerWidth=d.$wrapContainer.width();d.wrapContainerHeight=d.$wrapContainer.height();d.currentSlideIndex=d.options.defaultSlide;d.$currentSlide=d.$slides.eq(d.currentSlideIndex);d.currentSlideWidth=d.$currentSlide.outerWidth();d.currentSlideHeight=d.$currentSlide.outerHeight();d.calculateSliderWidth=function(){for(var a=0;a<d.slideCount;a++){if(a==0)d.sliderWidth=0;d.sliderWidth+=d.$slides.eq(a).outerWidth()}};d.beginTimer=function(){d.timer=window.setInterval(function(){d.toSlide("next")},d.options.displayTime)};d.clearTimer=function(){if(d.timer){window.clearInterval(d.timer)}};d.setSliderDimensions=function(){d.calculateSliderWidth();d.currentSlideWidth=d.$currentSlide.outerWidth();d.currentSlideHeight=d.$currentSlide.outerHeight();if(d.options.fullWidth){d.sliderWidth=d.wrapContainerWidth*d.slideCount;d.wrapContainerWidth=d.$wrapContainer.width();d.$slides.width(d.wrapContainerWidth);d.calculateSliderWidth();d.$wrap.width(d.wrapContainerWidth).height(d.currentSlideHeight);d.$el.width(d.sliderWidth).css("left",d.$currentSlide.position().left*-1+"px")}else if(d.options.sliderType=="slider"){d.$wrap.width(d.currentSlideWidth);d.$wrap.height(d.currentSlideHeight)}};d.toSlide=function(a){if(d.animating==false){d.animating=true;if(a==="next"||a===""){d.currentSlideIndex+=1}else if(a==="prev"){d.currentSlideIndex-=1}else{d.currentSlideIndex=parseInt(a)}if(d.options.disableLoop=="first"||d.options.disableLoop=="both"&&d.currentSlideIndex<0||d.options.disableLoop=="last"||d.options.disableLoop=="both"&&d.currentSlideIndex>d.slideIndexCount){return}if(d.currentSlideIndex>d.slideIndexCount){d.currentSlideIndex=0}else if(d.currentSlideIndex<0){d.currentSlideIndex=d.slideIndexCount}d.$currentSlide=d.$slides.eq(d.currentSlideIndex);d.currentSlideWidth=d.$currentSlide.width();d.currentSlideHeight=d.$currentSlide.height();if(d.options.onSlide&&typeof d.options.onSlide=="function")d.options.onSlide(d);if(d.options.sliderType=="slider"){if(d.options.createPagination){d.$sliderControls.find("li").removeClass("current").eq(d.currentSlideIndex).addClass("current")}d.$el.animate({height:d.$currentSlide.outerHeight(),left:d.$currentSlide.position().left*-1+"px"},d.options.speed,d.options.sliderEasing,function(){d.animating=false;if(d.options.afterSlide&&typeof d.options.afterSlide=="function")d.options.afterSlide(d);if(d.options.onSlideEnd&&typeof d.options.onSlideEnd=="function"&&d.currentSlideIndex==d.slideIndexCount)d.options.onSlideEnd(d)});d.$wrap.animate({height:d.$currentSlide.outerHeight(),width:d.$currentSlide.outerWidth()},d.options.speed,d.options.sliderEasing);d.$slides.removeClass("current").eq(d.currentSlideIndex).addClass("current")}else{if(d.options.createPagination){d.$sliderControls.find("li").removeClass("current").eq(d.currentSlideIndex).addClass("current")}d.$slides.removeClass("current").eq(d.currentSlideIndex).addClass("current").fadeIn(d.options.speed,function(){d.$slides.not(".current").hide();d.animating=false;if(d.options.afterSlide&&typeof d.options.afterSlide=="function")d.options.afterSlide(d);if(d.options.onSlideEnd&&typeof d.options.onSlideEnd=="function"&&d.currentSlideIndex==d.slideIndexCount)d.options.onSlideEnd(d)})}}if(d.options.autoPlay){d.clearTimer();d.beginTimer()}};if(d.slideCount===1){d.options.autoPlay=false;d.options.createArrows=false;d.options.createPagination=false}if(d.options.sliderType=="fader")d.options.fullWidth=false;d.$slides.addClass("child").eq(d.currentSlideIndex).addClass("current");d.setSliderDimensions();if(d.options.width)d.$wrap.width(d.options.width);if(d.options.height)d.$wrap.height(d.options.height);d.currentSlideWidth=d.$currentSlide.outerWidth();d.currentSlideHeight=d.$currentSlide.outerHeight();if(d.options.sliderType=="slider"){d.calculateSliderWidth();d.$wrap.addClass("plustype-slider").find(d.$el).width(d.sliderWidth);if(d.options.fullWidth){d.setSliderDimensions();a(window).resize(function(){d.clearTimer();d.beginTimer();d.setSliderDimensions()})}d.$slides.show();d.$el.css("left",d.$currentSlide.position().left*-1+"px")}else{d.$wrap.addClass("plustype-fader");d.$slides.eq(0).show()}if(d.options.createPagination){d.$sliderControls=a("<ul />",{"class":"plusslider-pagination"});switch(d.options.paginationPosition){case"before":d.$sliderControls.insertBefore(d.$wrap);break;case"prepend":d.$sliderControls.prependTo(d.$wrap);break;case"after":d.$sliderControls.insertAfter(d.$wrap);break;default:d.$sliderControls.appendTo(d.$wrap);break}d.$sliderControls.wrap('<div class="plusslider-pagination-wrapper" />');for(var b=0;b<d.slideCount;b++){a("<li />",{"data-index":b,text:d.options.paginationTitle?d.$slides.eq(b).attr("data-title"):b+1}).appendTo(d.$sliderControls)}if(d.options.paginationWidth)d.$sliderControls.width(d.$sliderControls.find("li").outerWidth(true)*d.slideCount);d.$sliderControls.find("li").click(function(){var b=a(this).index();d.toSlide(b)}).eq(d.currentSlideIndex).addClass("current")}if(d.options.createArrows){d.$arrows=a("<ul />",{"class":"plusslider-arrows"});switch(d.options.arrowsPosition){case"before":d.$arrows.insertBefore(d.$wrap);break;case"append":d.$arrows.appendTo(d.$wrap);break;case"after":d.$arrows.insertAfter(d.$wrap);break;default:d.$arrows.prependTo(d.$wrap);break}d.$arrows.wrap('<div class="plusslider-arrows-wrapper" />');a("<li />",{"class":"next",text:d.options.nextText}).prependTo(d.$arrows);a("<li />",{"class":"prev",text:d.options.prevText}).prependTo(d.$arrows);d.$arrows.find(".next").click(function(){d.toSlide("next")});d.$arrows.find(".prev").click(function(){d.toSlide("prev")})}if(d.options.autoPlay){d.beginTimer();if(d.options.pauseOnHover){d.$el.hover(function(){d.clearTimer()},function(){d.beginTimer()})}}if(d.options.keyboardNavigation){d.$el.click(function(){a(".active-plusslider").removeClass("active-plusslider");a(this).addClass("active-plusslider")});a(window).keyup(function(a){if(d.$el.is(".active-plusslider")){if(a.keyCode==39){d.toSlide("next")}else if(a.keyCode==37){d.toSlide("prev")}}})}if(d.options.onInit&&typeof d.options.onInit=="function")d.options.onInit(d)};d.init()};a.plusSlider.defaults={sliderType:"slider",disableLoop:false,fullWidth:false,width:null,height:null,defaultSlide:0,displayTime:4e3,sliderEasing:"linear",speed:500,autoPlay:true,keyboardNavigation:true,pauseOnHover:true,createArrows:true,arrowsPosition:"prepend",nextText:"Next",prevText:"Previous",createPagination:true,paginationPosition:"append",paginationWidth:false,paginationTitle:false,onInit:null,onSlide:null,afterSlide:null,onSlideEnd:null};a.fn.plusSlider=function(b){return this.each(function(){new a.plusSlider(this,b)})}})(jQuery)