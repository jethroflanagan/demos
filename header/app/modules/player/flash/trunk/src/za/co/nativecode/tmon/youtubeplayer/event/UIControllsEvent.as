package za.co.nativecode.tmon.youtubeplayer.event 
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Bradley Botha
	 */
	public class UIControllsEvent extends Event 
	{
		static public const PLAY:String = "play";
		static public const PAUSE:String = "pause";
		static public const SCRUB_TIMELINE:String = "scrubTimeline";
		static public const SCRUB_VOLUME:String = "scrubVolume";
		
		private var _data:Object;		
		
		public function UIControllsEvent(type:String, data:Object = null, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			_data = data;			
		} 
		
		public override function clone():Event 
		{ 
			return new UIControllsEvent(type, data, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("UIControllsEvent", "type", "data", "bubbles", "cancelable", "eventPhase"); 
		}
		
		public function get data():Object 
		{
			return _data;
		}		
	}
	
}