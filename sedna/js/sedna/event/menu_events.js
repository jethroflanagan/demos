define([],
function () {
	var type = new box.event.EventType('menu event');
	var e = box.event.createEvent(type);
	return {
		activate: e('activate'),
		highlight: e('highlight'),
		enable: e('enable'),
		disable: e('disable')
	};
});