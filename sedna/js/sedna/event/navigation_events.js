define([],
function () {
	var type = new box.event.EventType('navigation event');
	var e = box.event.createEvent(type);
	return {
		open: e('open')
	};
});