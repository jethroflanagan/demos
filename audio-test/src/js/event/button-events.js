var Event = require('model/event');

return {
	START_JOURNEY: Event.create('start journey'),
	STOP_JOURNEY: Event.create('stop journey'),
	TURN_ON: Event.create('turn on'),
	TURN_OFF: Event.create('turn off'),
	SET_VOLUME: Event.create('set volume'),
	WINDOW_UP: Event.create('window up'),
	WINDOW_DOWN: Event.create('window down'),
}