package za.co.nativecode.tmon.youtubeplayer.model.util 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.PixelSnapping;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	/**
	 * @author Bradley Botha
	 */
	public class ImageManipulator 
	{
		
		public static function createBitmapFrom(mc:DisplayObject, dimensions:Rectangle = null, smoothing:Boolean = true, transparent:Boolean = true, fillColor:uint = 0x00000000, matrix:Matrix = null):Bitmap 
		{
			var raster:BitmapData = createBitmapDataFrom(mc, dimensions, smoothing, transparent, fillColor, matrix);
			//var bitmap:Bitmap = new Bitmap(bitmapData, "none", smoothing);
			return new Bitmap(raster, PixelSnapping.NEVER, smoothing);
		}
		
		public static function createBitmapDataFrom(mc:DisplayObject, dimensions:Rectangle = null, smoothing:Boolean = true, transparent:Boolean = true, fillColor:uint = 0x00000000, matrix:Matrix = null):BitmapData 
		{
			if(dimensions == null)
				dimensions = new Rectangle(0, 0, mc.width, mc.height);
				
			var raster:BitmapData = new BitmapData(dimensions.width, dimensions.height, transparent, fillColor);
			
			var transform:Matrix;
			if (matrix)
			{
				transform = matrix;
			}
			else
			{
				transform = new Matrix();
				transform.tx = -dimensions.x;
				transform.ty = -dimensions.y;
			}
			
			var masker:Rectangle = new Rectangle(0, 0, dimensions.width, dimensions.height);
			
			raster.draw(mc, transform, null, null, masker, smoothing);
			
			return raster;
		}
		
	}

}