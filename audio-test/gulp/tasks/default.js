var gulp = require('gulp');
var runSequence = require('run-sequence');

gulp.task('default', function () {
	// just run the dev task since it's most likely to be needed
	runSequence('dev');
});