package za.co.nativecode.tmon.youtubeplayer.model.util 
{
	/**
	 * @author Bradley Botha
	 */
	public class MathUtils 
	{
		static public const TAU:Number = Math.PI * 2;
		
		static public function getDistance(x1:Number, y1:Number, x2:Number, y2:Number):Number
		{
			return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
		}
		
		static public function getAngle(x1:Number, y1:Number, x2:Number = 0, y2:Number = 0):Number
		{
			return Math.atan2(x1 - x2, y1 - y2) ;
		}		
		
		static public function toDegrees(radians:Number):Number
		{
			return 180 / Math.PI * radians;
		}
		
		static public function toRadians(degrees:Number):Number
		{
			return Math.PI / 180 * degrees;
		}
		
		
		
	}

}