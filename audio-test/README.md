# Run
**In commandline:**

Ensure nodejs is installed.

Run: `npm install`. 
This will install all nodejs modules, and automatically run the `bower` install process.

Run: `gulp dev` to begin the dev environment. It will be served from localhost:9001


# Documentation style

Description goes here. Uses markdown.
Any code, variables, etc will be mentioned within `backticks`
Longer snippets of code will be within
~~~
three tildes
~~~

Parameters listed as @param, followed by variable type in braces, 
the variable name, and then description like this:

~~~
@param {type} variableName		Some description of the variable
~~~

Valid types are:
* string
* int
* object
* float
* boolean

If a variable is optional, it will be surrounded with square braces:

~~~
@param {type} [variableName]	Some description of the variable
~~~

If a variable is optional and has a default value, it will be surrounded 
with square braces and the default value shown in the braces after an equals sign:

~~~
@param {type} [variableName='default value']	Some description of the variable
~~~

If a variable is an array, the type if object will be shown with open and closing
square braces as a suffix of the type:

~~~
@param {type[]} variableName	Some description of the variable
~~~

If a variable can have multiple types, they will be separated with a bar (|):

~~~
@param {type1|type2} variableName	Some description of the variable
~~~