define([
	box.lib('backbone'),
	box.lib('lodash'),
	box.lib('jquery-nc'),

	//project
	box.pkg('view/grid/grid_block'),
	box.pkg('view/grid/title_block'),
	box.pkg('view/grid/copy_block'),
	box.pkg('view/grid/element_block'),
	box.pkg('view/grid/click_block'),
	box.pkg('view/grid/number_block'),
	box.pkg('view/grid/solar_block'),
	box.pkg('view/grid/link_block'),
	box.pkg('view/grid/multiline_block'),

	box.pkg('event/timing_events'),
	box.pkg('event/block_events'),
	box.pkg('event/player_events')
],
function (Backbone, _, $,
          GridBlock,
          TitleBlock,
          CopyBlock,
          ElementBlock,
          ClickBlock,
          NumberBlock,
          SolarBlock,
          LinkBlock,
          MultilineBlock,
          TimingEvents,
          BlockEvents,
          PlayerEvents) {

	var GridModel = Backbone.Model.extend({
		blocks: null,
		timeouts: null,

		initialize: function () {
			_.bindAll(this);
			this.blocks = [];
			this.timeouts = [];
			box.event.addListener(null, TimingEvents.tick, this.onTimeTick);
			box.event.addListener(null, PlayerEvents.done, this.onPlaybackEnd);
		},

		getBlockClass: function (type) {
			switch (type) {
				case "title": return TitleBlock;
				case "element": return ElementBlock;
				case "click": return ClickBlock;
				case "number": return NumberBlock;
				case "solar": return SolarBlock;
				case "link": return LinkBlock;
				case "multiline": return MultilineBlock;

				// default to copy block
				case "copy":
				default:
					return CopyBlock;
			}
			return null;
		},

		onPlaybackEnd: function (e) {
			for (i = 0, len = this.blocks.length; i < len; i++) {
				this.timeouts.push(_.delay(this.showBlock, 500 + i * 400, this.blocks[i].block));
			}

			this.blocks = [];
		},

		loadScene: function (sceneInfo, onSuccess, onError) {
			this.clearBlocks();
			this.clearTimeouts();
			var blocks = sceneInfo.scene;
			var sceneId = sceneInfo.sceneId;
			$.ajax({
			 	url: 'partial/scene/' + sceneId + '.html',
			 	success: onSuccess,
			 	error: onError
			});
		},

		clearBlocks: function () {
			this.blocks = [];
		},

		addBlock: function (block, time) {
			this.blocks.push({ block: block, time: time });
		},

		/**
		 * Tell blocks to show if they are at the right sequence in the video
		 * @param  {float} time 	in seconds
		 */
		onTimeTick: function (e) {
			if (this.blocks.length == 0)
				return;

			var time = e.data.time;
			var i, len;
			var readyBlocks = [];
			for (i = 0, len = this.blocks.length; i < len; i++) {
				if (this.blocks[i].time <= time) {
					readyBlocks.push(this.blocks[i].block);
					//box.event.dispatch(null, BlockEvents.show, {block: this.blocks[i].block});
				}
			}
			this.blocks.splice(0, readyBlocks.length);
			// don't show them all at once
			for (i = 0, len = readyBlocks.length; i < len; i++) {
				this.timeouts.push(_.delay(this.showBlock, i * 400, readyBlocks[i]));
			}
		},

		showBlock: function (block) {
			box.event.dispatch(null, BlockEvents.show, {block: block});
		},

		clearTimeouts: function () {
			while (this.timeouts.length > 0) {
				clearTimeout(this.timeouts.pop());
			}
		},

		dispose: function () {
			this.clearTimouts();
		}
	});

	return GridModel;
});